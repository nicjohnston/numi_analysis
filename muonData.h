#ifndef muonData_h
#define muonData_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <Riostream.h>
#include <fstream>
#include "TH1.h"
#include "TH1D.h"

#include "TF1.h"
#include "TH1.h"
#include "TMath.h"
#include "TFitResult.h"

#include "TError.h"
#include <math.h>
#include <string.h>
#include <algorithm>
#include "TString.h"

#include <Math/SpecFuncMathCore.h>
#include <Math/PdfFuncMathCore.h>
#include <Math/ProbFuncMathCore.h>

#include "TCanvas.h"
#include <TGraph.h>

// #include <cstdarg>

#define DEFAULT_TTREE_NAME		"rawData"

#define NUM_DETECTORS	5

//from NuMIMuonRecon.cc:
//Bunches are 18.83 ns long
const double BucketWidth = 1./53;//0.01883;
const int nbuckets = 600;


using namespace std;

typedef struct
{
	double timeD;
	ULong64_t time;
	int usec;
	double mean[NUM_DETECTORS];
	double rms[NUM_DETECTORS];
	double start[NUM_DETECTORS];
	int startBin[NUM_DETECTORS];
	int nb;
	double integ[NUM_DETECTORS][nbuckets];
	double total[NUM_DETECTORS];
	double bucketMin[NUM_DETECTORS][nbuckets];
	double bucketMax[NUM_DETECTORS][nbuckets];
	double min[NUM_DETECTORS];
	double max[NUM_DETECTORS];
} MuonStruct;

typedef struct
{
	double timeD;
	ULong64_t time;
	int usec;
	double dT;
	double mean;
	double rms;
	double start;
	int startBin;
	int nb;
	double integ[nbuckets];
	double total[4];
	double bucketMin[nbuckets];
	double bucketMax[nbuckets];
	double min;
	double max;
} RwmStruct;


typedef struct
{
	
	double pot;
	double dt_pot;
	double pot2;
	double dt_pot2;
	double T1;
	double T2;
	double pitch;
	double yaw;
	double Palc;
	double Ppump;
	double Pmks;
	double Par;
	double dt_mu;
	double lineA;
	double lineB;
	double lineC;
	double lineD;
	double tttgth;
	double hornI;
	double dt_horn;
	double mm1;
	double mm2;
	double mm3;
	double mm1x;
	double mm1y;
	double mm2x;
	double mm2y;
	double mm1cor;
	double mm2cor;
	double mm3cor;
	double mm1gas;
	double mm2gas;
	double mm3gas;
	double dt_mm;
	double dt_mmAltTiming;
	double gasMon2;
	double tgtx;
	double tgty;
	double dt_bpm;

	// Arrays
	double dt_mma1;
	double mma1ds[81];
	double mma1pd[81];
	double mma1cor[81];
} AcnetStruct;

class muonData {
public:
	muonData(  ) { cout << "Note:  you did not provide any data files" << endl; };
	muonData( char * oldDataFiles );

	void setBranchAddresses();
	int loadVariable( char name[], int force = 0 );

	virtual ~muonData() {  };

	TChain chain;

	vector< double > total[NUM_DETECTORS];
	vector< double > pot;
	vector< ULong64_t > time;

	long unsigned int chainEntries = 0;

	MuonStruct muon;
	RwmStruct rwm;
	AcnetStruct acnet;

};

int muonData::loadVariable( char name[], int force ) {
	if ( !strcmp(name, "time") ) {

		cout << "loading time" << endl;
		if ( time.size() == chainEntries && force ) {
			cout << "time is already loaded" << endl;
			return 1;
		} else {
			time.clear();
			for ( long unsigned int i = 0; i < chainEntries; i++ ) {
				chain.GetEntry(i);
				time.push_back(muon.time);
			}
			if (time.size() == chainEntries) {
				cout << "time is now loaded" << endl;
				return time.size();
			} else {
				return -1;
			}
		}

	} else if ( !strcmp(name, "pot")  ) {
		cout << "loading pot" << endl;
		if ( pot.size() == chainEntries && force ) {
			cout << "pot is already loaded" << endl;
			return 1;
		} else {
			pot.clear();
			for ( long unsigned int i = 0; i < chainEntries; i++ ) {
				chain.GetEntry(i);
				pot.push_back(acnet.pot);
			}
			if (pot.size() == chainEntries) {
				cout << "pot is now loaded" << endl;
				return pot.size();
			} else {
				return -1;
			}
		}
	} else {
		cout << "Dumbass.  Select a variable I know." << endl;
		return -1;
	}
}

void muonData::setBranchAddresses() {
	// if ( !chain ) {
	// 	cout << "Chain not loaded?" << endl;
	// 	return;
	// }
	chain.SetBranchAddress("total",&this->muon.total);
	chain.SetBranchAddress("pot",&this->acnet.pot);
	chain.SetBranchAddress("time",&this->muon.time);
}

muonData::muonData(  char * oldDataFiles ) {
	// this.chain = new TChain("MuMonData");
	// TChain("MuMonData") chain;
	TChain chain("MuMonData");

	char * pch;
	pch = strtok (oldDataFiles," ,");
	while (pch != NULL) {
		chain.Add(pch);
		printf ("Adding %s to the data chain\n",pch);
		pch = strtok (NULL, " , ");
	}

	chainEntries = chain.GetEntries();

}

#endif