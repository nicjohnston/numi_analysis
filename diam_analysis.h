#ifndef diamAnalysis_h
#define diamAnalysis_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <Riostream.h>
#include <fstream>
#include "TH1.h"
#include "TH1D.h"

#include "TF1.h"
#include "TH1.h"
#include "TMath.h"
#include "TFitResult.h"

#include "TError.h"
#include <math.h>
#include <string.h>
#include <algorithm>
#include "TString.h"

#include <Math/SpecFuncMathCore.h>
#include <Math/PdfFuncMathCore.h>
#include <Math/ProbFuncMathCore.h>

#include "TCanvas.h"
#include <TGraph.h>
#include <TProfile.h>
#include <TH2D.h>

#include <cstdarg>


#include <TGraph2D.h>
#include <TRandom.h>
#include <TStyle.h>
#include <TF2.h>

#include "cuts.h"


#include <ctime>


#define DEFAULT_TTREE_NAME		"MuMonData"

#define NUM_DETECTORS	5

#define SILICONCHANNEL 4
#define NEWDIAMCHANNEL 3

#define HOUR 3600


//from NuMIMuonRecon.cc:
//Bunches are 18.83 ns long
const double BucketWidth = 1./53;//0.01883;
const int nbuckets = 600;

using namespace std;

typedef struct
{
// from muon struct
	double timeD;
	ULong64_t time;
	int usec;
	double mean[NUM_DETECTORS];
	double rms[NUM_DETECTORS];
	double start[NUM_DETECTORS];
	int startBin[NUM_DETECTORS];
	int nb;
	double integ[NUM_DETECTORS][nbuckets];
	double total[NUM_DETECTORS];
	double bucketMin[NUM_DETECTORS][nbuckets];
	double bucketMax[NUM_DETECTORS][nbuckets];
	double min[NUM_DETECTORS];
	double max[NUM_DETECTORS];
// from rwm struct:
	double rtimeD;
	ULong64_t rtime;
	int rusec;
	// double rdT;
	double rmean;
	double rrms;
	double rstart;
	int rstartBin;
	int rnb;
	double rinteg[nbuckets];
	double rtotal[4];
	double rbucketMin[nbuckets];
	double rbucketMax[nbuckets];
	double rmin;
	double rmax;
// from acnet struct:
	double pot;
	double dt_pot;
	double pot2;
	double dt_pot2;
	double T1;
	double T2;
	double pitch;
	double yaw;
	double Palc;
	double Ppump;
	double Pmks;
	double Par;
	double dt_mu;
	double lineA;
	double lineB;
	double lineC;
	double lineD;
	double tttgth;
	double hornI;
	double dt_horn;
	double mm1;
	double mm2;
	double mm3;
	double mm1x;
	double mm1y;
	double mm2x;
	double mm2y;
	double mm1cor;
	double mm2cor;
	double mm3cor;
	double mm1gas;
	double mm2gas;
	double mm3gas;
	double dt_mm;
	double dt_mmAltTiming;
	double gasMon2;
	double tgtx;
	double tgty;
	double dt_bpm;

	// Arrays
	double dt_mma1;
	double mma1ds[81];
	double mma1pd[81];
	double mma1cor[81];
	double mma2ds[81];
	double mma2pd[81];
	double mma2cor[81];
} MuMonStruct;

// typedef struct {
// 	double 
// } ChanSettings;

double muMonArrayMap[81][2] = {
	{6.0,4.0},
	{6.0,5.0},
	{6.0,6.0},
	{6.0,7.0},
	{6.0,8.0},
	{6.0,9.0},
	{7.0,1.0},
	{7.0,2.0},
	{7.0,3.0},
	{7.0,4.0},
	{7.0,5.0},
	{7.0,6.0},
	{7.0,7.0},
	{7.0,8.0},
	{7.0,9.0},
	{8.0,1.0},
	{8.0,2.0},
	{8.0,3.0},
	{8.0,4.0},
	{8.0,5.0},
	{8.0,6.0},
	{8.0,7.0},
	{8.0,8.0},
	{8.0,9.0},
	{9.0,1.0},
	{9.0,2.0},
	{9.0,3.0},
	{9.0,4.0},
	{9.0,5.0},
	{9.0,6.0},
	{9.0,7.0},
	{9.0,8.0},
	{9.0,9.0},
	{1.0,1.0},
	{1.0,2.0},
	{1.0,3.0},
	{1.0,4.0},
	{1.0,5.0},
	{1.0,6.0},
	{1.0,7.0},
	{1.0,8.0},
	{1.0,9.0},
	{2.0,1.0},
	{2.0,2.0},
	{2.0,3.0},
	{2.0,4.0},
	{2.0,5.0},
	{2.0,6.0},
	{2.0,7.0},
	{2.0,8.0},
	{2.0,9.0},
	{3.0,1.0},
	{3.0,2.0},
	{3.0,3.0},
	{3.0,4.0},
	{3.0,5.0},
	{3.0,6.0},
	{3.0,7.0},
	{3.0,8.0},
	{3.0,9.0},
	{4.0,1.0},
	{4.0,2.0},
	{4.0,3.0},
	{4.0,4.0},
	{4.0,5.0},
	{4.0,6.0},
	{4.0,7.0},
	{4.0,8.0},
	{4.0,9.0},
	{5.0,1.0},
	{5.0,2.0},
	{5.0,3.0},
	{5.0,4.0},
	{5.0,5.0},
	{5.0,6.0},
	{5.0,7.0},
	{5.0,8.0},
	{5.0,9.0},
	{6.0,1.0},
	{6.0,2.0},
	{6.0,3.0},
};

class diamAnalysis {
public :
	diamAnalysis( ) { cout << "Give me more info" << endl; };
	diamAnalysis( char * dataFiles, const char * outputFilename = "defaultout.root" );
	void makeAllPlots();
	void plotSignalVsTime( int channel = 3, double timePad = 43200.0 );
	void plotSignalVsTimeProfile( int channel = 3, double timePad = 43200.0, ULong64_t binWidth = 43200.0 );
	void plotSignalVsPotCorrelation( int channel = 3, ULong64_t minTime = 0, ULong64_t maxTime = 0, double binWidthPot = 0.05, double binWidthSignal = 100.0 );
	void plotSignalVsTemp1Correlation( int channel = 3, double binWidthTemp1 = 0.1, double binWidthSignal = 10.0 );
	void plotSignalVsTemp1( int channel = 3, double temp1Pad = 0 );
	void plotSignalVsTemp1Profile( int channel = 3, double timePad = 0, double binWidth = 0.5 );
	void plotMuMonCellVsTime( int channel, double timePad, ULong64_t binWidth );
	int plotMuMonArrayBeamView( int event, const char *drawOption );
	int plotMuMonArrayBeamViewIntegral( int event, int numSpills, const char *drawOption, int nodraw = 0 );
	void plotMuMonPosVsTime( int channel, double timePad, ULong64_t binWidth );
	void plotSignalvsMuMonYposCorrelation( int channel, double binWidthMuMonYpos, double binWidthSignal );
	void plotNewDetectorSignalRatioProfile( double timePad, ULong64_t binWidth );
	void plotMuMonCellRatioProfile( int cell1, int cell2, double timePad, ULong64_t binWidth );
	void plotMinMaxSignalVsTimeProfile( int channel = 3, double timePad = 43200.0, ULong64_t binWidth = 43200.0 );

	void measureBeamWidth( int startEntry, int maxEntries, int numSpills, ULong64_t binWidth = 12*HOUR, double timePad = 12*HOUR, const char* basename = "" );

	void setCuts();
	void setBranches();
	void savePlots( char * filename );
	void makeBeamViewMovie( const char *basename, int startEvent );

	template<class T> void SetRange( T& obj, char axis, double min, double max );
	
	virtual ~diamAnalysis();

	TChain *data;

	TFile *file;

	int entries = 0;

	MuMonStruct mumonData;

	Cuts cuts = Cuts();

	TGraph signalVsTimePlot, signalVsTemp1Plot;
	TProfile signalVsTimePlotProfile, signalVsTemp1PlotProfile, muMonCellVsTimePlotProfile, muMonCellNormedVsTimePlotProfile, muMonXposVsTimePlotProfile, muMonYposVsTimePlotProfile, newSignalRatioPlotProfile, muMonCellSigRatioPlotProfile, minMaxDistVsTimePlotProfile, minMaxMidpointVsTimePlotProfile, avgMinVsTimePlotProfile ;
	TH2D signalVsPotCorrelationPlot, signalVsTemp1CorrelationPlot, signalVsMuMonYposCorrelationPlot;
	// TGraph2D muMonArrayBeamViewTGraph2D;

	TGraph2D *muMonArrayBeamViewTGraph2D;
	TGraph2D *muMonArrayBeamViewIntegralTGraph2D;
	TCanvas *muMonArrayBeamViewCanvas;

	TF2 *fitresult;
	double beamMeasurementResults[200000][5];


	// cuts:
	double timei15 = 1427.13e6;
	double timef15 = 1434.6e6;
	
	double timei16 = 1457.1e6;
	double timef16 = 1467.95e6;
	double badhorni = 1433.755e6;
	double antii = 1467.2e6;
	double turnonf16 = 1458.8e6;

	TString D1sigCut15;
	TString D1sigCut16;
	TString D2sigCut15;
	TString D2sigCut16;
	TString potCut;
	TString DTpotCut;
	TString DThornCut;
	TString hornCut;
	TString antiTimeCut;
	TString badHornTimeCut;
	TString turnOntimeCut16;

	// double muMonArrayMap[81][2];

	
};


void diamAnalysis::plotMinMaxSignalVsTimeProfile( int channel, double timePad, ULong64_t binWidth ) {
	data->GetEntry((int)entries/2);
	ULong64_t minTime = mumonData.time;
	ULong64_t maxTime = mumonData.time;

	int numCol = 3;
	double minSig[numCol] = {mumonData.total[channel]/mumonData.pot};
	double maxSig[numCol] = {mumonData.total[channel]/mumonData.pot};
	ULong64_t time[entries];printf("line1: entries=%d\n",entries);
	//double signal[entries][2];printf("line2\n");
	
	double** signal = new double*[entries];
	for (int i = 0; i < entries; i++) signal[i] = new double[numCol];
	int pointNum = 0;

	double minMaxAvg = 0;
	double minMaxDist = 0;
	double minMaxMidPoint = 0;
	double minAvg = 0;
	double maxAvg = 0;
	int bucketCounter = 0;

	for ( int i = 0; i < entries; i++ ) {
		data->GetEntry(i);
		if ( cuts.potCut(mumonData.pot) && cuts.potDtCut(mumonData.dt_pot) ) {
			if (pointNum == 0) cout << "First data point at t=" << mumonData.time << endl;
			time[pointNum] = mumonData.time;
			minMaxAvg = 0;
			minMaxDist = 0;
			minMaxMidPoint = 0;
			bucketCounter = 0;
			minAvg = 0;
			maxAvg = 0;
			for (int j = 0; j < nbuckets; j++ ) {
				if (mumonData.bucketMin[channel][j] < 1e8 && mumonData.bucketMax[channel][j] > -1e8 ) {
					minMaxDist += mumonData.bucketMax[channel][j] - mumonData.bucketMin[channel][j];
					minMaxMidPoint += (mumonData.bucketMax[channel][j] - mumonData.bucketMin[channel][j])/2 + mumonData.bucketMin[channel][j];
					minAvg += mumonData.bucketMin[channel][j];
					maxAvg += mumonData.bucketMax[channel][j];
					bucketCounter++;
				}
			}
			if (mumonData.timeD > 1551217800.0) {
				signal[pointNum][0] = (( minMaxDist / (double)bucketCounter ) / mumonData.pot ) / 1.430413832;
				signal[pointNum][1] = (( minMaxMidPoint / (double)bucketCounter ) / mumonData.pot ) / 1.430413832;
				signal[pointNum][2] = (( 100* minAvg / (double)bucketCounter ) / mumonData.pot ) / 1.430413832;
			} else {
				signal[pointNum][0] = ( minMaxDist / (double)bucketCounter ) / mumonData.pot;
				signal[pointNum][1] = ( minMaxMidPoint / (double)bucketCounter ) / mumonData.pot;
				signal[pointNum][2] = ( 100* minAvg / (double)bucketCounter ) / mumonData.pot;
			}
			/*if (pointNum < 10) {
				printf("bucketCounter = %d\t",bucketCounter);
				printf("minMaxDist = %lf\t",signal[pointNum][0]);
				printf("minMaxMidPoint = %lf\t",signal[pointNum][1]);
				printf("minAvg = %lf\n",signal[pointNum][2]);
			}*/
			/*if (i % 10000 ) {
				printf("\rEntry: %d",i);
				fflush(stdout);
			}*/
			if (mumonData.time < minTime) minTime = mumonData.time;
			if (mumonData.time > maxTime) maxTime = mumonData.time;
			for (int j = 0; j < numCol; j++) {
				if (signal[pointNum][j] < minSig[j]) minSig[j] = signal[pointNum][j];
				if (signal[pointNum][j] > maxSig[j]) maxSig[j] = signal[pointNum][j];
			}
			pointNum++;
			//if (pointNum > 10) break;
		}
	}
	ULong64_t numBins = (ULong64_t) (maxTime - minTime) / binWidth;
	cout << "numBins = " << numBins << endl;

	minMaxDistVsTimePlotProfile = TProfile("minMaxDistVsTimePlotProfile","Signal Voltage Range vs Time (Fall 2018);Time;Signal / POT", numBins, minTime - 1,maxTime + 1);
	minMaxMidpointVsTimePlotProfile = TProfile("minMaxMidpointVsTimePlotProfile","Signal Midpoint vs Time (Fall 2018);Time;Signal / POT", numBins, minTime - 1,maxTime + 1);
	avgMinVsTimePlotProfile = TProfile("avgMinVsTimePlotProfile","Average bucket minimum vs Time (Fall 2018);Time;Signal / POT", numBins, minTime - 1,maxTime + 1);
	for ( int i = 0; i < pointNum; i++ ) {
		minMaxDistVsTimePlotProfile.Fill(time[i],signal[i][0]);
		minMaxMidpointVsTimePlotProfile.Fill(time[i],signal[i][1]);
		avgMinVsTimePlotProfile.Fill(time[i],signal[i][2]);
			/*if (i < 10) {
				printf("minMaxDist = %lf\t",signal[i][0]);
				printf("minMaxMidPoint = %lf\t",signal[i][1]);
				printf("minAvg = %lf\n",signal[i][2]);
			}*/
	}

	TAxis* xaxis[3] = {minMaxDistVsTimePlotProfile.GetXaxis(), minMaxMidpointVsTimePlotProfile.GetXaxis(),avgMinVsTimePlotProfile.GetXaxis()};
	for (int i = 0; i < numCol; i++) {
		xaxis[i]->SetTimeDisplay(1);
		xaxis[i]->SetNdivisions(-508);
		xaxis[i]->SetTimeFormat("%m/%d"); 
		xaxis[i]->SetTimeOffset(0,"utc");
	}
	TCanvas* minMaxDistVtimeProf = new TCanvas("minMaxVtimeProf", "minMax distance vs Time Canvas (TProfile)", 30, 30, 1600, 900);
	TCanvas* minMaxMidVtimeProf = new TCanvas("minMaxMidVtimeProf", "Bucket midpoint vs Time Canvas (TProfile)", 30, 30, 1600, 900);
	TCanvas* minVtimeProf = new TCanvas("minVtimeProf", "Bucket min vs Time Canvas (TProfile)", 30, 30, 1600, 900);
	minMaxDistVtimeProf->cd();
	gStyle->SetOptStat(0);
	minMaxDistVsTimePlotProfile.Draw();
	minMaxMidVtimeProf->cd();
	gStyle->SetOptStat(0);
	minMaxMidpointVsTimePlotProfile.Draw();
	minVtimeProf->cd();
	gStyle->SetOptStat(0);
	avgMinVsTimePlotProfile.Draw();


	for (int i = 0; i < entries; i++) delete [] signal[i];
	delete [] signal;
}

void diamAnalysis::plotSignalVsTime( int channel, double timePad ) {
	TCanvas* sigVtime = new TCanvas("sigVtime", "Signal vs Time Canvas", 30, 30, 1600, 900);
	signalVsTimePlot = TGraph();

	data->GetEntry((int)entries/2);
	ULong64_t minTime = mumonData.time;
	ULong64_t maxTime = mumonData.time;
	double signal = 0;
	double minSig = mumonData.total[channel]/mumonData.pot;
	double maxSig = mumonData.total[channel]/mumonData.pot;

	double pointNum = 0;
	for ( int i = 0; i < entries; i++ ) {
		data->GetEntry(i);
		if ( cuts.potCut(mumonData.pot) && cuts.potDtCut(mumonData.dt_pot) ) {
			signal = mumonData.total[channel]/mumonData.pot;
			signalVsTimePlot.SetPoint(pointNum,mumonData.time, signal);
			if (mumonData.time < minTime) minTime = mumonData.time;
			if (mumonData.time > maxTime) maxTime = mumonData.time;
			if (signal < minSig) minSig = signal;
			if (signal > maxSig) maxSig = signal;
			pointNum++;
		}
	}
	signalVsTimePlot.SetTitle("Total Signal vs Time (Fall 2018);Time;Signal / POT");
	auto xaxis = signalVsTimePlot.GetXaxis();
	auto yaxis = signalVsTimePlot.GetYaxis();
	xaxis->SetLimits(minTime - timePad,maxTime + timePad);
	double avgSig = signalVsTimePlot.GetMean(2);
	yaxis->SetRangeUser(avgSig-0.06*avgSig, avgSig+0.06*avgSig);
	cout << minSig << "\t\t" << maxSig << endl;
	cout << avgSig << "\t\t" << avgSig - 0.06*avgSig << "\t\t" << avgSig + 0.06*avgSig << endl;
	xaxis->SetTimeDisplay(1);
	xaxis->SetNdivisions(-508);
	xaxis->SetTimeFormat("%m/%d"); 
	xaxis->SetTimeOffset(0,"utc");
	sigVtime->cd();
	signalVsTimePlot.Draw("ap");
}

void diamAnalysis::plotSignalVsTimeProfile( int channel, double timePad, ULong64_t binWidth ) {
	TCanvas* sigVtimeProf = new TCanvas("sigVtimeProf", "Signal vs Time Canvas (TProfile)", 30, 30, 1600, 900);
	data->GetEntry((int)entries/2);
	ULong64_t minTime = mumonData.time;
	ULong64_t maxTime = mumonData.time;

	double minSig = mumonData.total[channel]/mumonData.pot;
	double maxSig = mumonData.total[channel]/mumonData.pot;
	ULong64_t time[entries];
	double signal[entries];
	int pointNum = 0;
	for ( int i = 0; i < entries; i++ ) {
		data->GetEntry(i);
		if ( cuts.potCut(mumonData.pot) && cuts.potDtCut(mumonData.dt_pot) ) {
			if (pointNum == 0) cout << "First data point at t=" << mumonData.time << endl;
			time[pointNum] = mumonData.time;
			signal[pointNum] = mumonData.total[channel]/mumonData.pot;
			if (mumonData.time < minTime) minTime = mumonData.time;
			if (mumonData.time > maxTime) maxTime = mumonData.time;
			if (signal[pointNum] < minSig) minSig = signal[pointNum];
			if (signal[pointNum] > maxSig) maxSig = signal[pointNum];
			pointNum++;
		}
	}
	ULong64_t numBins = (ULong64_t) (maxTime - minTime) / binWidth;
	cout << "numBins = " << numBins << endl;

	signalVsTimePlotProfile = TProfile("signalVsTimePlotProfile","Total Signal vs Time (Fall 2018);Time;Signal / POT", numBins, minTime - 1,maxTime + 1);
	for ( int i = 0; i < pointNum; i++ ) {
		signalVsTimePlotProfile.Fill(time[i],signal[i]);
	}

	auto xaxis = signalVsTimePlotProfile.GetXaxis();
	xaxis->SetTimeDisplay(1);
	xaxis->SetNdivisions(-508);
	xaxis->SetTimeFormat("%m/%d"); 
	xaxis->SetTimeOffset(0,"utc");
	sigVtimeProf->cd();
	signalVsTimePlotProfile.Draw();
}

void diamAnalysis::plotSignalVsPotCorrelation( int channel, ULong64_t minTime, ULong64_t maxTime, double binWidthPot, double binWidthSignal ) {
	TCanvas* sigVsPotCorrel = new TCanvas("sigVsPotCorrel", "Signal vs PoT Canvas (TProfile)", 30, 30, 1600, 900);
	data->GetEntry((int)entries/2);

	if ( maxTime == 0 ) {
		maxTime = 0x7fffffff;
		cout << "maxTime changed to: " << maxTime << endl;
	}

	double minPot = mumonData.pot;
	double maxPot = mumonData.pot;
	double minSig = mumonData.total[channel];
	double maxSig = mumonData.total[channel];
	double pot[entries];
	double signal[entries];
	int pointNum = 0;

	//correlation vars:
	double sigmaPotSquared = 0;
	double sigmaSignalSquared = 0;
	double sigmaPotSignal = 0;
	double potAvg = 0;
	double signalAvg = 0;
	for ( int i = 0; i < entries; i++ ) {
		data->GetEntry(i);
		if ( cuts.potCut(mumonData.pot) && cuts.potDtCut(mumonData.dt_pot) && mumonData.time > minTime && mumonData.time < maxTime ) {
			pot[pointNum] = mumonData.pot;
			signal[pointNum] = mumonData.total[channel];
			if (pot[pointNum] < minPot) minPot = pot[pointNum];
			if (pot[pointNum] > maxPot) maxPot = pot[pointNum];
			if (signal[pointNum] < minSig) minSig = signal[pointNum];
			if (signal[pointNum] > maxSig) maxSig = signal[pointNum];
			potAvg += pot[pointNum];
			signalAvg += signal[pointNum];
			pointNum++;
		}
	}
	potAvg = potAvg/( (double)pointNum - 1.0);
	signalAvg = signalAvg/( (double)pointNum - 1.0);

	int numBinsX = (int) round( ( 55.0 - 30.0 ) / binWidthPot );
	cout << "numBinsX = " << numBinsX << endl;
	int numBinsY = (int) round( ( maxSig - minSig ) / binWidthSignal );
	cout << "numBinsY = " << numBinsY << endl;
	// cout << minPot << "\t\t" << maxPot << "\t\t" << minSig << "\t\t" << maxSig << endl;

	signalVsPotCorrelationPlot = TH2D("signalVsTimePlotProfile","Total Signal vs PoT (Fall 2018);PoT;Signal", numBinsX, 30.0, 55.0, numBinsY, minSig, maxSig + 0.5e3 );
	for ( int i = 0; i < pointNum; i++ ) {
		signalVsPotCorrelationPlot.Fill(pot[i],signal[i]);

		sigmaPotSquared += pow( pot[i] - potAvg, 2.0 );
		sigmaSignalSquared += pow( signal[i] - signalAvg, 2.0 );
		sigmaPotSignal += ( pot[i] - potAvg )*( signal[i] - signalAvg );
	}

	double correlation = sigmaPotSignal / sqrt( sigmaPotSquared * sigmaSignalSquared );
	cout << "Correlation coefficient is: " << correlation << endl;
	sigVsPotCorrel->cd();
	signalVsPotCorrelationPlot.Draw("colz");
}



void diamAnalysis::plotSignalVsTemp1Correlation( int channel, double binWidthTemp1, double binWidthSignal ) {
	TCanvas* sigVsTemp1Correl = new TCanvas("sigVsTemp1Correl", "Signal vs Temp1 Canvas (TProfile)", 30, 30, 1600, 900);
	data->GetEntry((int)entries/2);
	
	double minTemp1 = mumonData.tttgth;
	double maxTemp1 = mumonData.tttgth;
	double minSig = mumonData.total[channel] / mumonData.pot;
	double maxSig = mumonData.total[channel] / mumonData.pot;
	double temp1[entries];
	double signal[entries];
	int pointNum = 0;
	
	//correlation vars:
	double sigmaTemp1Squared = 0;
	double sigmaSignalSquared = 0;
	double sigmaTemp1Signal = 0;
	double temp1Avg = 0;
	double signalAvg = 0;
	for ( int i = 0; i < entries; i++ ) {
		data->GetEntry(i);
		if ( cuts.potCut(mumonData.pot) && cuts.potDtCut(mumonData.dt_pot) ) {
			temp1[pointNum] = mumonData.tttgth;
			signal[pointNum] = mumonData.total[channel] / mumonData.pot;
			if (temp1[pointNum] < minTemp1) minTemp1 = temp1[pointNum];
			if (temp1[pointNum] > maxTemp1) maxTemp1 = temp1[pointNum];
			if (signal[pointNum] < minSig) minSig = signal[pointNum];
			if (signal[pointNum] > maxSig) maxSig = signal[pointNum];
			temp1Avg += temp1[pointNum];
			signalAvg += signal[pointNum];
			pointNum++;
		}
	}
	temp1Avg = temp1Avg/( (double)pointNum - 1.0);
	signalAvg = signalAvg/( (double)pointNum - 1.0);

	int numBinsX = (int) round( ( maxTemp1 - minTemp1 ) / binWidthTemp1 );
	cout << "numBinsX = " << numBinsX << endl;
	int numBinsY = (int) round( ( maxSig - minSig ) / binWidthSignal );
	cout << "numBinsY = " << numBinsY << endl;
	cout << minTemp1 << "\t\t" << maxTemp1 << "\t\t" << minSig << "\t\t" << maxSig << endl;

	signalVsTemp1CorrelationPlot = TH2D("signalVsTemp1CorrelationPlot","Total Signal vs PoT (Fall 2018);Temperature (E:TTTGHT);Signal / PoT", numBinsX, minTemp1, maxTemp1, numBinsY, minSig, maxSig );
	for ( int i = 0; i < pointNum; i++ ) {
		signalVsTemp1CorrelationPlot.Fill(temp1[i],signal[i]);

		sigmaTemp1Squared += pow( temp1[i] - temp1Avg, 2.0 );
		sigmaSignalSquared += pow( signal[i] - signalAvg, 2.0 );
		sigmaTemp1Signal += ( temp1[i] - temp1Avg )*( signal[i] - signalAvg );
	}

	double correlation = sigmaTemp1Signal / sqrt( sigmaTemp1Squared * sigmaSignalSquared );
	cout << "Correlation coefficient is: " << correlation << endl;
	sigVsTemp1Correl->cd();
	signalVsTemp1CorrelationPlot.Draw("colz");
}



void diamAnalysis::plotSignalVsTemp1( int channel, double temp1Pad ) {
	TCanvas* sigVtemp1 = new TCanvas("sigVtemp1", "Signal vs Time Canvas", 30, 30, 1600, 900);

	signalVsTemp1Plot = TGraph();

	data->GetEntry((int)entries/2);
	double minTemp1 = mumonData.tttgth;
	double maxTemp1 = mumonData.tttgth;

	double signal = 0;
	double minSig = mumonData.total[channel]/mumonData.pot;
	double maxSig = mumonData.total[channel]/mumonData.pot;

	double pointNum = 0;
	for ( int i = 0; i < entries; i++ ) {
		data->GetEntry(i);
		if ( cuts.potCut(mumonData.pot) && cuts.potDtCut(mumonData.dt_pot) ) {
			signal = mumonData.total[channel]/mumonData.pot;
			signalVsTemp1Plot.SetPoint(pointNum,mumonData.tttgth, signal);
			if (mumonData.tttgth < minTemp1) minTemp1 = mumonData.tttgth;
			if (mumonData.tttgth > maxTemp1) maxTemp1 = mumonData.tttgth;
			if (signal < minSig) minSig = signal;
			if (signal > maxSig) maxSig = signal;
			pointNum++;
		}
	}
	signalVsTemp1Plot.SetTitle("Total Signal vs Temperature 1 (Fall 2018);Temperature (E:TTTGHT);Signal / POT");
	auto xaxis = signalVsTemp1Plot.GetXaxis();
	auto yaxis = signalVsTemp1Plot.GetYaxis();
	xaxis->SetLimits(minTemp1 - temp1Pad, maxTemp1 + temp1Pad);
	double avgSig = signalVsTemp1Plot.GetMean(2);
	yaxis->SetRangeUser(avgSig-0.06*avgSig, avgSig+0.06*avgSig);
	cout << minSig << "\t\t" << maxSig << endl;
	cout << avgSig << "\t\t" << avgSig - 0.06*avgSig << "\t\t" << avgSig + 0.06*avgSig << endl;
	sigVtemp1->cd();
	signalVsTemp1Plot.Draw("ap");
}



void diamAnalysis::plotSignalVsTemp1Profile( int channel, double timePad, double binWidth ) {
	TCanvas* sigVsTemp1Prof = new TCanvas("sigVsTemp1Prof", "Signal vs Time Canvas (TProfile)", 30, 30, 1600, 900);
	data->GetEntry((int)entries/2);
	double minTemp1 = mumonData.tttgth;
	double maxTemp1 = mumonData.tttgth;

	double minSig = mumonData.total[channel]/mumonData.pot;
	double maxSig = mumonData.total[channel]/mumonData.pot;
	
	double temp1[entries];
	double signal[entries];
	int pointNum = 0;
	for ( int i = 0; i < entries; i++ ) {
		data->GetEntry(i);
		if ( cuts.potCut(mumonData.pot) && cuts.potDtCut(mumonData.dt_pot) ) {
			// if (pointNum == 0) cout << "First data point at t=" << mumonData.time << endl;
			temp1[pointNum] = mumonData.tttgth;
			signal[pointNum] = mumonData.total[channel]/mumonData.pot;
			if (temp1[pointNum] < minTemp1) minTemp1 = temp1[pointNum];
			if (temp1[pointNum] > maxTemp1) maxTemp1 = temp1[pointNum];
			if (signal[pointNum] < minSig) minSig = signal[pointNum];
			if (signal[pointNum] > maxSig) maxSig = signal[pointNum];
			pointNum++;
		}
	}
	int numBins = (int) round( (maxTemp1 - minTemp1) / binWidth );
	cout << "numBins = " << numBins << endl;

	signalVsTemp1PlotProfile = TProfile("signalVsTemp1PlotProfile","Total Signal vs Temperature (Fall 2018);Temperature;Signal / POT", numBins, minTemp1, maxTemp1);
	for ( int i = 0; i < pointNum; i++ ) {
		signalVsTemp1PlotProfile.Fill(temp1[i],signal[i]);
	}

	sigVsTemp1Prof->cd();
	signalVsTemp1PlotProfile.Draw();
}

void diamAnalysis::plotMuMonCellVsTime( int channel, double timePad, ULong64_t binWidth ) {
	TCanvas* muMonCellVsTimeProf = new TCanvas("muMonCellVsTimeProf", "MuMon Signal vs Time Canvas (TProfile)", 30, 30, 1600, 900);
	TCanvas* muMonCellNormedVsTimeProf = new TCanvas("muMonCellNormedVsTimeProf", "MuMon Signal vs Time Canvas (TProfile)", 30, 30, 1600, 900);
	data->GetEntry((int)entries/2);
	ULong64_t minTime = mumonData.time;
	ULong64_t maxTime = mumonData.time;

	// double minSig = mumonData.mma1ds[channel] - mumonData.mma1pd[channel];
	// double maxSig = mumonData.mma1ds[channel] - mumonData.mma1pd[channel];
	// double minNormedSig = ( mumonData.mma1ds[channel] - mumonData.mma1pd[channel] ) / mumonData.pot;
	// double maxNormedSig = ( mumonData.mma1ds[channel] - mumonData.mma1pd[channel] ) / mumonData.pot;
	ULong64_t time[entries];
	double signal[entries];
	double normedSignal[entries];
	int pointNum = 0;
	for ( int i = 0; i < entries; i++ ) {
		data->GetEntry(i);
		if ( cuts.potCut(mumonData.pot) && cuts.potDtCut(mumonData.dt_pot) ) {
			if (pointNum == 0) cout << "First data point at t=" << mumonData.time << endl;
			time[pointNum] = mumonData.time;
			signal[pointNum] = mumonData.mma2ds[channel] - mumonData.mma2pd[channel];
			normedSignal[pointNum] = ( mumonData.mma2ds[channel] - mumonData.mma2pd[channel] ) / mumonData.pot;
			if (mumonData.time < minTime) minTime = mumonData.time;
			if (mumonData.time > maxTime) maxTime = mumonData.time;
			// if (signal[pointNum] < minSig) minSig = signal[pointNum];
			// if (signal[pointNum] > maxSig) maxSig = signal[pointNum];
			// if (normedSignal[pointNum] < minNormedSig) minNormedSig = normedSignal[pointNum];
			// if (normedSignal[pointNum] > maxNormedSig) maxNormedSig = normedSignal[pointNum];
			pointNum++;
		}
	}
	ULong64_t numBins = (ULong64_t) (maxTime - minTime) / binWidth;
	cout << "numBins = " << numBins << endl;

	muMonCellVsTimePlotProfile = TProfile("muMonCellVsTimePlotProfile","MuMon Signal over Time;Time;MuMon Signal", numBins, minTime - 1,maxTime + 1);
	muMonCellNormedVsTimePlotProfile = TProfile("muMonCellNormedVsTimePlotProfile","PoT Normalized MuMon Signal over Time;Time;MuMon Signal / PoT", numBins, minTime - 1,maxTime + 1);
	for ( int i = 0; i < pointNum; i++ ) {
		muMonCellVsTimePlotProfile.Fill(time[i],signal[i]);
		muMonCellNormedVsTimePlotProfile.Fill(time[i],normedSignal[i]);
	}

	auto xaxis = muMonCellVsTimePlotProfile.GetXaxis();
	xaxis->SetTimeDisplay(1);
	xaxis->SetNdivisions(-508);
	xaxis->SetTimeFormat("%m/%d"); 
	xaxis->SetTimeOffset(0,"utc");
	muMonCellVsTimeProf->cd();
	muMonCellVsTimePlotProfile.Draw();

	auto xaxisNormed = muMonCellNormedVsTimePlotProfile.GetXaxis();
	xaxisNormed->SetTimeDisplay(1);
	xaxisNormed->SetNdivisions(-508);
	xaxisNormed->SetTimeFormat("%m/%d"); 
	xaxisNormed->SetTimeOffset(0,"utc");
	muMonCellNormedVsTimeProf->cd();
	muMonCellNormedVsTimePlotProfile.Draw();
}

int diamAnalysis::plotMuMonArrayBeamView( int event, const char *drawOption ) {
	data->GetEntry( event );
	// muMonArrayBeamViewCanvas = new TCanvas("muMonArrayBeamViewCanvas", "Muon Monitor Array Beam View", 30, 30, 1600, 900);

	muMonArrayBeamViewTGraph2D = new TGraph2D(81);
	muMonArrayBeamViewTGraph2D->SetNpx(9);
	muMonArrayBeamViewTGraph2D->SetNpy(9);
	int pointNum = 0;
	bool dataLoaded = false;
	for (Int_t i=1; i<81; i++) {
		if ( cuts.potCut(mumonData.pot) && cuts.potDtCut(mumonData.dt_pot) && abs(mumonData.mma2cor[i]) < 10.0 ) {
			muMonArrayBeamViewTGraph2D->SetPoint(i, muMonArrayMap[i][0], 10.0 - muMonArrayMap[i][1], -mumonData.mma2ds[i]/mumonData.pot);
			// pointNum++;
			dataLoaded = true;
		} else {
			muMonArrayBeamViewTGraph2D->SetPoint(i, muMonArrayMap[i][0], 10.0 - muMonArrayMap[i][1], 0);
		}
		// cout << "Loading element " << i << " into tgraph" << endl;
		// cout << muMonArrayMap[i][0] << "\t\t" << muMonArrayMap[i][1] << "\t\t" << mumonData.mma2cor[i] << endl;
	}
	if (dataLoaded) {
		muMonArrayBeamViewCanvas = new TCanvas("muMonArrayBeamViewCanvas", "Muon Monitor Array Beam View", 30, 30, 1600, 900);
		char buffer [80];
	    time_t rawtime = (time_t)(mumonData.time);
	    strftime (buffer,80,"%D at %H:%M\0",localtime(&rawtime));
	    // puts (buffer);
		muMonArrayBeamViewTGraph2D->SetTitle(Form("Muon Monitor 2 Signal on %s UTC (event %d) ;Column;Row", buffer, event));
		muMonArrayBeamViewTGraph2D->SetMarkerStyle(20);
		muMonArrayBeamViewCanvas->cd();
		muMonArrayBeamViewTGraph2D->Draw(drawOption);
		return 1;
	} else {
		return 0;
	}
	// if (saveName != "") {
	// 	muMonArrayBeamViewCanvas->Print(Form("%s.png",saveName),"png");
	// }
}

void diamAnalysis::makeBeamViewMovie( const char *basename, int startEvent ) {
	for (int i = startEvent; i < entries; i++) {
		int retStatus = plotMuMonArrayBeamView(i,"colz");
		// cout << Form("%s%d.png",basename,i) << endl;
		if (retStatus == 1) {
			muMonArrayBeamViewCanvas->SaveAs(Form("%s%d.png",basename,i));
		}
	}
}

int diamAnalysis::plotMuMonArrayBeamViewIntegral( int event, int numSpills, const char *drawOption, int nodraw ) {
	muMonArrayBeamViewCanvas = new TCanvas("muMonArrayBeamViewCanvas", "Muon Monitor Array Beam View", 30, 30, 1600, 900);

	muMonArrayBeamViewIntegralTGraph2D = new TGraph2D(81);
	muMonArrayBeamViewIntegralTGraph2D->SetNpx(9);
	muMonArrayBeamViewIntegralTGraph2D->SetNpy(9);
	int pointNum = 0;
	// cout << numSpills << endl;
	double signal[81];
    fill(signal, signal+81, 0);
    int i = event;
	for (i = event; pointNum < numSpills; i++) {
		data->GetEntry( i );
		// cout << "loading event " << i << endl;
		if ( cuts.potCut(mumonData.pot) && cuts.potDtCut(mumonData.dt_pot) ) {
			for (int j = 0; j < 81; j++) {
				if ( abs(mumonData.mma2cor[j]) < 10.0 ) {
					signal[j] += - mumonData.mma2cor[j]/mumonData.pot;
					// cout << "loaded value " << - mumonData.mma2cor[j]/mumonData.pot << " into signal[" << j << "]" << endl;
				}
			}
			// cout << "finished loading event " << i << endl;
			// cout << "dt_mma1: " << mumonData.dt_mma1 << endl;
			pointNum++;
		}
	}
	for (Int_t i=0; i<81; i++) {
		muMonArrayBeamViewIntegralTGraph2D->SetPoint(i, muMonArrayMap[i][0], 10.0 - muMonArrayMap[i][1], signal[i]);
		// cout << signal[i] << endl;
	}
		muMonArrayBeamViewIntegralTGraph2D->SetTitle(Form("Muon Monitor 2 Signal for events %d through %d;Column;Row", event, i-1));
		muMonArrayBeamViewIntegralTGraph2D->SetMarkerStyle(20);
	if (nodraw == 0) {
		muMonArrayBeamViewCanvas->cd();
		muMonArrayBeamViewIntegralTGraph2D->Draw(drawOption);
	}
	return i;
}


void diamAnalysis::plotMuMonPosVsTime( int channel, double timePad, ULong64_t binWidth ) {
	TCanvas *muMonXpositionCanvas = new TCanvas("muMonXpositionCanvas", "MuMon posx vs Time Canvas (TProfile)", 30, 30, 1600, 900);
	TCanvas *muMonYpositionCanvas = new TCanvas("muMonYpositionCanvas", "MuMon poxy vs Time Canvas (TProfile)", 30, 30, 1600, 900);
	data->GetEntry((int)entries/2);
	ULong64_t minTime = mumonData.time;
	ULong64_t maxTime = mumonData.time;

	// double minSig = mumonData.mma1ds[channel] - mumonData.mma1pd[channel];
	// double maxSig = mumonData.mma1ds[channel] - mumonData.mma1pd[channel];
	// double minNormedSig = ( mumonData.mma1ds[channel] - mumonData.mma1pd[channel] ) / mumonData.pot;
	// double maxNormedSig = ( mumonData.mma1ds[channel] - mumonData.mma1pd[channel] ) / mumonData.pot;
	ULong64_t time[entries];
	double signalx[entries];
	double signaly[entries];
	int pointNum = 0;
	for ( int i = 0; i < entries; i++ ) {
		data->GetEntry(i);
		if ( cuts.potCut(mumonData.pot) && cuts.potDtCut(mumonData.dt_pot) ) {
			if (pointNum == 0) cout << "First data point at t=" << mumonData.time << endl;
			time[pointNum] = mumonData.time;
			signalx[pointNum] = mumonData.mm2x;
			signaly[pointNum] = mumonData.mm2y;
			if (mumonData.time < minTime) minTime = mumonData.time;
			if (mumonData.time > maxTime) maxTime = mumonData.time;
			// if (signal[pointNum] < minSig) minSig = signal[pointNum];
			// if (signal[pointNum] > maxSig) maxSig = signal[pointNum];
			// if (normedSignal[pointNum] < minNormedSig) minNormedSig = normedSignal[pointNum];
			// if (normedSignal[pointNum] > maxNormedSig) maxNormedSig = normedSignal[pointNum];
			pointNum++;
		}
	}
	ULong64_t numBins = (ULong64_t) (maxTime - minTime) / binWidth;
	cout << "numBins = " << numBins << endl;

	muMonXposVsTimePlotProfile = TProfile("muMonXposVsTimePlotProfile","MuMon posx vs Time (Fall 2018);Time;X Position (inches)", numBins, minTime - 1,maxTime + 1);
	muMonYposVsTimePlotProfile = TProfile("muMonYposVsTimePlotProfile","MuMon posy vs Time (Fall 2018);Time;Y Position (inches)", numBins, minTime - 1,maxTime + 1);
	for ( int i = 0; i < pointNum; i++ ) {
		muMonXposVsTimePlotProfile.Fill(time[i],signalx[i]);
		muMonYposVsTimePlotProfile.Fill(time[i],signaly[i]);
	}

	auto xaxisXpos = muMonXposVsTimePlotProfile.GetXaxis();
	xaxisXpos->SetTimeDisplay(1);
	xaxisXpos->SetNdivisions(-508);
	xaxisXpos->SetTimeFormat("%m/%d"); 
	xaxisXpos->SetTimeOffset(0,"utc");
	muMonXpositionCanvas->cd();
	muMonXposVsTimePlotProfile.Draw();

	auto xaxisYpos = muMonYposVsTimePlotProfile.GetXaxis();
	xaxisYpos->SetTimeDisplay(1);
	xaxisYpos->SetNdivisions(-508);
	xaxisYpos->SetTimeFormat("%m/%d"); 
	xaxisYpos->SetTimeOffset(0,"utc");
	muMonYpositionCanvas->cd();
	muMonYposVsTimePlotProfile.Draw();
}


void diamAnalysis::plotSignalvsMuMonYposCorrelation( int channel, double binWidthMuMonYpos, double binWidthSignal ) {
	TCanvas* sigVsMuMonYposCorrel = new TCanvas("sigVsMuMonYposCorrel", "Signal vs Muon Monitor 2 Y position Canvas (TProfile)", 30, 30, 1600, 900);
	data->GetEntry((int)entries/2);
	
	double minMuMonYpos = 0;
	double maxMuMonYpos = 0;
	double minSig = mumonData.total[channel] / mumonData.pot;
	double maxSig = mumonData.total[channel] / mumonData.pot;
	double muMonSignalY[entries];
	double signal[entries];
	int pointNum = 0;
	
	//correlation vars:
	double sigmaMuMonYposSquared = 0;
	double sigmaSignalSquared = 0;
	double sigmaMuMonYposSignal = 0;
	double muMonYposAvg = 0;
	double signalAvg = 0;
	for ( int i = 0; i < entries; i++ ) {
		data->GetEntry(i);
		if ( cuts.potCut(mumonData.pot) && cuts.potDtCut(mumonData.dt_pot) && cuts.positionCut(mumonData.mm2x, mumonData.mm2y) ) {
			muMonSignalY[pointNum] = mumonData.mm2y;
			signal[pointNum] = mumonData.total[channel] / mumonData.pot;
			if (muMonSignalY[pointNum] < minMuMonYpos) minMuMonYpos = muMonSignalY[pointNum];
			if (muMonSignalY[pointNum] > maxMuMonYpos) maxMuMonYpos = muMonSignalY[pointNum];
			if (signal[pointNum] < minSig) minSig = signal[pointNum];
			if (signal[pointNum] > maxSig) maxSig = signal[pointNum];
			muMonYposAvg += muMonSignalY[pointNum];
			signalAvg += signal[pointNum];
			pointNum++;
		}
	}
	muMonYposAvg = muMonYposAvg/( (double)pointNum - 1.0);
	signalAvg = signalAvg/( (double)pointNum - 1.0);

	int numBinsX = (int) round( ( maxMuMonYpos - minMuMonYpos ) / binWidthMuMonYpos );
	cout << "numBinsX = " << numBinsX << endl;
	int numBinsY = (int) round( ( maxSig - minSig ) / binWidthSignal );
	cout << "numBinsY = " << numBinsY << endl;
	cout << minMuMonYpos << "\t\t" << maxMuMonYpos << "\t\t" << minSig << "\t\t" << maxSig << endl;

	signalVsMuMonYposCorrelationPlot = TH2D("signalVsMuMonYposCorrelationPlot","Total Signal vs Muon Monitor Y position (Fall 2018);Y Position (inches);Signal / PoT", numBinsX, minMuMonYpos, maxMuMonYpos, numBinsY, minSig, maxSig );
	for ( int i = 0; i < pointNum; i++ ) {
		signalVsMuMonYposCorrelationPlot.Fill(muMonSignalY[i],signal[i]);

		sigmaMuMonYposSquared += pow( muMonSignalY[i] - muMonYposAvg, 2.0 );
		sigmaSignalSquared += pow( signal[i] - signalAvg, 2.0 );
		sigmaMuMonYposSignal += ( muMonSignalY[i] - muMonYposAvg )*( signal[i] - signalAvg );
	}

	double correlation = sigmaMuMonYposSignal / sqrt( sigmaMuMonYposSquared * sigmaSignalSquared );
	cout << "Correlation coefficient is: " << correlation << endl;
	sigVsMuMonYposCorrel->cd();
	signalVsMuMonYposCorrelationPlot.Draw("colz");
	signalVsMuMonYposCorrelationPlot.Write();
}



void diamAnalysis::plotNewDetectorSignalRatioProfile( double timePad, ULong64_t binWidth ) {
	TCanvas* newSigRatioProf = new TCanvas("newSigRatioProf", "Signal vs Time Canvas (TProfile)", 30, 30, 1600, 900);
	data->GetEntry((int)entries/2);
	ULong64_t minTime = mumonData.time;
	ULong64_t maxTime = mumonData.time;

	double minSig = mumonData.total[SILICONCHANNEL] / mumonData.total[NEWDIAMCHANNEL];
	double maxSig = mumonData.total[SILICONCHANNEL] / mumonData.total[NEWDIAMCHANNEL];
	ULong64_t time[entries];
	double signal[entries];
	int pointNum = 0;
	for ( int i = 0; i < entries; i++ ) {
		data->GetEntry(i);
		if ( cuts.potCut(mumonData.pot) && cuts.potDtCut(mumonData.dt_pot) ) {
			if (pointNum == 0) cout << "First data point at t=" << mumonData.time << endl;
			time[pointNum] = mumonData.time;
			signal[pointNum] = mumonData.total[SILICONCHANNEL] / mumonData.total[NEWDIAMCHANNEL];
			if (mumonData.time < minTime) minTime = mumonData.time;
			if (mumonData.time > maxTime) maxTime = mumonData.time;
			if (signal[pointNum] < minSig) minSig = signal[pointNum];
			if (signal[pointNum] > maxSig) maxSig = signal[pointNum];
			pointNum++;
		}
	}
	ULong64_t numBins = (ULong64_t) (maxTime - minTime) / binWidth;
	cout << "numBins = " << numBins << endl;

	newSignalRatioPlotProfile = TProfile("newSignalRatioPlotProfile","New Detector Signal Ratio;Time;Silicon Signal / Polycrystal Signal", numBins, minTime - 1,maxTime + 1);
	for ( int i = 0; i < pointNum; i++ ) {
		newSignalRatioPlotProfile.Fill(time[i],signal[i]);
	}

	auto xaxis = newSignalRatioPlotProfile.GetXaxis();
	xaxis->SetTimeDisplay(1);
	xaxis->SetNdivisions(-508);
	xaxis->SetTimeFormat("%m/%d");
	xaxis->SetTimeOffset(0,"utc");
	newSigRatioProf->cd();
	newSignalRatioPlotProfile.Draw();
}



void diamAnalysis::plotMuMonCellRatioProfile( int cell1, int cell2, double timePad, ULong64_t binWidth ) {
	TCanvas* muMonCellSigRatioProf = new TCanvas("muMonCellSigRatioProf", "Signal vs Time Canvas (TProfile)", 30, 30, 1600, 900);
	data->GetEntry((int)entries/2);
	ULong64_t minTime = mumonData.time;
	ULong64_t maxTime = mumonData.time;

	double minSig = 1;//mumonData.mma2cor[cell1] / mumonData.mma2cor[cell2];
	double maxSig = 1;//mumonData.mma2cor[cell1] / mumonData.mma2cor[cell2];
	ULong64_t time[entries];
	double signal[entries];
	int pointNum = 0;
	for ( int i = 0; i < entries; i++ ) {
		data->GetEntry(i);
		if ( cuts.potCut(mumonData.pot) && cuts.potDtCut(mumonData.dt_pot) ) {
			// if (pointNum == 0) cout << "First data point at t=" << mumonData.time << endl;
			time[pointNum] = mumonData.time;
			signal[pointNum] = mumonData.mma2cor[cell1] / mumonData.mma2cor[cell2];
			// cout << signal[pointNum] << endl;
			if (mumonData.time < minTime) minTime = mumonData.time;
			if (mumonData.time > maxTime) maxTime = mumonData.time;
			if (signal[pointNum] < minSig) minSig = signal[pointNum];
			if (signal[pointNum] > maxSig) maxSig = signal[pointNum];
			pointNum++;
		}
	}
	ULong64_t numBins = (ULong64_t) (maxTime - minTime) / binWidth;
	cout << "numBins = " << numBins << endl;

	muMonCellSigRatioPlotProfile = TProfile("muMonCellSigRatioPlotProfile","Muon Monitor 2 Signal Ratio;Time;Cell 1/ Cell 2", numBins, minTime - 1,maxTime + 1);
	for ( int i = 0; i < pointNum; i++ ) {
		muMonCellSigRatioPlotProfile.Fill(time[i],signal[i]);
	}

	auto xaxis = muMonCellSigRatioPlotProfile.GetXaxis();
	xaxis->SetTimeDisplay(1);
	xaxis->SetNdivisions(-508);
	xaxis->SetTimeFormat("%m/%d");
	xaxis->SetTimeOffset(0,"utc");
	muMonCellSigRatioProf->cd();
	muMonCellSigRatioPlotProfile.Draw();
}

void diamAnalysis::measureBeamWidth( int startEntry, int maxEntries, int numSpills, ULong64_t binWidth, double timePad, const char* basename ) {
	TCanvas *muMonArrayBeamWidthCanvas = new TCanvas("muMonArrayBeamWidthCanvas", "Muon Monitor Array Beam View", 30, 30, 1600, 900);
	muMonArrayBeamViewCanvas = new TCanvas("muMonArrayBeamViewCanvas", "Muon Monitor Array Beam View", 30, 30, 1600, 900);
	TF2 *fxygaus = new TF2("fxygaus","xygaus",1,9,1,9);

	int resultNum = 0;
	int eventNum = startEntry;
	int maxEntry = startEntry + maxEntries;
	while ( eventNum < entries && eventNum < maxEntry ) {
	// for (int i = 0; i < entries; i++) {
		data->GetEntry(eventNum);
		if ( cuts.potCut(mumonData.pot) && cuts.potDtCut(mumonData.dt_pot) ) {
			// eventNum = plotMuMonArrayBeamViewIntegral( eventNum, numSpills, "colz", 1);

			// muMonArrayBeamViewCanvas = new TCanvas("muMonArrayBeamViewCanvas", "Muon Monitor Array Beam View", 30, 30, 1600, 900);
			muMonArrayBeamViewIntegralTGraph2D = new TGraph2D(81);
			muMonArrayBeamViewIntegralTGraph2D->SetNpx(9);
			muMonArrayBeamViewIntegralTGraph2D->SetNpy(9);
			int pointNum = 0;
			double signal[81];
		    fill(signal, signal+81, 0);
		    int i = eventNum;
			for (i = eventNum; pointNum < numSpills; i++) {
				data->GetEntry( i );
				if ( cuts.potCut(mumonData.pot) && cuts.potDtCut(mumonData.dt_pot) ) {
					for (int j = 0; j < 81; j++) {
						if ( abs(mumonData.mma2cor[j]) < 10.0 ) {
							signal[j] += - mumonData.mma2cor[j]/mumonData.pot;
						}
					}
					pointNum++;
				}
			}
			for (Int_t i=0; i<81; i++) {
				muMonArrayBeamViewIntegralTGraph2D->SetPoint(i, muMonArrayMap[i][0], 10.0 - muMonArrayMap[i][1], signal[i]);
			}
			// muMonArrayBeamViewIntegralTGraph2D->SetTitle(Form("Muon Monitor 2 Signal for events %d through %d;Column;Row", eventNum, i-1));
			muMonArrayBeamViewIntegralTGraph2D->SetMarkerStyle(20);
			muMonArrayBeamViewCanvas->cd();
			muMonArrayBeamViewIntegralTGraph2D->Draw("colz");
			eventNum = i;


			muMonArrayBeamViewCanvas->cd();
			muMonArrayBeamViewIntegralTGraph2D->Fit(fxygaus,"Q");
			fitresult = (TF2*)muMonArrayBeamViewIntegralTGraph2D->FindObject("fxygaus");
			muMonArrayBeamViewIntegralTGraph2D->SetTitle(Form("Muon Monitor 2 Signal for events %d through %d, Sx=%f, Sy=%f, Ax=%f, Ay=%f;Column;Row", eventNum, i-1, fitresult->GetParameter(2), fitresult->GetParameter(4), fitresult->GetParameter(1), fitresult->GetParameter(3)));
			muMonArrayBeamViewIntegralTGraph2D->Draw("colz");
			fitresult->Draw("same");
			for (int k = 0; k < 5; k++) {
				beamMeasurementResults[resultNum][k] = fitresult->GetParameter(k);
				// cout << beamMeasurementResults[resultNum][k] << endl;
			}
			
			// fitresult->Draw("surf1");
			// muMonArrayBeamViewIntegralTGraph2D->Fit("fxygaus");
			// muMonArrayBeamViewIntegralTGraph2D->Draw("colz");
			muMonArrayBeamViewCanvas->SaveAs(Form("%s%d.png",basename,eventNum));
			resultNum++;
		}
		eventNum++;
	}

	// muMonArrayBeamViewIntegralTGraph2D->SetTitle("Muon Monitor 2 Integrated Signal;Column;Row");
	// muMonArrayBeamViewIntegralTGraph2D->SetMarkerStyle(20);
	// muMonArrayBeamWidthCanvas->cd();
	// muMonArrayBeamViewIntegralTGraph2D->Draw(drawOption);
}

void diamAnalysis::makeAllPlots() {
	plotSignalVsTime( 3, 43200.0 );
	plotSignalVsTimeProfile( 3, 43200.0, 43200.0 );
	plotSignalVsPotCorrelation( 3, 0.05, 100.0 );
	plotSignalVsTemp1Correlation( 3, 0.1, 10.0 );
	plotSignalVsTemp1( 3, 0 );
	plotSignalVsTemp1Profile( 3, 0, 0.5 );
	plotMuMonCellVsTime(91,3600*12,3600*24);
	plotSignalvsMuMonYposCorrelation( 3,0.02, 10.0 );
	plotNewDetectorSignalRatioProfile( 3600*12,3600*12 );
}

void diamAnalysis::savePlots( char * filename ) {
	// TFile *file = new TFile(filename,"RECREATE");
	if ( file->IsOpen() ) {
		signalVsTimePlot.Write("signalVsTimePlot");
		signalVsTemp1Plot.Write("signalVsTemp1Plot");
		signalVsTimePlotProfile.Write("signalVsTimePlotProfile");
		signalVsTemp1PlotProfile.Write("signalVsTemp1PlotProfile");
		muMonCellVsTimePlotProfile.Write("muMonCellVsTimePlotProfile");
		muMonCellNormedVsTimePlotProfile.Write("muMonCellNormedVsTimePlotProfile");
		muMonXposVsTimePlotProfile.Write("muMonXposVsTimePlotProfile");
		muMonYposVsTimePlotProfile.Write("muMonYposVsTimePlotProfile");
		signalVsPotCorrelationPlot.Write("signalVsPotCorrelationPlot");
		signalVsTemp1CorrelationPlot.Write("signalVsTemp1CorrelationPlot");
		signalVsMuMonYposCorrelationPlot.Write("signalVsMuMonYposCorrelationPlot");
		muMonArrayBeamViewTGraph2D->Write("muMonArrayBeamViewTGraph2D");
		file->Write();
		file->Close();
	} else {
		cout << "File open failed." << endl;
	}
}

template<class T> void diamAnalysis::SetRange( T& obj, char axis, double min, double max ) {
	if (axis == 'x') {
		auto xaxis = obj.GetXaxis();
		xaxis->SetRangeUser(min, max);
	} else if (axis == 'y') {
		auto yaxis = obj.GetYaxis();
		yaxis->SetRangeUser(min, max);	
	} else {
		cout << "Please specify a valix axis, either \"x\" or \"y\"" << endl;
	}
	
}

diamAnalysis::diamAnalysis(  char * dataFiles, const char * outputFilename ) {
	data = new TChain(DEFAULT_TTREE_NAME);

	// oldData->Add("~/work/lnxdata41/dune/njohnston/numi_diamond_data_conversion/data/results/Results_2017_11.root");
	// oldData->Add("~/work/lnxdata41/dune/njohnston/numi_diamond_data_conversion/data/results/Results_2017_12.root");


	char * pch;
	pch = strtok (dataFiles," ,");
	while (pch != NULL) {
		data->Add(pch);
		printf ("Adding %s to the old data TTree\n",pch);
		pch = strtok (NULL, " , ");
	}

	entries = data->GetEntries();

	setBranches();
	setCuts();
	file = new TFile(outputFilename,"RECREATE");
}

void diamAnalysis::setBranches() {
	data->SetBranchAddress("timeD",&this->mumonData.timeD);
	data->SetBranchAddress("time",&this->mumonData.time);
	data->SetBranchAddress("usec",&this->mumonData.usec);
	data->SetBranchAddress("mean",this->mumonData.mean);
	data->SetBranchAddress("rms",this->mumonData.rms);
	data->SetBranchAddress("start",this->mumonData.start);
	data->SetBranchAddress("startBin",this->mumonData.startBin);
	data->SetBranchAddress("nbm",&this->mumonData.nb);
	data->SetBranchAddress("integ",this->mumonData.integ);
	data->SetBranchAddress("total",this->mumonData.total);
	data->SetBranchAddress("bucketMin",this->mumonData.bucketMin);
	data->SetBranchAddress("bucketMax",this->mumonData.bucketMax);
	data->SetBranchAddress("min",this->mumonData.min);
	data->SetBranchAddress("max",this->mumonData.max);
	data->SetBranchAddress("rtimeD",&this->mumonData.rtimeD);
	data->SetBranchAddress("rtime",&this->mumonData.rtime);
	data->SetBranchAddress("rusec",&this->mumonData.rusec);
	data->SetBranchAddress("rmean",&this->mumonData.rmean);
	data->SetBranchAddress("rrms",&this->mumonData.rrms);
	data->SetBranchAddress("rstart",&this->mumonData.rstart);
	data->SetBranchAddress("rstartBin",&this->mumonData.rstartBin);
	data->SetBranchAddress("nbr",&this->mumonData.rnb);
	data->SetBranchAddress("rinteg",this->mumonData.rinteg);
	data->SetBranchAddress("rtotal",this->mumonData.rtotal);
	data->SetBranchAddress("rbucketMin",this->mumonData.rbucketMin);
	data->SetBranchAddress("rbucketMax",&this->mumonData.rbucketMax);
	data->SetBranchAddress("rmin",&this->mumonData.rmin);
	data->SetBranchAddress("rmax",&this->mumonData.rmax);
	//data->SetBranchAddress("acnet",&this->acnet);
	data->SetBranchAddress("pot",&this->mumonData.pot);
	data->SetBranchAddress("dt_pot",&this->mumonData.dt_pot);
	data->SetBranchAddress("pot2",&this->mumonData.pot2);
	data->SetBranchAddress("dt_pot2",&this->mumonData.dt_pot2);
	data->SetBranchAddress("T1",&this->mumonData.T1);
	data->SetBranchAddress("T2",&this->mumonData.T2);
	data->SetBranchAddress("pitch",&this->mumonData.pitch);
	data->SetBranchAddress("yaw",&this->mumonData.yaw);
	data->SetBranchAddress("Palc",&this->mumonData.Palc);
	data->SetBranchAddress("Ppump",&this->mumonData.Ppump);
	data->SetBranchAddress("Pmks",&this->mumonData.Pmks);
	data->SetBranchAddress("Par",&this->mumonData.Par);
	data->SetBranchAddress("dt_mu",&this->mumonData.dt_mu);
	data->SetBranchAddress("lineA",&this->mumonData.lineA);
	data->SetBranchAddress("lineB",&this->mumonData.lineB);
	data->SetBranchAddress("lineC",&this->mumonData.lineC);
	data->SetBranchAddress("lineD",&this->mumonData.lineD);
	data->SetBranchAddress("hornI",&this->mumonData.hornI);
	data->SetBranchAddress("tttgth",&this->mumonData.tttgth);
	data->SetBranchAddress("dt_horn",&this->mumonData.dt_horn);
	data->SetBranchAddress("mm1",&this->mumonData.mm1);
	data->SetBranchAddress("mm2",&this->mumonData.mm2);
	data->SetBranchAddress("mm3",&this->mumonData.mm3);
	data->SetBranchAddress("mm1x",&this->mumonData.mm1x);
	data->SetBranchAddress("mm1y",&this->mumonData.mm1y);
	data->SetBranchAddress("mm2x",&this->mumonData.mm2x);
	data->SetBranchAddress("mm2y",&this->mumonData.mm2y);
	data->SetBranchAddress("mm1cor",&this->mumonData.mm1cor);
	data->SetBranchAddress("mm2cor",&this->mumonData.mm2cor);
	data->SetBranchAddress("mm3cor",&this->mumonData.mm3cor);
	data->SetBranchAddress("mm1gas",&this->mumonData.mm1gas);
	data->SetBranchAddress("mm2gas",&this->mumonData.mm2gas);
	data->SetBranchAddress("mm3gas",&this->mumonData.mm3gas);
	data->SetBranchAddress("dt_mm",&this->mumonData.dt_mm);
	data->SetBranchAddress("dt_mmAltTiming",&this->mumonData.dt_mmAltTiming);
	data->SetBranchAddress("gasMon2",&this->mumonData.gasMon2);
	data->SetBranchAddress("tgtX",&this->mumonData.tgtx);
	data->SetBranchAddress("tgtY",&this->mumonData.tgty);
	data->SetBranchAddress("dt_bpm",&this->mumonData.dt_bpm);

	//Arrays
	data->SetBranchAddress("dt_mma1",&this->mumonData.dt_mma1);
	data->SetBranchAddress("mma1ds",this->mumonData.mma1ds);
	data->SetBranchAddress("mma1pd",this->mumonData.mma1pd);
	data->SetBranchAddress("mma1cor",this->mumonData.mma1cor);
	data->SetBranchAddress("mma2ds",this->mumonData.mma2ds);
	data->SetBranchAddress("mma2pd",this->mumonData.mma2pd);
	data->SetBranchAddress("mma2cor",this->mumonData.mma2cor);
}

void diamAnalysis::setCuts() {
	D1sigCut15 = TString::Format("(total[2]/pot) > %f && (total[2]/pot) < %f",-130e3,-80e3);
	D1sigCut16 = TString::Format("(total[1]/pot) > %f && (total[1]/pot) < %f",-130e3,-90e3);
	D2sigCut15 = TString::Format("(total[2]/pot) > %f && (total[2]/pot) < %f",-120e3,-80e3);
	D2sigCut16 = TString::Format("(total[2]/pot) > %f && (total[2]/pot) < %f",-125e3,-85e3);
	potCut = TString::Format("pot > %f",15.);
	DTpotCut = TString::Format("dt_pot > %f && dt_pot < %f",-1.,1.);
	DThornCut = TString::Format("dt_horn > %f && dt_horn < %f",-1.,1.);
	hornCut = TString::Format("hornI < %f", -190.);
	antiTimeCut = TString::Format("timeD < %f", antii);
	badHornTimeCut = TString::Format("timeD < %f", badhorni);
	turnOntimeCut16 = TString::Format("timeD < %f", turnonf16);
}

diamAnalysis::~diamAnalysis()
{
//	if (!fChain) return;
//	delete fChain->GetCurrentFile();
}

#endif
