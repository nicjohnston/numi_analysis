// #ifndef muonData_h
// #define muonData_h

// #include <TROOT.h>
// #include <TChain.h>
// #include <TFile.h>
// #include <Riostream.h>
// #include <fstream>
// #include "TH1.h"
// #include "TH1D.h"

// #include "TF1.h"
// #include "TH1.h"
// #include "TMath.h"
// #include "TFitResult.h"

// #include "TError.h"
// #include <math.h>
// #include <string.h>
// #include <algorithm>
// #include "TString.h"

// #include <Math/SpecFuncMathCore.h>
// #include <Math/PdfFuncMathCore.h>
// #include <Math/ProbFuncMathCore.h>

// #include "TCanvas.h"
// #include <TGraph.h>


using namespace std;

class Cuts {
public:
	Cuts();

	int potCut( double potVal );
	int potDtCut( double potDtVal );
	int positionCut( double xpos, double ypos );

	virtual ~Cuts() {  };

	double minPot;
	double minPotDt;
	double maxPotDt;
	double minMuMonPos;
	double maxMuMonPos;
};

Cuts::Cuts() {
	minPot = 15.0;
	maxPotDt = 1.0;
	minPotDt = -1.0;
	minMuMonPos = -4.0;
	maxMuMonPos = 4.0;
}

int Cuts::potCut( double potVal ) {
	if ( potVal > minPot) {
		return 1;
	} else {
		return 0;
	}
}

int Cuts::potDtCut( double potDtVal ) {
	if ( potDtVal < maxPotDt && potDtVal > minPotDt) {
		return 1;
	} else {
		return 0;
	}
}

int Cuts::positionCut( double xpos, double ypos ) {
	if ( xpos < maxMuMonPos && xpos > minMuMonPos && ypos < maxMuMonPos && ypos > minMuMonPos ) {
		return 1;
	} else {
		return 0;
	}
}


// #endif