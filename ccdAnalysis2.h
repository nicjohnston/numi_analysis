#ifndef ccdAnalysis2_h
#define ccdAnalysis2_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <Riostream.h>
#include <fstream>
#include "TH1.h"
#include "TH1D.h"

#include "TF1.h"
#include "TH1.h"
#include "TMath.h"
#include "TFitResult.h"

#include "TError.h"
#include <math.h>
#include <string.h>
#include <algorithm>
#include "TString.h"

#include <Math/SpecFuncMathCore.h>
#include <Math/PdfFuncMathCore.h>
#include <Math/ProbFuncMathCore.h>

#include "TCanvas.h"
#include <TGraph.h>

#define DEFAULT_TTREE_NAME		"rawData"

using namespace std;

class ccdData {
public:
	ccdData() { };
	void setValues( Double_t	_computed_ccd, Double_t _computed_ccd_error, Double_t _lowerBound_time, Double_t _lowerBound_index, Double_t _upperBound_time, Double_t _upperBound_index) {
		computed_ccd = _computed_ccd;
		computed_ccd_error = _computed_ccd_error;
		lowerBound_time = _lowerBound_time;
		lowerBound_index = _lowerBound_index;
		upperBound_time = _upperBound_time;
		upperBound_index = _upperBound_index;
	};
	void drawHist() { histogram->Draw(); };
	virtual ~ccdData() {  };
	TH1D		*histogram;
	Double_t	computed_ccd;
	Double_t	computed_ccd_error;
	Double_t	lowerBound_time;
	int			lowerBound_index;
	Double_t	upperBound_time;
	int			upperBound_index;
	
	TF1 * user;
	TF1 * user_dup;

	TFitResultPtr r1;  // first fit
	TFitResultPtr r2;  // second fit
	
	TF1 * f1;
	
	Double_t lognormal_sigma;
	Double_t lognormal_m;
	Double_t lognormal_x0;
	
	Double_t lognormal_mean;
};

class ccdAnalysis2 {
public :
	ccdAnalysis2( ) { };
	ccdAnalysis2( const char * filename );
	ccdAnalysis2( const char * filename, const char * rootFilename );
	int readRawDataToTTreev2( const char * rawDataFilename, const char * rootTTreeFilename, const char * rootTTreeName = DEFAULT_TTREE_NAME );
	Double_t getRawMean( const char* _ChannelName = "corInt");
	
	int loadROOTfile( const char * rootTTreeFilename, const char * rootTTreeName );
	int initTTree();
	int initVars();
	Double_t  calcCCD( int startIndex = 0, int indexInterval = -1, Double_t fp0 = 4e-5, Double_t fp1 = 0.5, Double_t fp2 = 5e-8, Double_t fp3 = 0, Double_t uflb = 0.01e-6 );
	int makeh1( Int_t numBins, Double_t lb, Double_t ub, Int_t startIndex, Int_t endIndex );
	void test( int rawMean);
	
	int calcCCDv2( ccdData &retData, int _startIndex = 0, int _indexInterval = -1, double _timeInterval = 1800, int _histIndex = 0 );
	Double_t makeHist( TH1D *&hist, double &channel, Int_t numBins, Double_t lb, Double_t ub, Int_t startIndex, Int_t endIndex, const char * histName, const char * histTitle );
//	Double_t makeHist( TH1D *&hist, Int_t numBins, Double_t lb, Double_t ub, Int_t startIndex, Int_t endIndex, const char * histName );
//	Double_t LogNormal2(Double_t x, Double_t sigma, Double_t theta, Double_t m);

	int calcCCDv3( ccdData *retData, int _startIndex = 0, int _indexInterval = -1, double _timeInterval = 1800, int _histIndex = 0 );


	void ccdTimeSweep( int startIndex, int timeInterval, int upperLimit );
	void ccdTimeSweepV2( const char * filename, const char * treeName, int startIndex, int timeInterval, int upperLimit );
	int calcCCDTTree( int _startIndex, int _indexInterval, double _timeInterval, int _histIndex );
	void initAnalysisVars(int read = 0 );
	void ReadccdTimeSweep( const char * filename, const char * treeName, int startIndex, int timeInterval, int upperLimit );
	void saveccdGraph_corInt( const char * baseFilename = "graphSave", const char * graphLabels = "graph title;x title;y title" );
	
	Double_t calcCCDTTree2( int _startIndex, int _indexInterval, double _timeInterval, int _histIndex );


	Int_t findIndexBounds( int _startIndex, int _indexInterval, double _timeInterval );
	void refTest(const char * name);
	Double_t calcCCDTTree3( int _startIndex, int _endIndex, int _histIndex, const char* _channelName, Double_t fp1, Double_t fp2, Double_t fp3, Double_t fp4 );
	void setBounds( Double_t _FitBounds[4], Double_t _HistParams[3], Double_t _initFitParams[4], char _LogNormalFitFormula[128], const char *_ChannelName, int selfTrig = 0, int silicon = 0 );
	Double_t makeHistFromChannel( TH1D *&hist, Int_t numBins, Double_t lb, Double_t ub, Int_t startIndex, Int_t endIndex, const char *_ChannelName, const char * histName, const char * histTitle );
	Double_t calcCCDTTree4( int _startIndex, int _endIndex, int _histIndex, const char* _channelName, int debug = 0, int selfTrig = 0, int silicon = 0, Double_t fp1 = 0, Double_t fp2 = 0, Double_t fp3 = 0, Double_t fp4 = 0 );
	Double_t calcCCDTTree4_fixedGaus( int _startIndex, int _endIndex, int _histIndex, const char* _channelName, int debug = 0, Double_t fp1 = 0, Double_t fp2 = 0, Double_t fp3 = 0, Double_t fp4 = 0 );

	virtual ~ccdAnalysis2();
   
//private:
	TFile			*dataFile;
	TTree			*dataTTree;   //!pointer to the ccdAnalysis2d TTree or TChain
	TFile			*analysisFile;
	TTree			*analysisTTree;   //!pointer to the ccdAnalysis2d TTree or TChain
	Long64_t		nentries;
	
	TH1D			*h1;
	TH1D			*fullHist_corInt;
	TH1D			*fullHist_postInt;
	
	
	
	TGraph* graph1;
	
	vector<ccdData> fitData;

	double timestamp, preInt, postInt, LVcorInt, corInt, timeElapsed;

	// List of branches
	TBranch			*b_timestamp;
	TBranch			*b_preInt;
	TBranch			*b_postInt;
	TBranch			*b_LVcorInt;
	TBranch			*b_corInt;
	TBranch			*b_timeElapsed;
	
	
	
	
	//variables for the V4 approach
	
	TH1D		*fitHist_corInt;
	TF1			*userFit_corInt;
	TF1			*userDupFit_corInt;
	TF1			*gausFit_corInt;
	TF1			*combFit_corInt;
	Double_t	ccd_corInt, ccdErr_corInt, lowBndTime, uppBndTime, sigma_val_corInt, m_val_corInt, x0_val_corInt, mean_val_corInt, gaus_m_val_corInt;
	Int_t		lowBndIndex, uppBndIndex;
	TGraph		*ccdGraph_corInt;
	TGraph		*backgroundGraph_corInt;
	TGraph		*meanGraph_corInt;
	TGraph		*correctedMeanGraph_corInt;
	
	
	TBranch		*b_fitHist_corInt;
	TBranch		*b_userFit_corInt;
	TBranch		*b_userDupFit_corInt;
	TBranch		*b_gausFit_corInt;
	TBranch		*b_combFit_corInt;
	TBranch		*b_ccd_corInt;
	TBranch		*b_ccdErr_corInt;
	TBranch		*b_lowBndTime;
	TBranch		*b_uppBndTime;
	TBranch		*b_sigma_val_corInt;
	TBranch		*b_m_val_corInt;
	TBranch		*b_x0_val_corInt;
	TBranch		*b_mean_val_corInt;
	TBranch		*b_lowBndIndex;
	TBranch		*b_uppBndIndex;
	TBranch		*b_ccdGraph_corInt;
	TBranch		*b_backgroundGraph_corInt;
	TBranch		*b_meanGraph_corInt;
	TBranch		*b_correctedMeanGraph_corInt;
	
	//variables for the extended analysis
	TH1D		*fitHist_postInt;
	TF1			*userFit_postInt;
	TF1			*userDupFit_postInt;
	TF1			*gausFit_postInt;
	TF1			*combFit_postInt;
	Double_t	ccd_postInt, ccdErr_postInt, sigma_val_postInt, m_val_postInt, x0_val_postInt, mean_val_postInt, gaus_m_val_postInt, mean_val_adj_postInt;
	TGraph		*ccdGraph_postInt;
	TGraph		*backgroundGraph_postInt;
	TGraph		*mVal_diffGraph_postInt;
	TGraph		*meanGraph_postInt;
	TGraph		*correctedMeanGraph_postInt;

	
	
	TBranch		*b_fitHist_postInt;
	TBranch		*b_userFit_postInt;
	TBranch		*b_userDupFit_postInt;
	TBranch		*b_gausFit_postInt;
	TBranch		*b_combFit_postInt;
	TBranch		*b_ccd_postInt;
	TBranch		*b_ccdErr_postInt;
	TBranch		*b_sigma_val_postInt;
	TBranch		*b_m_val_postInt;
	TBranch		*b_x0_val_postInt;
	TBranch		*b_mean_val_postInt;
	TBranch		*b_gaus_m_val_postInt;
	TBranch		*b_mean_val_adj_postInt;
	TBranch		*b_ccdGraph_postInt;
	TBranch		*b_backgroundGraph_postInt;
	TBranch		*b_mVal_diffGraph_postInt;
	TBranch		*b_meanGraph_postInt;
	TBranch		*b_correctedMeanGraph_postInt;

	TH1D		*fitHist_preInt;
	TF1			*gausFit_preInt;
	Double_t	mean_val_preInt;
	TGraph		*backgroundGraph_preInt;
	
	
	TBranch		*b_fitHist_preInt;
	TBranch		*b_gausFit_preInt;
	TBranch		*b_mean_val_preInt;
	TBranch		*b_backgroundGraph_preInt;
};


//#endif

//#ifdef ccdAnalysis_cxx

ccdAnalysis2::ccdAnalysis2( const char * filename ) {

	this->initVars();

	if (strstr(filename, ".root")) {
//		cout << "you gave me a root file" << endl;
		loadROOTfile( filename, DEFAULT_TTREE_NAME );
	} else {
		cout << "you gave me a file that is not a root file.  Please use the constructor with the rootFilename argument" << endl;
//		~ccdAnalysis2();		                  
	}
}

ccdAnalysis2::ccdAnalysis2( const char * filename, const char * rootFilename ) {
	//initialize variables
	this->initVars();
	
	if (strstr(filename, ".root")) {
		cout << ".root file given, switching to loading the TTree" << endl;
//		ccdAnalysis2( filename );
	} else {
		cout << "you gave me a file that is not a root file.  I am going to assume it is a raw data file and process it into a .root file of the given name" << endl;
		readRawDataToTTreev2( filename, rootFilename, DEFAULT_TTREE_NAME );  
	}
}

int ccdAnalysis2::loadROOTfile( const char * rootTTreeFilename, const char * rootTTreeName ) {
	this->dataFile = new TFile( rootTTreeFilename );
	this->dataTTree = (TTree*)dataFile->Get(DEFAULT_TTREE_NAME);
	this->initTTree();
	
	if (dataTTree->GetEntriesFast() > 0) return 1;
	else {
		cout << "failure1" << endl;
		return 0;
	}
}


int ccdAnalysis2::readRawDataToTTreev2( const char * rawDataFilename, const char * rootTTreeFilename, const char * rootTTreeName ) {

	ifstream in;
	in.open(rawDataFilename);

	this->dataFile = new TFile( rootTTreeFilename , "RECREATE");
	this->dataTTree = new TTree( rootTTreeName, "" );
//	this->initTTree();

	b_timestamp = dataTTree->Branch("timestamp", &timestamp, "timestamp/D");
	b_preInt = dataTTree->Branch("preInt", &preInt, "preInt/D");
	b_postInt = dataTTree->Branch("postInt", &postInt, "postInt/D");
	b_LVcorInt = dataTTree->Branch("LVcorInt", &LVcorInt, "LVcorInt/D");
	b_corInt = dataTTree->Branch("corInt", &corInt, "corInt/D");
	b_timeElapsed = dataTTree->Branch("timeElapsed", &timeElapsed, "timeElapsed/D");
	
	this->nentries = dataTTree->GetEntries();
	
	char line[64];
	cout << endl << endl;
	Long64_t counter = 0;
	
	double timeOffset = 0;
	
	while (1) {
		in >> timestamp >> preInt >> postInt >> LVcorInt;
//		timestamp /= 10000000;
		preInt /= 10000000;
		postInt /= 10000000;
		LVcorInt /= 10000000;
		corInt = postInt - preInt;
		
		if (counter == 0) {
			timeOffset = timestamp;
//			timeElapsed = 0;
		}
		
		timeElapsed = timestamp - timeOffset;
		
		dataTTree->Fill();
		counter++;

		if (counter < 5) printf("timestamp=%.016e,preInt=%.08e,postInt=%.08e,LVcorInt=%.08e,corInt=%.08e,timeElapsed=%.08e\n",timestamp, preInt, postInt, LVcorInt, corInt, timeElapsed);
		else cout << "\r" << counter << flush;
		
		if (in.eof()) break; 
		if (in.fail()) {
			cout << "failure" << endl;
			return 0;
		}
	}
	dataTTree->Print();
	dataFile->Write();
	
	this->nentries = dataTTree->GetEntries();
	
	if (dataTTree->GetEntriesFast() > 0) return 1;
	else return 0;
}

int ccdAnalysis2::initTTree() {

//	b_timestamp = dataTTree->Branch("timestamp", &timestamp, "timestamp/D");
//	b_preInt = dataTTree->Branch("preInt", &preInt, "preInt/D");
//	b_postInt = dataTTree->Branch("postInt", &postInt, "postInt/D");
//	b_LVcorInt = dataTTree->Branch("LVcorInt", &LVcorInt, "LVcorInt/D");
//	b_corInt = dataTTree->Branch("corInt", &corInt, "corInt/D");
	
	dataTTree->SetBranchAddress("timestamp", &timestamp, &b_timestamp);
	dataTTree->SetBranchAddress("preInt", &preInt, &b_preInt);
	dataTTree->SetBranchAddress("postInt", &postInt, &b_postInt);
	dataTTree->SetBranchAddress("LVcorInt", &LVcorInt, &b_LVcorInt);
	dataTTree->SetBranchAddress("corInt", &corInt, &b_corInt);
	dataTTree->SetBranchAddress("timeElapsed", &timeElapsed, &b_timeElapsed);
	
	this->nentries = dataTTree->GetEntries();
	
}

int ccdAnalysis2::initVars() {
	this->fullHist_corInt = new TH1D("fullHist_corInt","",2400,-0.2e-6,0.2e-6);
	this->fullHist_postInt = new TH1D("fullHist_corInt","",3000,-2e-6,-1.5e-6);
	this->timestamp, preInt, postInt, LVcorInt, corInt, timeElapsed = 0;
}

void ccdAnalysis2::test( int rawMean ) {
		cout << "test1" << endl;
	char logNormalFitFormula[128] = {0};
//	Double_t rawMean = this->getRawMean();
	if (rawMean > 0) {
		//set params for negative supply voltage
//		logNormalFitFormula = "[0]*TMath::LogNormal((x>=[3])*(x)+(x<[3])*[3],[1],[3],[2])";
		strlcpy(logNormalFitFormula, "[0]*TMath::LogNormal((x>=[3])*(x)+(x<[3])*[3],[1],[3],[2])", 128);
	} else if (rawMean < 0) {
		//set params ofr positive supply voltage
//		logNormalFitFormula = "[0]*TMath::LogNormal((x<=[3])*(-1*x)+(x>[3])*[3],[1],[3],[2])";
		strlcpy(logNormalFitFormula, "[0]*TMath::LogNormal((x<=[3])*(-1*x)+(x>[3])*[3],[1],[3],[2])", 128);
	} else {
		cout << "error, rawMean equals zero: " << rawMean << endl;
		strlcpy(logNormalFitFormula, "0", 128);
	}
	puts(logNormalFitFormula);
}

// Double_t LogNormal2(Double_t x, Double_t sigma, Double_t theta, Double_t m) {
// 	if ((x<theta) || (sigma<=0) || (m<=0)) {
// //		Error("TMath::Lognormal", "illegal parameter values");
// 		return 0;
// 	}
// 	return ::ROOT::Math::lognormal_pdf(x, TMath::Log(m), sigma, theta);
// }

Double_t ccdAnalysis2::makeHist( TH1D *&hist, double &channel, Int_t numBins, Double_t lb, Double_t ub, Int_t startIndex, Int_t endIndex, const char * histName, const char * histTitle ) {
//Double_t ccdAnalysis2::makeHist( TH1D *&hist, Int_t numBins, Double_t lb, Double_t ub, Int_t startIndex, Int_t endIndex, const char * histName ) {

	hist = new TH1D(histName, histTitle, numBins, lb, ub);
	hist->GetXaxis()->SetTitle("Vsec");
	hist->GetYaxis()->SetTitle("Counts");
	Long64_t i = 0;
	for (i = startIndex; i < this->nentries && i < endIndex; i++) {
		this->dataTTree->GetEntry(i);
		hist->Fill(channel);
	}
	
	return hist->GetEntries();
}

int ccdAnalysis2::calcCCDv2( ccdData &retData, int _startIndex, int _indexInterval, double _timeInterval, int _histIndex ) {
//	if (indexInterval == -1 && nentries > 0) indexInterval = nentries;	
//	ccdData retData;
	Double_t fitBounds[4] = {0}; // r1l, r1h, r2l, r2h
	Double_t histParams[3] = {0}; // numbins, lb, ub
	char logNormalFitFormula[128] = {0};
	Double_t rawMean = this->getRawMean();
	if (rawMean > 0) {
		//set params for negative supply voltage
		strlcpy(logNormalFitFormula, "[0]*LogNormal2(x,[1],[3],[2])", 128); // from findMean4
		fitBounds[0] = -0.05e-6;
		fitBounds[1] = 0.01e-6;
		fitBounds[2] = 0.01e-6;
		fitBounds[3] = 0.15e-6;
		histParams[0] = 1200;
		histParams[1] = -0.05e-6;
		histParams[2] = 0.15e-6;
		
	} else if (rawMean < 0) {
		strlcpy(logNormalFitFormula, "[0]*LogNormal2((x<=[3])*(-1*x)+(x>[3])*[3],[1],[3],[2])", 128); // from findMean4
		fitBounds[0] = -0.04e-6;
		fitBounds[1] = 0.04e-6;
		fitBounds[2] = -0.15e-6;
		fitBounds[3] = -0.02e-6;
		histParams[0] = 1200;
		histParams[1] = -0.15e-6;
		histParams[2] = 0.05e-6;
	} else {
		cout << "error, rawMean equals zero: " << rawMean << endl;
		strlcpy(logNormalFitFormula, "0", 128);
	}
	
//	makeh1(histParams[0], histParams[1], histParams[2], startIndex, endIndex);
//	Int_t startIndex = _startIndex;
	Int_t endIndex = 0;
	if ( _indexInterval == -1) {
//		endIndex = findEnd(corInt, _startIndex, _timeInterval);
		Long64_t i = _startIndex;
		this->dataTTree->GetEntry(i);
		double initialTime = timeElapsed;
		double targetTime = initialTime + _timeInterval;
		double timeDiff = 0;
	//	for (i = _start; i < this->nentries && i < endIndex; i++) {
		while ( this->dataTTree->GetEntry(i) ) {
			if (targetTime >= timeElapsed) {
				timeDiff = timeElapsed - targetTime;
//				if (timeDiff > 300) {
//					
//				}
				i++;
			} else {
//			cout << "breaking" << endl;
				break;
			}
		}
//		this->dataTTree->GetEntry(i-1);
//cout << "fuck: " << i << endl;
		endIndex = i - 1;
	} else {
		endIndex = _startIndex + _indexInterval;
	}
	
	const char *histName = Form("h%d", _histIndex);
	this->dataTTree->GetEntry(endIndex);
	const char *histTitle = Form("Diamond Signal Integral Histogram at %e seconds", timeElapsed);
	
	retData.upperBound_time = timeElapsed;
	retData.upperBound_index = endIndex;
	this->dataTTree->GetEntry(_startIndex);
	retData.lowerBound_time = timeElapsed;
	retData.lowerBound_index = _startIndex;

	cout << "haha: " << timestamp << endl ;
	cout << "si: " << _startIndex << endl;
	cout << "ei" << endIndex << endl;
	cout << "iI" << _indexInterval << endl;
	
	makeHist( retData.histogram, corInt, histParams[0], histParams[1], histParams[2], _startIndex, endIndex, histName, histTitle );

	retData.user = new TF1("user", logNormalFitFormula, fitBounds[2], fitBounds[3]);
	retData.user_dup = new TF1("user_dup", logNormalFitFormula, fitBounds[2]-0.05e-6, fitBounds[3]+0.05e-6);
	retData.user->SetParameters(1e-8, 0.5, 5e-8, 0);


	retData.r1 = retData.histogram->Fit("gaus","SQ","", fitBounds[0], fitBounds[1]);  // first fit
	retData.r2 = retData.histogram->Fit("user","SQ","", fitBounds[2], fitBounds[3]);  // second fit
	
	retData.f1 = new TF1("fitFunc","gaus+user");
	// get parameters and set in global TF1
	// parameters of first gaussian
	retData.f1->SetParameter(0,retData.r1->Parameter(0));
	retData.f1->SetParameter(1,retData.r1->Parameter(1));
	retData.f1->SetParameter(2,retData.r1->Parameter(2));
	// parameters of lognormal
	retData.f1->SetParameter(3,retData.r2->Parameter(0));
	retData.f1->SetParameter(4,retData.r2->Parameter(1));
	retData.f1->SetParameter(5,retData.r2->Parameter(2));

	retData.histogram->Fit(retData.f1,"Q");
	
	retData.user_dup->SetParameter(0,retData.f1->GetParameter(3));
	retData.user_dup->SetParameter(1,retData.f1->GetParameter(4));
	retData.user_dup->SetParameter(2,retData.f1->GetParameter(5));
	retData.user_dup->SetParameter(3,retData.f1->GetParameter(6));
	
	retData.lognormal_sigma = retData.user_dup->GetParameter(1);
	retData.lognormal_m = retData.user_dup->GetParameter(2);
	retData.lognormal_x0 = retData.user_dup->GetParameter(3);
	
	retData.lognormal_mean = retData.lognormal_m*TMath::Exp(0.5*TMath::Power(retData.lognormal_sigma,2))+retData.lognormal_x0;
	retData.computed_ccd = retData.lognormal_mean*1000/(32e-8);
	
//	cout << endl << endl << "by method 1, the ccd is: \t\t" << ccd << " microns \t\tfrom a mean of " << lognormal_mean << endl << endl << endl;
//	cout << endl << endl << "by method 2, the ccd is: \t\t" << (user->GetParameter(2) * TMath::Exp(0.5*TMath::Power(user->GetParameter(1),2)) + user->GetParameter(3))*1000/(32e-8) << " microns \t\tfrom a mean of " << (user->GetParameter(2) * TMath::Exp(0.5*TMath::Power(user->GetParameter(1),2)) + user->GetParameter(3)) << endl << endl << endl;
//	return &retData;
//	return ccdData();
	return 1;
}




int ccdAnalysis2::calcCCDv3( ccdData *retData, int _startIndex, int _indexInterval, double _timeInterval, int _histIndex ) {
	Double_t fitBounds[4] = {0}; // r1l, r1h, r2l, r2h
	Double_t histParams[3] = {0}; // numbins, lb, ub
	char logNormalFitFormula[128] = {0};
	Double_t rawMean = this->getRawMean();
	if (rawMean > 0) {
		//set params for negative supply voltage
		strlcpy(logNormalFitFormula, "[0]*LogNormal2(x,[1],[3],[2])", 128); // from findMean4
		fitBounds[0] = -0.05e-6;
		fitBounds[1] = 0.01e-6;
		fitBounds[2] = 0.01e-6;
		fitBounds[3] = 0.15e-6;
		histParams[0] = 1200;
		histParams[1] = -0.05e-6;
		histParams[2] = 0.15e-6;
		
	} else if (rawMean < 0) {
		strlcpy(logNormalFitFormula, "[0]*LogNormal2((x<=[3])*(-1*x)+(x>[3])*[3],[1],[3],[2])", 128); // from findMean4
		fitBounds[0] = -0.04e-6;
		fitBounds[1] = 0.04e-6;
		fitBounds[2] = -0.15e-6;
		fitBounds[3] = -0.02e-6;
		histParams[0] = 1200;
		histParams[1] = -0.15e-6;
		histParams[2] = 0.05e-6;
	} else {
		cout << "error, rawMean equals zero: " << rawMean << endl;
		strlcpy(logNormalFitFormula, "0", 128);
	}
	
	Int_t endIndex = 0;
	if ( _indexInterval == -1) {
		Long64_t i = _startIndex;
		this->dataTTree->GetEntry(i);
		double initialTime = timeElapsed;
		double targetTime = initialTime + _timeInterval;
		double timeDiff = 0;
		while ( this->dataTTree->GetEntry(i) ) {
			if (targetTime >= timeElapsed) {
				timeDiff = timeElapsed - targetTime;
				i++;
			} else {
				break;
			}
		}
		endIndex = i - 1;
	} else {
		endIndex = _startIndex + _indexInterval;
	}
	
	const char *histName = Form("h%d", _histIndex);
	this->dataTTree->GetEntry(endIndex);
	const char *histTitle = Form("Diamond Signal Integral Histogram at %e seconds", timeElapsed);
	
	retData->upperBound_time = timeElapsed;
	retData->upperBound_index = endIndex;
	this->dataTTree->GetEntry(_startIndex);
	retData->lowerBound_time = timeElapsed;
	retData->lowerBound_index = _startIndex;
	
	makeHist( retData->histogram, corInt, histParams[0], histParams[1], histParams[2], _startIndex, endIndex, histName, histTitle );

	retData->user = new TF1("user", logNormalFitFormula, fitBounds[2], fitBounds[3]);
	retData->user_dup = new TF1("user_dup", logNormalFitFormula, fitBounds[2]-0.05e-6, fitBounds[3]+0.05e-6);
	retData->user->SetParameters(1e-8, 0.5, 5e-8, 0);


	retData->r1 = retData->histogram->Fit("gaus","SQ0","", fitBounds[0], fitBounds[1]);  // first fit
	retData->r2 = retData->histogram->Fit("user","SQ0","", fitBounds[2], fitBounds[3]);  // second fit
	
	retData->f1 = new TF1("fitFunc","gaus+user");
	// get parameters and set in global TF1
	// parameters of first gaussian
	retData->f1->SetParameter(0,retData->r1->Parameter(0));
	retData->f1->SetParameter(1,retData->r1->Parameter(1));
	retData->f1->SetParameter(2,retData->r1->Parameter(2));
	// parameters of lognormal
	retData->f1->SetParameter(3,retData->r2->Parameter(0));
	retData->f1->SetParameter(4,retData->r2->Parameter(1));
	retData->f1->SetParameter(5,retData->r2->Parameter(2));

	retData->histogram->Fit(retData->f1,"QME");
	
	retData->user_dup->SetParameter(0,retData->f1->GetParameter(3));
	retData->user_dup->SetParameter(1,retData->f1->GetParameter(4));
	retData->user_dup->SetParameter(2,retData->f1->GetParameter(5));
	retData->user_dup->SetParameter(3,retData->f1->GetParameter(6));
	
	retData->lognormal_sigma = retData->user_dup->GetParameter(1);
	retData->lognormal_m = retData->user_dup->GetParameter(2);
	retData->lognormal_x0 = retData->user_dup->GetParameter(3);
	
	retData->lognormal_mean = retData->lognormal_m*TMath::Exp(0.5*TMath::Power(retData->lognormal_sigma,2))+retData->lognormal_x0;
	retData->computed_ccd = retData->lognormal_mean*1000/(32e-8);
	
	return 1;
}




int ccdAnalysis2::calcCCDTTree( int _startIndex, int _indexInterval, double _timeInterval, int _histIndex ) {
	Double_t fitBounds[4] = {0}; // r1l, r1h, r2l, r2h
	Double_t histParams[3] = {0}; // numbins, lb, ub
	char logNormalFitFormula[128] = {0};
	Double_t rawMean = this->getRawMean();
	if (rawMean > 0) {
		//set params for negative supply voltagefitHist_corInt
		strlcpy(logNormalFitFormula, "[0]*LogNormal2(x,[1],[3],[2])", 128); // from findMean4
		fitBounds[0] = -0.05e-6;
		fitBounds[1] = 0.01e-6;
		fitBounds[2] = 0.01e-6;
		fitBounds[3] = 0.15e-6;
		histParams[0] = 1200;
		histParams[1] = -0.05e-6;
		histParams[2] = 0.15e-6;
		
	} else if (rawMean < 0) {
//		strlcpy(logNormalFitFormula, "[0]*LogNormal2((x<=[3])*(-1*x)+(x>[3])*[3],[1],[3],[2])", 128); // from findMean4
		strlcpy(logNormalFitFormula, "[0]*LogNormal2(-x,[1],[3],[2])", 128); // from findMean4
		fitBounds[0] = -0.04e-6;
		fitBounds[1] = 0.04e-6;
		fitBounds[2] = -0.15e-6;
		fitBounds[3] = -0.02e-6;
		histParams[0] = 1200;
		histParams[1] = -0.15e-6;
		histParams[2] = 0.05e-6;
	} else {
		cout << "error, rawMean equals zero: " << rawMean << endl;
		strlcpy(logNormalFitFormula, "0", 128);
	}
	
	Int_t endIndex = 0;
	if ( _indexInterval == -1) {
		Long64_t i = _startIndex;
		this->dataTTree->GetEntry(i);
		double initialTime = timeElapsed;
		double targetTime = initialTime + _timeInterval;
		double timeDiff = 0;
		while ( this->dataTTree->GetEntry(i) ) {
			if (targetTime >= timeElapsed) {
				timeDiff = timeElapsed - targetTime;
				i++;
			} else {
				break;
			}
		}
		endIndex = i - 1;
	} else {
		endIndex = _startIndex + _indexInterval;
	}
	
	const char *histName = Form("h%d", _histIndex);
	this->dataTTree->GetEntry(endIndex);
	const char *histTitle = Form("Diamond Signal Integral Histogram at %e seconds", timeElapsed);
	
	uppBndTime = timeElapsed;
	uppBndIndex = endIndex;
	this->dataTTree->GetEntry(_startIndex);
	lowBndTime = timeElapsed;
	lowBndIndex = _startIndex;
	cout << "make and fit" << endl;
	makeHist( fitHist_corInt, corInt, histParams[0], histParams[1], histParams[2], _startIndex, endIndex, histName, histTitle );

	userFit_corInt = new TF1("userFit_corInt", logNormalFitFormula, fitBounds[2], fitBounds[3]);
	userDupFit_corInt = new TF1("userDupFit_corInt", logNormalFitFormula, fitBounds[2]-0.05e-6, fitBounds[3]+0.05e-6);
	userFit_corInt->SetParameters(1e-8, 0.5, 5e-8, 0);
	
	
	gausFit_corInt = new TF1("gausFit_corInt", "gaus", fitBounds[0], fitBounds[0]);	
	
	TFitResultPtr r1 = fitHist_corInt->Fit(gausFit_corInt,"Q0","", fitBounds[0], fitBounds[1]);  // first fit
	TFitResultPtr r2 = fitHist_corInt->Fit(userFit_corInt,"Q0","", fitBounds[2], fitBounds[3]);  // second fit
	
	
	char fullFitFormula[128] = {0};
	strlcpy(fullFitFormula,"[4]*exp(-0.5*((x-[5])/[6])**2) + ", 128);
	
	strlcat(fullFitFormula, logNormalFitFormula, 128);

	combFit_corInt = new TF1("combFit_corInt", fullFitFormula);
	// get parameters and set in global TF1
	// parameters of first gaussian
	combFit_corInt->SetParameter(4,gausFit_corInt->GetParameter(0));
	combFit_corInt->SetParameter(5,gausFit_corInt->GetParameter(1));
	combFit_corInt->SetParameter(6,gausFit_corInt->GetParameter(2));
	// parameters of lognormal
	combFit_corInt->SetParameter(0,userFit_corInt->GetParameter(0));
	combFit_corInt->SetParameter(1,userFit_corInt->GetParameter(1));
	combFit_corInt->SetParameter(2,userFit_corInt->GetParameter(2));
	combFit_corInt->SetParameter(3,userFit_corInt->GetParameter(3));

	fitHist_corInt->Fit(combFit_corInt,"QME");
	
	userDupFit_corInt->SetParameter(0,combFit_corInt->GetParameter(0));
	userDupFit_corInt->SetParameter(1,combFit_corInt->GetParameter(1));
	userDupFit_corInt->SetParameter(2,combFit_corInt->GetParameter(2));
	userDupFit_corInt->SetParameter(3,combFit_corInt->GetParameter(3));
	
	sigma_val_corInt = userDupFit_corInt->GetParameter(1);
	m_val_corInt = userDupFit_corInt->GetParameter(2);
	x0_val_corInt = userDupFit_corInt->GetParameter(3);
	
	mean_val_corInt = m_val_corInt*TMath::Exp(0.5*TMath::Power(sigma_val_corInt,2))+x0_val_corInt;
	ccd_corInt = mean_val_corInt*1000/(32e-8);

	return 1;
}




Double_t ccdAnalysis2::calcCCDTTree2( int _startIndex, int _indexInterval, double _timeInterval, int _histIndex ) {
	Double_t fitBounds[4] = {0}; // r1l, r1h, r2l, r2h
	Double_t histParams[3] = {0}; // numbins, lb, ub
	char logNormalFitFormula[128] = {0};
	Double_t rawMean = this->getRawMean();
	if (rawMean > 0) {
		//set params for negative supply voltage
		strlcpy(logNormalFitFormula, "[0]*LogNormal2(x,[1],[3],[2])", 128); // from findMean4
		fitBounds[0] = -0.05e-6;
		fitBounds[1] = 0.01e-6;
		fitBounds[2] = 0.01e-6;
		fitBounds[3] = 0.15e-6;
		histParams[0] = 1200;
		histParams[1] = -0.05e-6;
		histParams[2] = 0.15e-6;
		
	} else if (rawMean < 0) {
//		strlcpy(logNormalFitFormula, "[0]*LogNormal2((x<=[3])*(-1*x)+(x>[3])*[3],[1],[3],[2])", 128); // from findMean4
		strlcpy(logNormalFitFormula, "[0]*LogNormal2(-x,[1],[3],[2])", 128); // from findMean4
		fitBounds[0] = -0.02e-6;
		fitBounds[1] = 0.04e-6;
		fitBounds[2] = -0.15e-6;
		fitBounds[3] = -0.02e-6;
		histParams[0] = 1200;
		histParams[1] = -0.15e-6;
		histParams[2] = 0.05e-6;
	} else {
		cout << "error, rawMean equals zero: " << rawMean << endl;
		strlcpy(logNormalFitFormula, "0", 128);
	}
	
	Int_t endIndex = 0;
	if ( _indexInterval == -1) {
		Long64_t i = _startIndex;
		this->dataTTree->GetEntry(i);
		double initialTime = timeElapsed;
		double targetTime = initialTime + _timeInterval;
		double timeDiff = 0;
		while ( this->dataTTree->GetEntry(i) ) {
			if (targetTime >= timeElapsed) {
				timeDiff = timeElapsed - targetTime;
				i++;
			} else {
				break;
			}
		}
		endIndex = i - 1;
	} else {
		endIndex = _startIndex + _indexInterval;
		cout << "endIndex set to " << endIndex << endl;
	}
	
	cout << "running calcCCDTTree2 with start = " << _startIndex << " and end = " << endIndex << endl;
	const char *histName = Form("h%d", _histIndex);
	this->dataTTree->GetEntry(endIndex);
	const char *histTitle = Form("Diamond Signal Integral Histogram at %e seconds", timeElapsed);
	
	uppBndTime = timeElapsed;
	uppBndIndex = endIndex;
	this->dataTTree->GetEntry(_startIndex);
	lowBndTime = timeElapsed;
	lowBndIndex = _startIndex;
	// cout << "make and fit" << endl;
	makeHist( fitHist_corInt, corInt, histParams[0], histParams[1], histParams[2], _startIndex, endIndex, histName, histTitle );

	userFit_corInt = new TF1("userFit_corInt", logNormalFitFormula, fitBounds[2], fitBounds[3]);
	userDupFit_corInt = new TF1("userDupFit_corInt", logNormalFitFormula, histParams[1], histParams[2]);
	userFit_corInt->SetParameters(1e-8, 0.5, 5e-8, 0);
	
	
	gausFit_corInt = new TF1("gausFit_corInt", "gaus", fitBounds[0], fitBounds[1]);	
	
	TFitResultPtr r1 = fitHist_corInt->Fit(gausFit_corInt,"Q0","", fitBounds[0], fitBounds[1]);  // first fit
	TFitResultPtr r2 = fitHist_corInt->Fit(userFit_corInt,"Q0","", fitBounds[2], fitBounds[3]);  // second fit
	
	
	char fullFitFormula[128] = {0};
	strlcpy(fullFitFormula,"[4]*exp(-0.5*((x-[5])/[6])**2) + ", 128);
	
	strlcat(fullFitFormula, logNormalFitFormula, 128);
	
	combFit_corInt = new TF1("combFit_corInt", fullFitFormula);
	// get parameters and set in global TF1
	// parameters of first gaussian
	combFit_corInt->SetParameter(4,gausFit_corInt->GetParameter(0));
	combFit_corInt->SetParameter(5,gausFit_corInt->GetParameter(1));
	combFit_corInt->SetParameter(6,gausFit_corInt->GetParameter(2));
	// parameters of lognormal
	combFit_corInt->SetParameter(0,userFit_corInt->GetParameter(0));
	combFit_corInt->SetParameter(1,userFit_corInt->GetParameter(1));
	combFit_corInt->SetParameter(2,userFit_corInt->GetParameter(2));
	combFit_corInt->SetParameter(3,userFit_corInt->GetParameter(3));

	fitHist_corInt->Fit(combFit_corInt,"QME");
	
	userDupFit_corInt->SetParameter(0,combFit_corInt->GetParameter(0));
	userDupFit_corInt->SetParameter(1,combFit_corInt->GetParameter(1));
	userDupFit_corInt->SetParameter(2,combFit_corInt->GetParameter(2));
	userDupFit_corInt->SetParameter(3,combFit_corInt->GetParameter(3));
	// fitHist_corInt->Draw();
	sigma_val_corInt = userDupFit_corInt->GetParameter(1);
	m_val_corInt = userDupFit_corInt->GetParameter(2);
	x0_val_corInt = userDupFit_corInt->GetParameter(3);
	gaus_m_val_corInt = combFit_corInt->GetParameter(5);


	mean_val_corInt = m_val_corInt*TMath::Exp(0.5*TMath::Power(sigma_val_corInt,2))+x0_val_corInt;
	ccd_corInt = mean_val_corInt*1000/(32e-8);
	
	return ccd_corInt;
}

Int_t ccdAnalysis2::findIndexBounds( int _startIndex, int _indexInterval, double _timeInterval ) {
	Int_t endIndex = 0;
	if ( _indexInterval == -1) {
		Long64_t i = _startIndex;
		this->dataTTree->GetEntry(i);
		double initialTime = timeElapsed;
		double targetTime = initialTime + _timeInterval;
		double timeDiff = 0;
		while ( this->dataTTree->GetEntry(i) ) {
			if (targetTime >= timeElapsed) {
				timeDiff = timeElapsed - targetTime;
				i++;
			} else {
				break;
			}
		}
		endIndex = i - 1;
	} else {
		endIndex = _startIndex + _indexInterval;
		cout << "endIndex set to " << endIndex << endl;
	}
	return endIndex;
}

void ccdAnalysis2::refTest(const char * name) {
	double *valToPrint = 0;
	if (!strcmp(name, "corInt")) {
		valToPrint = &corInt;
	} else if (!strcmp(name, "postInt")) {
		valToPrint = &postInt;
	} else {
		// double &valToPrint = 0;
		cout << "invalid argument" << endl;
		return;
	}
	dataTTree->GetEntry(10);
	cout << "the value of the requested param at entry 10 is " << *valToPrint << endl;
}

Double_t ccdAnalysis2::makeHistFromChannel( TH1D *&hist, Int_t numBins, Double_t lb, Double_t ub, Int_t startIndex, Int_t endIndex, const char *_ChannelName, const char * histName, const char * histTitle ) {
//Double_t ccdAnalysis2::makeHist( TH1D *&hist, Int_t numBins, Double_t lb, Double_t ub, Int_t startIndex, Int_t endIndex, const char * histName ) {
	double *dataChannel = 0;

	if (!strcmp(_ChannelName, "corInt")) {
		dataChannel = &corInt;
	} else if (!strcmp(_ChannelName, "postInt")) {
		dataChannel = &postInt;
	} else {
		// double &valToPrint = 0;
		cout << "invalid argument" << endl;
		return -1;
	}

	hist = new TH1D(histName, histTitle, numBins, lb, ub);
	hist->GetXaxis()->SetTitle("Integrated Signal");
	hist->GetYaxis()->SetTitle("Counts");
	hist->SetStats(kFALSE);
	Long64_t i = 0;
	for (i = startIndex; i < this->nentries && i < endIndex; i++) {
		this->dataTTree->GetEntry(i);
		hist->Fill(*dataChannel);
	}
	
	return hist->GetEntries();
}

void ccdAnalysis2::setBounds( Double_t _FitBounds[4], Double_t _HistParams[3], Double_t _initFitParams[4], char _LogNormalFitFormula[128], const char *_ChannelName, int selfTrig, int silicon ) {

	Double_t rawMean = this->getRawMean(_ChannelName);
	// cout << "rawMean = " << rawMean << endl;
	if (!strcmp(_ChannelName, "corInt")) {

		if (_initFitParams) {
			_initFitParams[0] = 1e-7;
			_initFitParams[1] = 0.5;
			_initFitParams[2] = 5e-8;
			_initFitParams[3] = 0;
		}

		if (rawMean > 0 && !selfTrig && !silicon) {
			//set params for negative supply voltage
			strlcpy(_LogNormalFitFormula, "[0]*LogNormal2(x,[1],[3],[2])", 128); // from findMean4
			_FitBounds[0] = -0.05e-6;
			_FitBounds[1] = 0.01e-6;
			_FitBounds[2] = 0.01e-6;
			_FitBounds[3] = 0.15e-6;
			_HistParams[0] = 1200;
			_HistParams[1] = -0.05e-6;
			_HistParams[2] = 0.15e-6;
			
		} else if (rawMean < 0 && !selfTrig && !silicon) {
	//		strlcpy(logNormalFitFormula, "[0]*LogNormal2((x<=[3])*(-1*x)+(x>[3])*[3],[1],[3],[2])", 128); // from findMean4
			strlcpy(_LogNormalFitFormula, "[0]*LogNormal2(-x,[1],[3],[2])", 128); // from findMean4
			_FitBounds[0] = -0.02e-6;
			_FitBounds[1] = 0.04e-6;
			_FitBounds[2] = -0.15e-6;
			_FitBounds[3] = -0.02e-6;
			_HistParams[0] = 1200;
			_HistParams[1] = -0.15e-6;
			_HistParams[2] = 0.05e-6;
		} else if (rawMean > 0 && selfTrig && !silicon) {
			//set params for negative supply voltage on selfTrig data
			strlcpy(_LogNormalFitFormula, "[0]*LogNormal2(x,[1],[3],[2])", 128); // from findMean4
			_FitBounds[0] = -0.01e-6;
			_FitBounds[1] = 0.005e-6;
			_FitBounds[2] = -0.005e-6;
			_FitBounds[3] = 0.15e-6;
			_HistParams[0] = 1200;
			_HistParams[1] = -0.05e-6;
			_HistParams[2] = 0.15e-6;
			
		} else if (rawMean < 0 && selfTrig && !silicon) {
	//		strlcpy(logNormalFitFormula, "[0]*LogNormal2((x<=[3])*(-1*x)+(x>[3])*[3],[1],[3],[2])", 128); // from findMean4
			strlcpy(_LogNormalFitFormula, "[0]*LogNormal2(-x,[1],[3],[2])", 128); // from findMean4
			_FitBounds[0] = -0.01e-6;
			_FitBounds[1] = 0.01e-6;
			_FitBounds[2] = -0.15e-6;
			_FitBounds[3] = 0.02e-6;
			_HistParams[0] = 1200;
			_HistParams[1] = -0.15e-6;
			_HistParams[2] = 0.05e-6;
		} else if (rawMean > 0 && selfTrig && silicon) {
			//set params for negative supply voltage on selfTrig data
			strlcpy(_LogNormalFitFormula, "[0]*LogNormal2(x,[1],[3],[2])", 128); // from findMean4
			_FitBounds[0] = -0.05e-6;
			_FitBounds[1] = 0.05e-6;
			_FitBounds[2] = 0.05e-6;
			_FitBounds[3] = 0.30e-6;
			_HistParams[0] = 1200;
			_HistParams[1] = -0.10e-6;
			_HistParams[2] = 0.30e-6;
			
		} else if (rawMean < 0 && selfTrig && silicon) {
	//		strlcpy(logNormalFitFormula, "[0]*LogNormal2((x<=[3])*(-1*x)+(x>[3])*[3],[1],[3],[2])", 128); // from findMean4
			strlcpy(_LogNormalFitFormula, "[0]*LogNormal2(-x,[1],[3],[2])", 128); // from findMean4
			_FitBounds[0] = -0.05e-6;
			_FitBounds[1] = 0.05e-6;
			_FitBounds[2] = -0.30e-6;
			_FitBounds[3] = 0.05e-6;
			_HistParams[0] = 1200;
			_HistParams[1] = -0.30e-6;
			_HistParams[2] = 0.30e-6;
		} else {
			cout << "error, rawMean equals zero: " << rawMean << endl;
			strlcpy(_LogNormalFitFormula, "0", 128);
		}
	} else if (!strcmp(_ChannelName, "postInt")) {
		if (_initFitParams) {
			_initFitParams[0] = 1e-6;
			_initFitParams[1] = 0.5;
			_initFitParams[2] = 5e-6;
			_initFitParams[3] = -1.84e-6;
		}
		if (rawMean > -1.78e-6) {
			//set params for negative supply voltage
			strlcpy(_LogNormalFitFormula, "[0]*LogNormal2(x,[1],[3],[2])", 128); // from findMean4
			_FitBounds[0] = -1.79e-6;
			_FitBounds[1] = -1.764e-6;
			_FitBounds[2] = -1.76e-6;
			_FitBounds[3] = -1.63e-6;
			_HistParams[0] = 1200;
			_HistParams[1] = -1.85e-6;
			_HistParams[2] = -1.55e-6;
			
		} else if (rawMean < -1.78e-6) {
	//		strlcpy(logNormalFitFormula, "[0]*LogNormal2((x<=[3])*(-1*x)+(x>[3])*[3],[1],[3],[2])", 128); // from findMean4
			strlcpy(_LogNormalFitFormula, "[0]*LogNormal2(-x,[1],[3],[2])", 128); // from findMean4
			_FitBounds[0] = -1.793e-6;
			_FitBounds[1] = -1.74e-6;
			_FitBounds[2] = -1.93e-6;
			_FitBounds[3] = -1.793e-6;
			_HistParams[0] = 1200;		//CHANGE THIS VALUE TO MAINTAIN CONSISTIENT BIN WIDTH
			_HistParams[1] = -1.95e-6;
			_HistParams[2] = -1.74e-6;	//OR THIS VALUE
		} else {
			cout << "error, rawMean equals zero: " << rawMean << endl;
			strlcpy(_LogNormalFitFormula, "0", 128);
		}
	} else {
		// double &valToPrint = 0;
		cout << "invalid argument" << endl;
		return;
	}
}

Double_t ccdAnalysis2::calcCCDTTree3( int _startIndex, int _endIndex, int _histIndex, const char* _channelName, Double_t fp1, Double_t fp2, Double_t fp3, Double_t fp4 ) {
	Double_t fitBounds[4] = {0}; // r1l, r1h, r2l, r2h
	Double_t histParams[3] = {0}; // numbins, lb, ub
	char logNormalFitFormula[128] = {0};
	cout << "running calcCCDTTree3 with start = " << _startIndex << " and end = " << _endIndex << endl;
	setBounds( fitBounds, histParams, 0, logNormalFitFormula, _channelName);
	
	const char *histName = Form("h%d", _histIndex);
	this->dataTTree->GetEntry(_endIndex);
	const char *histTitle = Form("Diamond Signal Integral Histogram at %e seconds", timeElapsed);
	
	uppBndTime = timeElapsed;
	uppBndIndex = _endIndex;
	this->dataTTree->GetEntry(_startIndex);
	lowBndTime = timeElapsed;
	lowBndIndex = _startIndex;
	// cout << "make and fit" << endl;
	makeHist( fitHist_postInt, postInt, histParams[0], histParams[1], histParams[2], _startIndex, _endIndex, histName, histTitle );
	
	userFit_postInt = new TF1("userFit_postInt", logNormalFitFormula, fitBounds[2], fitBounds[3]);
	userDupFit_postInt = new TF1("userDupFit_postInt", logNormalFitFormula, histParams[1], histParams[2]);
	// userFit_postInt->SetParameters(1e-8, 0.5, 5e-8, 0);
	userFit_postInt->SetParameters(1e-8,0.5,5e-8,-1.8e-6);
	
	gausFit_postInt = new TF1("gausFit_postInt", "gaus", fitBounds[0], fitBounds[1]);	
	
	TFitResultPtr r1 = fitHist_postInt->Fit(gausFit_postInt,"","", fitBounds[0], fitBounds[1]);  // first fit
	TFitResultPtr r2 = fitHist_postInt->Fit(userFit_postInt,"","", fitBounds[2], fitBounds[3]);  // second fit
	
	// fitHist_postInt->Draw();
	// return 0;
	
	
	char fullFitFormula[128] = {0};
	strlcpy(fullFitFormula,"[4]*exp(-0.5*((x-[5])/[6])**2) + ", 128);
	
	strlcat(fullFitFormula, logNormalFitFormula, 128);
	
	combFit_postInt = new TF1("combFit_postInt", fullFitFormula);
	combFit_postInt->SetNpx(10000);
	// get parameters and set in global TF1
	// parameters of first gaussian
	combFit_postInt->SetParameter(4,gausFit_postInt->GetParameter(0));
	combFit_postInt->SetParameter(5,gausFit_postInt->GetParameter(1));
	combFit_postInt->SetParameter(6,gausFit_postInt->GetParameter(2));
	// parameters of lognormal
	combFit_postInt->SetParameter(0,userFit_postInt->GetParameter(0));
	combFit_postInt->SetParameter(1,userFit_postInt->GetParameter(1));
	combFit_postInt->SetParameter(2,userFit_postInt->GetParameter(2));
	combFit_postInt->SetParameter(3,userFit_postInt->GetParameter(3));

	fitHist_postInt->Fit(combFit_postInt,"QME");
	
	userDupFit_postInt->SetParameter(0,combFit_postInt->GetParameter(0));
	userDupFit_postInt->SetParameter(1,combFit_postInt->GetParameter(1));
	userDupFit_postInt->SetParameter(2,combFit_postInt->GetParameter(2));
	userDupFit_postInt->SetParameter(3,combFit_postInt->GetParameter(3));
	// fitHist_postInt->Draw();
	sigma_val_postInt = userDupFit_postInt->GetParameter(1);
	m_val_postInt = userDupFit_postInt->GetParameter(2);
	x0_val_postInt = userDupFit_postInt->GetParameter(3);

	gaus_m_val_postInt = gausFit_postInt->GetParameter(1);
	
	mean_val_postInt = m_val_postInt*TMath::Exp(0.5*TMath::Power(sigma_val_postInt,2))+x0_val_postInt;
	mean_val_adj_postInt = mean_val_postInt - gaus_m_val_postInt;
	ccd_postInt = mean_val_adj_postInt*1000/(32e-8);
	
	return ccd_postInt;
}



Double_t ccdAnalysis2::calcCCDTTree4( int _startIndex, int _endIndex, int _histIndex, const char* _channelName, int debug, int selfTrig, int silicon, Double_t fp1, Double_t fp2, Double_t fp3, Double_t fp4 ) {

	TString userFitName("userFit_");
	TString userDupFitName("userDupFit_");
	TString gausFitName("gausFit_");
	TString combFitName("combFit_");
	userFitName.Append(_channelName);
	userDupFitName.Append(_channelName);
	gausFitName.Append(_channelName);
	combFitName.Append(_channelName);

	TH1D	**fitHist;
	TF1		**userFit;
	TF1		**userDupFit;
	TF1		**gausFit;
	TF1		**combFit;
	double *sigma_val = 0;
	double *m_val = 0;
	double *x0_val = 0;
	double *gaus_m_val = 0;
	double *mean_val = 0;
	double *mean_val_adj = 0;
	double *ccd = 0;

	if (!strcmp(_channelName, "corInt")) {
		// dataChannel = &corInt;
		fitHist = &fitHist_corInt;
		userFit = &userFit_corInt;
		userDupFit = &userDupFit_corInt;
		gausFit = &gausFit_corInt;
		combFit = &combFit_corInt;
		sigma_val = &sigma_val_corInt;
		m_val = &m_val_corInt;
		x0_val = &x0_val_corInt;
		gaus_m_val = &gaus_m_val_corInt;
		mean_val = &mean_val_corInt;
		ccd = &ccd_corInt;
	} else if (!strcmp(_channelName, "postInt")) {
		// dataChannel = &postInt;
		fitHist = &fitHist_postInt;
		userFit = &userFit_postInt;
		userDupFit = &userDupFit_postInt;
		gausFit = &gausFit_postInt;
		combFit = &combFit_postInt;
		sigma_val = &sigma_val_postInt;
		m_val = &m_val_postInt;
		x0_val = &x0_val_postInt;
		gaus_m_val = &gaus_m_val_postInt;
		mean_val = &mean_val_postInt;
		mean_val_adj = &mean_val_adj_postInt; // only used for postInt
		ccd = &ccd_postInt;
	} else {
		// double &valToPrint = 0;
		cout << "invalid argument" << endl;
		return -1;
	}

	Double_t fitBounds[4] = {0}; // r1l, r1h, r2l, r2h
	Double_t histParams[3] = {0}; // numbins, lb, ub
	Double_t initFitParams[4] = {0};
	char logNormalFitFormula[128] = {0};
	printf("running calcCCDTTree4 on %s with start = %d and end = %d\n", _channelName, _startIndex, _endIndex);
	setBounds( fitBounds, histParams, initFitParams, logNormalFitFormula, _channelName, selfTrig, silicon);
	printf("hello 1\n");
	const char *histName = Form("h%d", _histIndex);
	this->dataTTree->GetEntry(_endIndex);
	printf("hello 2\n");
	// const char *histTitle = Form("Diamond Signal Integral Histogram at %e seconds",  timeElapsed);
	const char *histTitle = Form("Diamond Signal Integral Histogram");
	
	uppBndTime = timeElapsed;
	uppBndIndex = _endIndex;
	this->dataTTree->GetEntry(_startIndex);
	lowBndTime = timeElapsed;
	lowBndIndex = _startIndex;
	// cout << "make and fit" << endl;
	makeHistFromChannel( (*fitHist), histParams[0], histParams[1], histParams[2], _startIndex, _endIndex, _channelName, histName, histTitle );
	// (*fitHist) = new TH1D(histName, histTitle, 1000, -10, 10);
	// (*fitHist)->FillRandom("gaus", 10000);
	// (*fitHist)->Draw();
	// return 0;
	
	(*fitHist)->Scale(1/(*fitHist)->Integral(), "width");
	//(*fitHist)->Scale(1);
	

	(*userFit) = new TF1(userFitName.Data(), logNormalFitFormula, fitBounds[2], fitBounds[3]);
	(*userDupFit) = new TF1(userDupFitName.Data(), logNormalFitFormula, histParams[1], histParams[2]);
	// userFit->SetParameters(1e-8, 0.5, 5e-8, 0);
	// userFit->SePtarameters(1e-8,0.5,5e-8,-1.8e-6);
	if (debug) {
		cout << "normal fit params: " << initFitParams[0] << ", " << initFitParams[1] << ", " << initFitParams[2] << ", " << initFitParams[3] << endl;
		(*userFit)->SetParameters(fp1,fp2,fp3,fp4);
	} else {
		(*userFit)->SetParameters(initFitParams);
	}
	// (*userFit)->SetParameters(fp1,fp2,fp3,fp4);
	
	(*gausFit) = new TF1(gausFitName.Data(), "gaus", fitBounds[0], fitBounds[1]);	
	
	if (debug == 1) {
		TFitResultPtr r1 = (*fitHist)->Fit((*gausFit),"","", fitBounds[0], fitBounds[1]);  // first fit	
		TFitResultPtr r2 = (*fitHist)->Fit((*userFit),"","", fitBounds[2], fitBounds[3]);  // second fit
		(*fitHist)->Draw();
		return 0;
	} else {
		TFitResultPtr r1 = (*fitHist)->Fit((*gausFit),"Q0","", fitBounds[0], fitBounds[1]);  // first fit	
		TFitResultPtr r2 = (*fitHist)->Fit((*userFit),"Q0","", fitBounds[2], fitBounds[3]);  // second fit
	}
	
	
	char fullFitFormula[128] = {0};
	strlcpy(fullFitFormula,"[4]*exp(-0.5*((x-[5])/[6])**2) + ", 128);
	
	strlcat(fullFitFormula, logNormalFitFormula, 128);
	
	(*combFit) = new TF1(combFitName.Data(), fullFitFormula);
	(*combFit)->SetNpx(10000);
	// get parameters and set in global TF1
	// parameters of first gaussian
	(*combFit)->SetParameter(4,(*gausFit)->GetParameter(0));
	(*combFit)->SetParameter(5,(*gausFit)->GetParameter(1));
	(*combFit)->SetParameter(6,(*gausFit)->GetParameter(2));
	// parameters of lognormal
	(*combFit)->SetParameter(0,(*userFit)->GetParameter(0));
	(*combFit)->SetParameter(1,(*userFit)->GetParameter(1));
	(*combFit)->SetParameter(2,(*userFit)->GetParameter(2));
	(*combFit)->SetParameter(3,(*userFit)->GetParameter(3));

	(*fitHist)->Fit((*combFit),"QME");
	
	(*userDupFit)->SetParameter(0,(*combFit)->GetParameter(0));
	(*userDupFit)->SetParameter(1,(*combFit)->GetParameter(1));
	(*userDupFit)->SetParameter(2,(*combFit)->GetParameter(2));
	(*userDupFit)->SetParameter(3,(*combFit)->GetParameter(3));
	// fitHist->Draw();
	*sigma_val = (*userDupFit)->GetParameter(1);
	*m_val = (*userDupFit)->GetParameter(2);
	*x0_val = (*userDupFit)->GetParameter(3);

	*gaus_m_val = (*gausFit)->GetParameter(1);
	
	*mean_val = *m_val*TMath::Exp(0.5*TMath::Power(*sigma_val,2))+*x0_val;
	
	if (!strcmp(_channelName, "corInt")) {
		*ccd = *mean_val*1000/(32e-8);
	} else if (!strcmp(_channelName, "postInt")) {
		*mean_val_adj = *mean_val - *gaus_m_val;
		*ccd = *mean_val_adj*1000/(32e-8);
	} else {
		// double &valToPrint = 0;
		cout << "invalid argument and why didnt this get caught above?" << endl;
		return -1;
	}

	return *ccd;
}




Double_t ccdAnalysis2::calcCCDTTree4_fixedGaus( int _startIndex, int _endIndex, int _histIndex, const char* _channelName, int debug, Double_t fp1, Double_t fp2, Double_t fp3, Double_t fp4 ) {

	TString userFitName("userFit_");
	TString userDupFitName("userDupFit_");
	TString gausFitName("gausFit_");
	TString combFitName("combFit_");
	userFitName.Append(_channelName);
	userDupFitName.Append(_channelName);
	gausFitName.Append(_channelName);
	combFitName.Append(_channelName);

	TH1D	**fitHist;
	TF1		**userFit;
	TF1		**userDupFit;
	TF1		**gausFit;
	TF1		**combFit;
	double *sigma_val = 0;
	double *m_val = 0;
	double *x0_val = 0;
	double *gaus_m_val = 0;
	double *mean_val = 0;
	double *mean_val_adj = 0;
	double *ccd = 0;

	if (!strcmp(_channelName, "corInt")) {
		// dataChannel = &corInt;
		fitHist = &fitHist_corInt;
		userFit = &userFit_corInt;
		userDupFit = &userDupFit_corInt;
		gausFit = &gausFit_corInt;
		combFit = &combFit_corInt;
		sigma_val = &sigma_val_corInt;
		m_val = &m_val_corInt;
		x0_val = &x0_val_corInt;
		gaus_m_val = &gaus_m_val_corInt;
		mean_val = &mean_val_corInt;
		ccd = &ccd_corInt;
	} else if (!strcmp(_channelName, "postInt")) {
		// dataChannel = &postInt;
		fitHist = &fitHist_postInt;
		userFit = &userFit_postInt;
		userDupFit = &userDupFit_postInt;
		gausFit = &gausFit_postInt;
		combFit = &combFit_postInt;
		sigma_val = &sigma_val_postInt;
		m_val = &m_val_postInt;
		x0_val = &x0_val_postInt;
		gaus_m_val = &gaus_m_val_postInt;
		mean_val = &mean_val_postInt;
		mean_val_adj = &mean_val_adj_postInt; // only used for postInt
		ccd = &ccd_postInt;
	} else {
		// double &valToPrint = 0;
		cout << "invalid argument" << endl;
		return -1;
	}

	Double_t fitBounds[4] = {0}; // r1l, r1h, r2l, r2h
	Double_t histParams[3] = {0}; // numbins, lb, ub
	Double_t initFitParams[4] = {0};
	char logNormalFitFormula[128] = {0};
	printf("running calcCCDTTree4 on %s with start = %d and end = %d\n", _channelName, _startIndex, _endIndex);
	setBounds( fitBounds, histParams, initFitParams, logNormalFitFormula, _channelName);
	
	const char *histName = Form("h%d", _histIndex);
	this->dataTTree->GetEntry(_endIndex);
	const char *histTitle = Form("%s histogram at %e seconds", _channelName, timeElapsed);
	
	uppBndTime = timeElapsed;
	uppBndIndex = _endIndex;
	this->dataTTree->GetEntry(_startIndex);
	lowBndTime = timeElapsed;
	lowBndIndex = _startIndex;
	// cout << "make and fit" << endl;
	makeHistFromChannel( (*fitHist), histParams[0], histParams[1], histParams[2], _startIndex, _endIndex, _channelName, histName, histTitle );
	// (*fitHist) = new TH1D(histName, histTitle, 1000, -10, 10);
	// (*fitHist)->FillRandom("gaus", 10000);
	// (*fitHist)->Draw();
	// return 0;
	


	(*userFit) = new TF1(userFitName.Data(), logNormalFitFormula, fitBounds[2], fitBounds[3]);
	(*userDupFit) = new TF1(userDupFitName.Data(), logNormalFitFormula, histParams[1], histParams[2]);
	// userFit->SetParameters(1e-8, 0.5, 5e-8, 0);
	// userFit->SetParameters(1e-8,0.5,5e-8,-1.8e-6);
	if (debug) {
		cout << "normal fit params: " << initFitParams[0] << ", " << initFitParams[1] << ", " << initFitParams[2] << ", " << initFitParams[3] << endl;
		(*userFit)->SetParameters(fp1,fp2,fp3,fp4);
	} else {
		(*userFit)->SetParameters(initFitParams);
	}
	// (*userFit)->SetParameters(fp1,fp2,fp3,fp4);
	
	(*gausFit) = new TF1(gausFitName.Data(), "gaus", fitBounds[0], fitBounds[1]);	
	
	if (debug == 1) {
		TFitResultPtr r1 = (*fitHist)->Fit((*gausFit),"","", fitBounds[0], fitBounds[1]);  // first fit	
		TFitResultPtr r2 = (*fitHist)->Fit((*userFit),"","", fitBounds[2], fitBounds[3]);  // second fit
		(*fitHist)->Draw();
		return 0;
	} else {
		TFitResultPtr r1 = (*fitHist)->Fit((*gausFit),"Q0","", fitBounds[0], fitBounds[1]);  // first fit	
		TFitResultPtr r2 = (*fitHist)->Fit((*userFit),"Q0","", fitBounds[2], fitBounds[3]);  // second fit
	}
	
	
	char fullFitFormula[128] = {0};
	strlcpy(fullFitFormula,"[4]*exp(-0.5*((x-[5])/[6])**2) + ", 128);
	
	strlcat(fullFitFormula, logNormalFitFormula, 128);
	
	(*combFit) = new TF1(combFitName.Data(), fullFitFormula);
	(*combFit)->SetNpx(10000);
	// get parameters and set in global TF1
	// parameters of first gaussian
	(*combFit)->SetParameter(4,2.76052e+04);
	(*combFit)->FixParameter(5,2.54466e-11);
	(*combFit)->SetParameter(6,1.12541e-09);
	// parameters of lognormal
	(*combFit)->SetParameter(0,4.90489e-05);
	(*combFit)->SetParameter(1,0.363001);
	(*combFit)->SetParameter(2,6.42453e-08);
	(*combFit)->SetParameter(3,-9.73279e-09);

	(*fitHist)->Fit((*combFit),"");
	
	(*userDupFit)->SetParameter(0,(*combFit)->GetParameter(0));
	(*userDupFit)->SetParameter(1,(*combFit)->GetParameter(1));
	(*userDupFit)->SetParameter(2,(*combFit)->GetParameter(2));
	(*userDupFit)->SetParameter(3,(*combFit)->GetParameter(3));
	// fitHist->Draw();
	*sigma_val = (*userDupFit)->GetParameter(1);
	*m_val = (*userDupFit)->GetParameter(2);
	*x0_val = (*userDupFit)->GetParameter(3);

	*gaus_m_val = (*gausFit)->GetParameter(1);
	
	*mean_val = *m_val*TMath::Exp(0.5*TMath::Power(*sigma_val,2))+*x0_val;
	
	if (!strcmp(_channelName, "corInt")) {
		*ccd = *mean_val*1000/(32e-8);
	} else if (!strcmp(_channelName, "postInt")) {
		*mean_val_adj = *mean_val - *gaus_m_val;
		*ccd = *mean_val_adj*1000/(32e-8);
	} else {
		// double &valToPrint = 0;
		cout << "invalid argument and why didnt this get caught above?" << endl;
		return -1;
	}

	return *ccd;
}





Double_t ccdAnalysis2::calcCCD( int startIndex, int indexInterval, Double_t fp0, Double_t fp1, Double_t fp2, Double_t fp3, Double_t uflb ) {
	
	if (indexInterval == -1 && nentries > 0) indexInterval = nentries;	
	
	Double_t fitBounds[4] = {0}; // r1l, r1h, r2l, r2h
	Double_t histParams[3] = {0}; // numbins, lb, ub
	char logNormalFitFormula[128] = {0};
	Double_t rawMean = this->getRawMean();
	if (rawMean > 0) {
		//set params for negative supply voltage
//		logNormalFitFormula = "[0]*TMath::LogNormal((x>=[3])*(x)+(x<[3])*[3],[1],[3],[2])";

//		strlcpy(logNormalFitFormula, "[0]*LogNormal2((x>=[3])*(x)+(x<[3])*[3],[1],[3],[2])", 128); // from findMean4
		strlcpy(logNormalFitFormula, "[0]*LogNormal2(x,[1],[3],[2])", 128); // from findMean4

//		fitBounds = {-0.04e-6, 0.04e-6, -0.15e-6, -0.02e-6};
//		histParams = {-0.15e-6,0.05e-6};
		fitBounds[0] = -0.05e-6;
		fitBounds[1] = 0.01e-6;
		fitBounds[2] = 0.01e-6;
//		fitBounds[2] = uflb;
		fitBounds[3] = 0.15e-6;
		histParams[0] = 1200;
		histParams[1] = -0.05e-6;
		histParams[2] = 0.15e-6;
		
	} else if (rawMean < 0) {
		//set params for positive supply voltage
//		logNormalFitFormula = "[0]*TMath::LogNormal((x<=[3])*(-1*x)+(x>[3])*[3],[1],[3],[2])";

		strlcpy(logNormalFitFormula, "[0]*LogNormal2((x<=[3])*(-1*x)+(x>[3])*[3],[1],[3],[2])", 128); // from findMean4

//		fitBounds = {-0.05e-6, 0.01e-6, 0.01e-6, 0.15e-6};
//		histParams = {-0.05e-6, 0.15e-6};
		
//		strlcpy(logNormalFitFormula, "[0]*TMath::LogNormal((x<=0)*(-1*x)+(x>0)*0,[1],0,[2])", 128);
		
		fitBounds[0] = -0.04e-6;
		fitBounds[1] = 0.04e-6;
		fitBounds[2] = -0.15e-6;
		fitBounds[3] = -0.02e-6;
		histParams[0] = 1200;
		histParams[1] = -0.15e-6;
		histParams[2] = 0.05e-6;
	} else {
		cout << "error, rawMean equals zero: " << rawMean << endl;
		strlcpy(logNormalFitFormula, "0", 128);
	}
	
	makeh1(histParams[0],histParams[1],histParams[2], startIndex, startIndex + indexInterval);	
		
	TF1 * user = new TF1("user", logNormalFitFormula, fitBounds[2], fitBounds[3]);
	TF1 * user_dup = new TF1("user_dup", logNormalFitFormula, fitBounds[2]-0.05e-6, fitBounds[3]+0.05e-6);
//	user->SetParameters(fp0, fp1, fp2, fp3);
	user->SetParameters(1e-8, 0.5, 5e-8, 0);
//	user->FixParameter(3,0);

//	TCanvas *c0 = new TCanvas("c0","co");
//	TCanvas *c1 = new TCanvas("c1","c1");
	
//	c0->cd();
//	user->Draw();
//	c1->cd();
//	h1->Fit("user", "", "", fitBounds[2], fitBounds[3]);
////	h1->Fit("gaus","S","", fitBounds[0], fitBounds[1]);


	TFitResultPtr r1 = h1->Fit("gaus","SQ","", fitBounds[0], fitBounds[1]);  // first fit
	TFitResultPtr r2 = h1->Fit("user","SQ","", fitBounds[2], fitBounds[3]);  // second fit
	
	TF1 * f1 = new TF1("fitFunc","gaus+user");
	// get parameters and set in global TF1

	// parameters of first gaussian
	f1->SetParameter(0,r1->Parameter(0));
	f1->SetParameter(1,r1->Parameter(1));
	f1->SetParameter(2,r1->Parameter(2));
	// parameters of second gaussian
	f1->SetParameter(3,r2->Parameter(0));
	f1->SetParameter(4,r2->Parameter(1));
	f1->SetParameter(5,r2->Parameter(2));
//	f1->FixParameter(5,0);

	h1->Fit(f1,"Q");
	
	user_dup->SetParameter(0,f1->GetParameter(3));
	user_dup->SetParameter(1,f1->GetParameter(4));
	user_dup->SetParameter(2,f1->GetParameter(5));
	user_dup->SetParameter(3,f1->GetParameter(6));
	
	Double_t lognormal_sigma = user_dup->GetParameter(1);
	Double_t lognormal_m = user_dup->GetParameter(2);
	Double_t lognormal_x0 = user_dup->GetParameter(3);
	
//	Double_t ccd = lognormal_m*1000/(32e-8);
	Double_t lognormal_mean = lognormal_m*TMath::Exp(0.5*TMath::Power(lognormal_sigma,2))+lognormal_x0;
	Double_t ccd = lognormal_mean*1000/(32e-8);
	
	//cout << endl << endl << "by method 1, the ccd is: \t\t" << ccd << " microns \t\tfrom a mean of " << lognormal_mean << endl << endl << endl;
	//cout << endl << endl << "by method 2, the ccd is: \t\t" << (user->GetParameter(2) * TMath::Exp(0.5*TMath::Power(user->GetParameter(1),2)) + user->GetParameter(3))*1000/(32e-8) << " microns \t\tfrom a mean of " << (user->GetParameter(2) * TMath::Exp(0.5*TMath::Power(user->GetParameter(1),2)) + user->GetParameter(3)) << endl << endl << endl;
	
	return ccd;
}

// getPolarity returns the raw mean of the dataset
Double_t ccdAnalysis2::getRawMean( const char* _ChannelName ) {
	TH1D *hist;
	double *dataVar = 0;
	if (!strcmp(_ChannelName, "corInt")) {
		hist = fullHist_corInt;
		dataVar = &corInt;
	} else if (!strcmp(_ChannelName, "postInt")) {
		hist = fullHist_postInt;
		dataVar = &postInt;
	} else {
		// double &valToPrint = 0;
		cout << "invalid argument" << endl;
		return 0;
	}	
	
	if (nentries != (Long64_t)hist->GetEntries()) {
		cout << "REFILLING THE FULL-RANGE HISTOGRAM" << endl;
		for (Long64_t i = 0; i < this->nentries; i++) {
			this->dataTTree->GetEntry(i);
			hist->Fill(*dataVar);
		}
	}
	return hist->GetMean();
}

int ccdAnalysis2::makeh1( Int_t numBins, Double_t lb, Double_t ub, Int_t startIndex, Int_t endIndex ) {

	h1 = new TH1D("h1", "Diamond Signal Integral Histogram", numBins, lb, ub);
	h1->GetXaxis()->SetTitle("Vsec");
	h1->GetYaxis()->SetTitle("Counts");
	Long64_t i = 0;
	for (i = startIndex; i < this->nentries && i < endIndex; i++) {
		this->dataTTree->GetEntry(i);
		h1->Fill(corInt);
	}
	
	return i;
}

void ccdAnalysis2::ccdTimeSweep( int startIndex, int timeInterval, int upperLimit ) {
//	ccdAnalysis2 analysis1("tmp/test1.root");
	graph1 = new TGraph();
	int i = 0;
	int upperBound = -1;
	double upperBoundTime = 0;
	double nextTime = timeInterval;
	
//	while ((i+1)*20000 <= nentries && (i+1)*20000 < upperLimit) {
	while (upperBound <= nentries && upperBound <= upperLimit) {
//		cout << i << "\t" << i*20000 << "\t" << (i+1)*20000 << "\t\tccd=" << calcCCD(i*20000, (i+1)*20000) << " microns" << endl;
		ccdData *newFit = new ccdData;
//		analysis1.calcCCDv3(newFit, 0, -1, 83200, 0);
		calcCCDv3(newFit, upperBound+1, 10000, timeInterval, i);
		upperBound = newFit->upperBound_index;
		upperBoundTime = newFit->upperBound_time;
		nextTime = upperBoundTime + timeInterval;
		
		
		graph1->SetPoint(i, newFit->upperBound_time, newFit->computed_ccd);
		
//		fitData.push_back(*newFit);
		
		i++;
	}
	cout << "well: " << fitData.size() << endl;
	graph1->Draw();
}

void ccdAnalysis2::initAnalysisVars(int read ) {
	if (read) {
	
		analysisTTree->SetBranchAddress("fitHist_corInt", &fitHist_corInt, &b_fitHist_corInt);
		analysisTTree->SetBranchAddress("userFit_corInt", &userFit_corInt, &b_userFit_corInt);
		analysisTTree->SetBranchAddress("userDupFit_corInt", &userDupFit_corInt, &b_userDupFit_corInt);
		analysisTTree->SetBranchAddress("gausFit_corInt", &gausFit_corInt, &b_gausFit_corInt);
		analysisTTree->SetBranchAddress("combFit_corInt", &combFit_corInt, &b_combFit_corInt);
		
	} else {
		//setup the branches 

		b_lowBndTime = analysisTTree->Branch("lowBndTime", &lowBndTime, "lowBndTime/D");
		b_uppBndTime = analysisTTree->Branch("uppBndTime", &uppBndTime, "uppBndTime/D");
		b_lowBndIndex = analysisTTree->Branch("lowBndIndex", &lowBndIndex, "lowBndIndex/I");
		b_uppBndIndex = analysisTTree->Branch("uppBndIndex", &uppBndIndex, "uppBndIndex/I");

		//branches for the corInt ccd
		ccd_corInt, ccdErr_corInt, lowBndTime, uppBndTime, sigma_val_corInt, m_val_corInt, x0_val_corInt, mean_val_corInt, gaus_m_val_corInt = 0;
		lowBndIndex, uppBndIndex = 0;
		b_ccd_corInt = analysisTTree->Branch("ccd_corInt", &ccd_corInt, "ccd_corInt/D");
		b_ccdErr_corInt = analysisTTree->Branch("ccdErr_corInt", &ccdErr_corInt, "ccdErr_corInt/D");
		b_sigma_val_corInt = analysisTTree->Branch("sigma_val_corInt", &sigma_val_corInt, "sigma_val_corInt/D");
		b_m_val_corInt = analysisTTree->Branch("m_val_corInt", &m_val_corInt, "m_val_corInt/D");
		b_x0_val_corInt = analysisTTree->Branch("x0_val_corInt", &x0_val_corInt, "x0_val_corInt/D");
		b_mean_val_corInt = analysisTTree->Branch("mean_val_corInt", &mean_val_corInt, "mean_val_corInt/D");
		
		fitHist_corInt = 0;
		userFit_corInt = 0;
		userDupFit_corInt = 0;
		gausFit_corInt = 0;
		combFit_corInt = 0;
		ccdGraph_corInt = 0;
		backgroundGraph_corInt = 0;
		meanGraph_corInt = 0;
		correctedMeanGraph_corInt = 0;
	
//		ccd_corInt, ccdErr_corInt, lowBndTime, uppBndTime, sigma_val_corInt, m_val_corInt, x0_val_corInt, mean_val_corInt = 0;
//		lowBndIndex, uppBndIndex = 0;
	
		b_fitHist_corInt = analysisTTree->Branch("fitHist_corInt", "TH1D", &fitHist_corInt);
		b_userFit_corInt = analysisTTree->Branch("userFit_corInt", "TF1", &userFit_corInt);
		b_userDupFit_corInt = analysisTTree->Branch("userDupFit_corInt", "TF1", &userDupFit_corInt);
		b_gausFit_corInt = analysisTTree->Branch("gausFit_corInt", "TF1", &gausFit_corInt);
		b_combFit_corInt = analysisTTree->Branch("combFit_corInt", "TF1", &combFit_corInt);
		b_ccdGraph_corInt = analysisTTree->Branch("ccdGraph_corInt", "TGraph", &ccdGraph_corInt);
		b_backgroundGraph_corInt = analysisTTree->Branch("backgroundGraph_corInt", "TGraph", &backgroundGraph_corInt);
		b_meanGraph_corInt = analysisTTree->Branch("meanGraph_corInt", "TGraph", &meanGraph_corInt);
		b_correctedMeanGraph_corInt = analysisTTree->Branch("correctedMeanGraph_corInt", "TGraph", &correctedMeanGraph_corInt);

		//branches for the postInt ccd
		ccd_postInt, ccdErr_postInt, sigma_val_postInt, m_val_postInt, x0_val_postInt, mean_val_postInt = 0;
		b_ccd_postInt = analysisTTree->Branch("ccd_postInt", &ccd_postInt, "ccd_postInt/D");
		b_ccdErr_postInt = analysisTTree->Branch("ccdErr_postInt", &ccdErr_postInt, "ccdErr_postInt/D");
		b_sigma_val_postInt = analysisTTree->Branch("sigma_val_postInt", &sigma_val_postInt, "sigma_val_postInt/D");
		b_m_val_postInt = analysisTTree->Branch("m_val_postInt", &m_val_postInt, "m_val_postInt/D");
		b_x0_val_postInt = analysisTTree->Branch("x0_val_postInt", &x0_val_postInt, "x0_val_postInt/D");
		b_mean_val_postInt = analysisTTree->Branch("mean_val_postInt", &mean_val_postInt, "mean_val_postInt/D");
		b_gaus_m_val_postInt = analysisTTree->Branch("gaus_m_val_postInt", &gaus_m_val_postInt, "gaus_m_val_postInt/D");
		b_mean_val_adj_postInt = analysisTTree->Branch("mean_val_adj_postInt", &mean_val_adj_postInt, "mean_val_adj_postInt/D");
		
		fitHist_postInt = 0;
		userFit_postInt = 0;
		userDupFit_postInt = 0;
		gausFit_postInt = 0;
		combFit_postInt = 0;
		ccdGraph_postInt = 0;
		backgroundGraph_postInt = 0;
		mVal_diffGraph_postInt = 0;
		meanGraph_postInt = 0;
		correctedMeanGraph_postInt = 0;
	
		b_fitHist_postInt = analysisTTree->Branch("fitHist_postInt", "TH1D", &fitHist_postInt);
		b_userFit_postInt = analysisTTree->Branch("userFit_postInt", "TF1", &userFit_postInt);
		b_userDupFit_postInt = analysisTTree->Branch("userDupFit_postInt", "TF1", &userDupFit_postInt);
		b_gausFit_postInt = analysisTTree->Branch("gausFit_postInt", "TF1", &gausFit_postInt);
		b_combFit_postInt = analysisTTree->Branch("combFit_postInt", "TF1", &combFit_postInt);
		b_ccdGraph_postInt = analysisTTree->Branch("ccdGraph_postInt", "TGraph", &ccdGraph_postInt);
		b_backgroundGraph_postInt = analysisTTree->Branch("backgroundGraph_postInt", "TGraph", &backgroundGraph_postInt);
		b_mVal_diffGraph_postInt = analysisTTree->Branch("mVal_diffGraph_postInt", "TGraph", &mVal_diffGraph_postInt);
		b_meanGraph_postInt = analysisTTree->Branch("meanGraph_postInt", "TGraph", &meanGraph_postInt);
		b_correctedMeanGraph_postInt = analysisTTree->Branch("correctedMeanGraph_postInt", "TGraph", &correctedMeanGraph_postInt);

		//branches for the preInt background measurements
		mean_val_preInt = 0;
		b_mean_val_preInt = analysisTTree->Branch("mean_val_preInt", &mean_val_preInt, "mean_val_preInt/D");
		
		fitHist_preInt = 0;
		gausFit_preInt = 0;
		backgroundGraph_preInt = 0;
	
		b_fitHist_preInt = analysisTTree->Branch("fitHist_preInt", "TH1D", &fitHist_preInt);
		b_gausFit_preInt = analysisTTree->Branch("gausFit_preInt", "TF1", &gausFit_preInt);
		b_backgroundGraph_preInt = analysisTTree->Branch("backgroundGraph_preInt", "TGraph", &backgroundGraph_preInt);
	}
}

void ccdAnalysis2::ccdTimeSweepV2( const char * filename, const char * treeName, int startIndex, int timeInterval, int upperLimit ) {
//	graph1 = new TGraph();
	int i = 0;
	int upperBound = -1;
	Int_t endIndexVal = 0;
	Int_t endIndexValPrev = 0;
	double upperBoundTime = 0;
	double nextTime = timeInterval;
	
	
	this->analysisFile = new TFile( filename , "UPDATE");
	this->analysisTTree = new TTree( treeName, "" );
	
	initAnalysisVars(0);
	
	while (uppBndIndex <= nentries && uppBndIndex <= upperLimit) {
		// calcCCDTTree2( uppBndIndex+1, -1, timeInterval, i);
		endIndexVal = findIndexBounds(endIndexValPrev, -1, timeInterval);
		calcCCDTTree4(endIndexValPrev, endIndexVal, i, "corInt");
		// calcCCDTTree3(uppBndIndex+1, endIndexVal, i, "postInt",0,0,0,0);
//		upperBound = uppBndIndex;
//		upperBoundTime = uppBndTime;
//		nextTime = uppBndTime + timeInterval;
		
		ccdGraph_corInt->SetPoint(i, uppBndTime, ccd_corInt);
		backgroundGraph_corInt->SetPoint(i, uppBndTime, gaus_m_val_corInt);
		meanGraph_corInt->SetPoint(i, uppBndTime, mean_val_corInt);
		correctedMeanGraph_corInt->SetPoint(i, uppBndTime, mean_val_corInt/gaus_m_val_corInt);
		// ccdGraph_postInt->SetPoint(i, uppBndTime, ccd_postInt);
		// backgroundGraph_preInt->SetPoint(i, uppBndTime, mean_val_preInt);

		analysisTTree->Fill();

		endIndexValPrev = endIndexVal;
		
		i++;
	}
//	ccdGraph_corInt->Draw();

	ccdGraph_corInt->RemovePoint(ccdGraph_corInt->GetN()-1);
	backgroundGraph_corInt->RemovePoint(backgroundGraph_corInt->GetN()-1);
	meanGraph_corInt->RemovePoint(meanGraph_corInt->GetN()-1);
	correctedMeanGraph_corInt->RemovePoint(correctedMeanGraph_corInt->GetN()-1);
	analysisTTree->Fill();
	
	analysisTTree->Write();
	// ccdGraph_corInt->Draw();
}

void ccdAnalysis2::ReadccdTimeSweep( const char * filename, const char * treeName, int startIndex, int timeInterval, int upperLimit ) {
//	graph1 = new TGraph();
//	int i = 0;
//	int upperBound = -1;
//	double upperBoundTime = 0;
//	double nextTime = timeInterval;
	
	
	this->analysisFile = new TFile( filename , "UPDATE");
	this->analysisTTree = new TTree( treeName, "" );
	
	initAnalysisVars(1);

////	fitHist_corInt = new TH1D("testing1234", "testing1234", 10,0,10);
////	fitHist_corInt->Draw();
//	
//	while (uppBndIndex <= nentries && uppBndIndex <= upperLimit) {
//		
//		
//		
//		i++;
//	}
////	ccdGraph_corInt->Draw();

//	ccdGraph_corInt->RemovePoint(ccdGraph_corInt->GetN()-1);
//	analysisTTree->Fill();
//	
//	analysisTTree->Write();
//	ccdGraph_corInt->Draw();
}


void ccdAnalysis2::saveccdGraph_corInt( const char * baseFilename, const char * graphLabels ) { 
	
	ccdGraph_corInt->Draw();
	ccdGraph_corInt->SetTitle(graphLabels);
	char file1[128] = {0};
	char file2[128] = {0};
	
	strlcpy(file1, baseFilename, 128);
	strlcat(file1, ".eps", 128);
	strlcpy(file2, baseFilename, 128);
	strlcat(file2, ".pdf", 128);
	
	
	gPad->Print(file1);
	gPad->Print(file2);
	
}

ccdAnalysis2::~ccdAnalysis2()
{
//	if (!fChain) return;
//	delete fChain->GetCurrentFile();
	dataFile->Close();
}

#endif
