void
plotgen_1_29_19(int chanNum = 1)
{
	// gStyle->SetOptStat(00); //turns legend off
	gStyle->SetOptStat(1111); //turns legend on
	gStyle->SetOptFit(1);
	
	//ch->Add("~/work/data41/dune/kdochen/RawCNGSDataAna/resultData/Results_2015_03.root");
	//ch->Add("~/work/data41/dune/kdochen/RawCNGSDataAna/resultData/Results_2015_04.root");
	//ch->Add("~/work/data41/dune/kdochen/RawCNGSDataAna/resultData/Results_2015_05.root");
	//ch->Add("~/work/data41/dune/kdochen/RawCNGSDataAna/resultData/Results_2015_06.root");
	//ch->Add("~/work/data41/dune/kdochen/RawCNGSDataAna/resultData/Results_2015_07.root");
	TChain* ch = new TChain("MuMonData");
	ch->Add("~/work/lnxdata41/dune/njohnston/numi_diamond_data_conversion/data/results/Results_2017_11.root");
	ch->Add("~/work/lnxdata41/dune/njohnston/numi_diamond_data_conversion/data/results/Results_2017_12.root");
	
	TChain* ch2 = new TChain("MuMonData");
	ch2->Add("~/work/lnxdata41/dune/njohnston/numi_diamond_data_conversion/data/results/Results_2018_11_withtemp.root");
	ch2->Add("~/work/lnxdata41/dune/njohnston/numi_diamond_data_conversion/data/results/Results_2018_12_withtemp.root");
	// ch2->Add("~/work/lnxdata41/dune/njohnston/numi_diamond_data_conversion/data/results/Results_2019_01_withtemp.root");
	//ch2->Add("~/work/data41/dune/kdochen/RawCNGSDataAna/resultData/Results_2016_03.root");
	//ch2->Add("~/work/data41/dune/kdochen/RawCNGSDataAna/resultData/Results_2016_04.root");
	//ch2->Add("~/work/data41/dune/kdochen/RawCNGSDataAna/resultData/Results_2016_05.root");
	//ch2->Add("~/work/data41/dune/kdochen/RawCNGSDataAna/resultData/Results_2016_06.root");
	//ch2->Add("~/work/data41/dune/kdochen/RawCNGSDataAna/resultData/Results_2016_07.root");
	//ch2->Add("~/work/data41/dune/kdochen/RawCNGSDataAna/resultData/Results_2015_03.root");
	//ch2->Add("~/work/data41/dune/kdochen/RawCNGSDataAna/resultData/Results_2015_04.root");
	//ch2->Add("~/work/data41/dune/kdochen/RawCNGSDataAna/resultData/Results_2015_05.root");
	//ch2->Add("~/work/data41/dune/kdochen/RawCNGSDataAna/resultData/Results_2015_06.root");
	//ch2->Add("~/work/data41/dune/kdochen/RawCNGSDataAna/resultData/Results_2015_07.root");
	
	
	TCanvas* cLin = new TCanvas("cLin", "Linearity Canvas", 30, 30, 1600, 900); 

	double timei15 = 1427.13e6;
	double timef15 = 1434.6e6;
	
	double timei16 = 1457.1e6;
	double timef16 = 1467.95e6;
	double badhorni = 1433.755e6;
	double antii = 1467.2e6;
	double turnonf16 = 1458.8e6;
	
	//all the data
	TProfile* D1_15 = new TProfile("D1_15","Diamond 1 Nov & Dec 2017;POT / spill [x10^12];Diamond1 / POT [arb. units]",50,15,35); 
	TProfile* D1_15_2 = new TProfile("D1_15_2","Diamond 1 Nov & Dec 2017;POT / spill [x10^12];Diamond1 / POT [arb. units]",50,15,35); 
	TProfile* c2_15 = new TProfile("c2_15","Second Plot Nov & Dec 2017;POT / spill [x10^12];Diamond1 / POT [arb. units]",50,15,35); 
	TProfile* D1_16 = new TProfile("D1_16","Diamond 1 Mar - Jul 2016;POT / spill [x10^12];Diamond1 / POT [arb. units]",50,15,44);
	
	TProfile* D2_15 = new TProfile("D2_15","Diam2 vs POT '15;POT;Diamond2 / POT",50,15,35); 
	TProfile* D2_16 = new TProfile("D2_16","Diam2 vs POT (2016);POT;Diamond2 / POT",50,15,44);
	
	TString D1sigCut15 = TString::Format("(total[2]/pot) > %f && (total[2]/pot) < %f",-130e3,-80e3);
	TString D1sigCut16 = TString::Format("(total[1]/pot) > %f && (total[1]/pot) < %f",-130e3,-90e3);
	TString D2sigCut15 = TString::Format("(total[2]/pot) > %f && (total[2]/pot) < %f",-120e3,-80e3);
	TString D2sigCut16 = TString::Format("(total[2]/pot) > %f && (total[2]/pot) < %f",-125e3,-85e3);
	TString potCut = TString::Format("pot > %f",15.);
	TString DTpotCut = TString::Format("dt_pot > %f && dt_pot < %f",-1.,1.);
	TString DThornCut = TString::Format("dt_horn > %f && dt_horn < %f",-1.,1.);
	TString hornCut = TString::Format("hornI < %f", -190.);
	TString antiTimeCut = TString::Format("timeD < %f", antii);
	TString badHornTimeCut = TString::Format("timeD < %f", badhorni);
	TString turnOntimeCut16 = TString::Format("timeD < %f", turnonf16);

	// normalized signal vs time plot - tgraph
	TCanvas* cPot1 = new TCanvas("cPot1", "Signal vs Time Canvas", 30, 30, 1600, 900);
	cPot1->Divide(0,2);
	cPot1->cd(1);
	ch2->Draw(Form("total[%d]/pot:time",chanNum), hornCut +"&&"+ potCut +"&&"+ DTpotCut);
	TGraph *d1_17_pot1 = (TGraph*)gPad->GetPrimitive("Graph")->Clone("d1_17_pot1");
	gROOT->cd(0);
	d1_17_pot1->SetTitle("Polycrystal Nov 8th - Dec 31st 2018;Time;Signal / POT");
	d1_17_pot1->GetXaxis()->SetTimeDisplay(1);
	d1_17_pot1->GetXaxis()->SetNdivisions(-508);
	d1_17_pot1->GetXaxis()->SetTimeFormat("%m/%d"); 
	d1_17_pot1->GetXaxis()->SetTimeOffset(0,"utc");
	d1_17_pot1->GetYaxis()->SetRangeUser(7400,8000);
	d1_17_pot1->Draw("ap");
	cPot1->cd(2);
	// ch2->Draw("pot:timeD", hornCut +"&&"+ potCut +"&&"+ DTpotCut);
	// TGraph *pot_17 = (TGraph*)gPad->GetPrimitive("Graph")->Clone("pot_17");
	// gROOT->cd(0);
	// pot_17->SetTitle("POT vs Time (Fall 2018);Time;POT");
	// pot_17->GetXaxis()->SetTimeDisplay(1);
	// pot_17->GetXaxis()->SetNdivisions(-508);
	// pot_17->GetXaxis()->SetTimeFormat("%m/%d"); 
	// pot_17->GetXaxis()->SetTimeOffset(0,"utc");
	// pot_17->Draw("ap");
	ch2->Draw(Form("total[%d]/pot:time",chanNum+1), hornCut +"&&"+ potCut +"&&"+ DTpotCut);
	TGraph *d1_17_pot2 = (TGraph*)gPad->GetPrimitive("Graph")->Clone("d1_17_pot2");
	gROOT->cd(0);
	d1_17_pot2->SetTitle("Silicon Nov 8th - Dec 31st 2018;Time;Signal / POT");
	d1_17_pot2->GetXaxis()->SetTimeDisplay(1);
	d1_17_pot2->GetXaxis()->SetNdivisions(-508);
	d1_17_pot2->GetXaxis()->SetTimeFormat("%m/%d"); 
	d1_17_pot2->GetXaxis()->SetTimeOffset(0,"utc");
	d1_17_pot2->GetYaxis()->SetRangeUser(-155e3,-150e3);
	d1_17_pot2->Draw("ap");



	// normalized signal vs time and first temperature measurement tttgth
	TCanvas* cSig9 = new TCanvas("cSig9", "Investigation 9 - temperature plot 1", 30, 30, 1600, 900);
	cSig9->Divide(0,2);
	cSig9->cd(1);
	d1_17_pot1->Draw("ap");
	cSig9->cd(2);
	ch2->Draw("tttgth:timeD", hornCut +"&&"+ potCut +"&&"+ DTpotCut);
	TGraph *tttgth_fa18 = (TGraph*)gPad->GetPrimitive("Graph")->Clone("tttgth_fa18");
	gROOT->cd(0);
	tttgth_fa18->SetTitle("MMA1DS Channel 88 (subtracted pedestal, pot normalized) vs Time (Fall 2018);Time;Volts");
	tttgth_fa18->GetYaxis()->SetRangeUser(0.85,1.35);
	tttgth_fa18->GetXaxis()->SetTimeDisplay(1);
	tttgth_fa18->GetXaxis()->SetNdivisions(-508);
	tttgth_fa18->GetXaxis()->SetTimeFormat("%m/%d"); 
	tttgth_fa18->GetXaxis()->SetTimeOffset(0,"utc");
	tttgth_fa18->Draw("ap");




}

