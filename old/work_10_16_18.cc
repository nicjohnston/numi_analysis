
void
work_10_16_18()
{
	// gStyle->SetOptStat(00); //turns legend off
	gStyle->SetOptStat(1111); //turns legend on
	gStyle->SetOptFit(1);
	
	//ch->Add("~/work/data41/dune/kdochen/RawCNGSDataAna/resultData/Results_2015_03.root");
	//ch->Add("~/work/data41/dune/kdochen/RawCNGSDataAna/resultData/Results_2015_04.root");
	//ch->Add("~/work/data41/dune/kdochen/RawCNGSDataAna/resultData/Results_2015_05.root");
	//ch->Add("~/work/data41/dune/kdochen/RawCNGSDataAna/resultData/Results_2015_06.root");
	//ch->Add("~/work/data41/dune/kdochen/RawCNGSDataAna/resultData/Results_2015_07.root");
	TChain* ch = new TChain("MuMonData");
	ch->Add("~/work/data41/dune/njohnston/numi_diamond_data_conversion/data/results/Results_2017_11.root");
	ch->Add("~/work/data41/dune/njohnston/numi_diamond_data_conversion/data/results/Results_2017_12.root");
	
	TChain* ch2 = new TChain("MuMonData");
	ch2->Add("~/work/data41/dune/kdochen/RawCNGSDataAna/resultData/Results_2016_03.root");
	ch2->Add("~/work/data41/dune/kdochen/RawCNGSDataAna/resultData/Results_2016_04.root");
	ch2->Add("~/work/data41/dune/kdochen/RawCNGSDataAna/resultData/Results_2016_05.root");
	ch2->Add("~/work/data41/dune/kdochen/RawCNGSDataAna/resultData/Results_2016_06.root");
	ch2->Add("~/work/data41/dune/kdochen/RawCNGSDataAna/resultData/Results_2016_07.root");
	//ch2->Add("~/work/data41/dune/kdochen/RawCNGSDataAna/resultData/Results_2015_03.root");
	//ch2->Add("~/work/data41/dune/kdochen/RawCNGSDataAna/resultData/Results_2015_04.root");
	//ch2->Add("~/work/data41/dune/kdochen/RawCNGSDataAna/resultData/Results_2015_05.root");
	//ch2->Add("~/work/data41/dune/kdochen/RawCNGSDataAna/resultData/Results_2015_06.root");
	//ch2->Add("~/work/data41/dune/kdochen/RawCNGSDataAna/resultData/Results_2015_07.root");
	
	
	TCanvas* cLin = new TCanvas("cLin", "Linearity Canvas", 30, 30, 1600, 900); 
	TCanvas* c1 = new TCanvas("c1", "canvas1", 30, 30, 700, 500);  
	TCanvas* c2 = new TCanvas("c2", "canvas2", 50, 50, 700, 500);
	TCanvas* c3 = new TCanvas("c3", "canvas3", 70, 70, 700, 500);
	//TCanvas* c4 = new TCanvas("c4", "canvas4", 90, 90, 700, 500);
	
	c1->Divide(0,2);
	c2->Divide(0,2);
	
	double timei15 = 1427.13e6;
	double timef15 = 1434.6e6;
	
	double timei16 = 1457.1e6;
	double timef16 = 1467.95e6;
	double badhorni = 1433.755e6;
	double antii = 1467.2e6;
	double turnonf16 = 1458.8e6;
	
	//all the data
	TProfile* D1_15 = new TProfile("D1_15","Diamond 1 Nov & Dec 2017;POT / spill [x10^12];Diamond1 / POT [arb. units]",50,15,35); 
	TProfile* D1_15_2 = new TProfile("D1_15_2","Diamond 1 Nov & Dec 2017;POT / spill [x10^12];Diamond1 / POT [arb. units]",50,15,35); 
	TProfile* c2_15 = new TProfile("c2_15","Second Plot Nov & Dec 2017;POT / spill [x10^12];Diamond1 / POT [arb. units]",50,15,35); 
	TProfile* D1_16 = new TProfile("D1_16","Diamond 1 Mar - Jul 2016;POT / spill [x10^12];Diamond1 / POT [arb. units]",50,15,44);
	
	TProfile* D2_15 = new TProfile("D2_15","Diam2 vs POT '15;POT;Diamond2 / POT",50,15,35); 
	TProfile* D2_16 = new TProfile("D2_16","Diam2 vs POT '16;POT;Diamond2 / POT",50,15,44);
	
	TString D1sigCut15 = TString::Format("(total[2]/pot) > %f && (total[2]/pot) < %f",-130e3,-80e3);
	TString D1sigCut16 = TString::Format("(total[1]/pot) > %f && (total[1]/pot) < %f",-130e3,-90e3);
	TString D2sigCut15 = TString::Format("(total[2]/pot) > %f && (total[2]/pot) < %f",-120e3,-80e3);
	TString D2sigCut16 = TString::Format("(total[2]/pot) > %f && (total[2]/pot) < %f",-125e3,-85e3);
	TString potCut = TString::Format("pot > %f",15.);
	TString DTpotCut = TString::Format("dt_pot > %f && dt_pot < %f",-1.,1.);
	TString hornCut = TString::Format("hornI < %f", -190.);
	TString antiTimeCut = TString::Format("timeD < %f", antii);
	TString badHornTimeCut = TString::Format("timeD < %f", badhorni);
	TString turnOntimeCut16 = TString::Format("timeD < %f", turnonf16);

	// linearity plot -- compare Kerrie's 2016 linearity plot to a linearity plot of the fall 2017 data
	TProfile* d1_16_lin = new TProfile("d1_16_lin","Diamond 1 Total Signal vs POT '16;POT;Diamond1 / POT",50,15,35); 
	TProfile* d1_18_lin = new TProfile("d1_18_lin","Diamond 1 Total Signal vs POT '18;POT;Diamond1 / POT",50,15,35); 
	d1_16_lin->GetYaxis()->SetTitleOffset(1.4);
	d1_16_lin->GetYaxis()->SetRangeUser(-130e3,-105e3);
	d1_18_lin->GetYaxis()->SetTitleOffset(1.4);
	d1_18_lin->GetYaxis()->SetRangeUser(-89.5e3,-64.5e3);
	// gStyle->SetMaxDigitsY(2);
	cLin->Divide(0,2);
	cLin->cd(1);
	ch2->Draw("total[1]/pot:pot>>d1_16_lin", hornCut +"&&"+ potCut +"&&"+ DTpotCut +"&&"+ antiTimeCut +"&&"+ D1sigCut16 +"&&"+ turnOntimeCut16 +"&& timeD > 1457.48e6");
	d1_16_lin->Fit("pol1");

	cLin->cd(2);
	ch->Draw("total[1]/pot:pot>>d1_18_lin", hornCut +"&&"+ potCut +"&&"+ DTpotCut);
	d1_18_lin->Fit("pol1");
	

	// normalized signal vs time plot - histogram
	// TCanvas* cPot1 = new TCanvas("cPot1", "Signal vs Time Canvas", 30, 30, 1600, 900);
	// // TGraph *d1_16_pot1 = new TGraph();//"d1_16_pot1", "Diamond 1 Total Signal vs Time '16;Time;Diamond1 / POT");
	// // TGraph *pot_18 = new TGraph();//"pot_18", "POT vs Time '18;Time;POT");
	// cPot1->Divide(0,2);
	// cPot1->cd(1);
	// TString D1signalCut15 = TString::Format("(total[1]/pot) > %f && (total[1]/pot) < %f",-130e3,-90e3);
	// ch->Draw("total[1]/pot:timeD>>d1_18_pot1", hornCut +"&&"+ potCut +"&&"+ DTpotCut);
	// TGraph *d1_16_pot1 = (TGraph*)gPad->GetPrimitive("d1_18_pot1")->Clone();
	// // d1_16_pot1->SetTitle("Diamond 1 Total Signal vs Time '16;Time;Diamond1 / POT");
	// d1_16_pot1->GetXaxis()->SetTimeDisplay(1);
	// cPot1->cd(2);
	// ch->Draw("pot:timeD>>pot_18", hornCut +"&&"+ potCut +"&&"+ DTpotCut);
	// TGraph *pot_18 = (TGraph*)gPad->GetPrimitive("pot_18");
	// pot_18->SetTitle("POT vs Time '18;Time;POT");
	// // pot_18->GetXaxis()->SetTimeDisplay(1);
	// gPad->Modified();
	// gPad->Update();
	// // pot_18->Draw();


	TCanvas* cPot1 = new TCanvas("cPot1", "Signal vs Time Canvas", 30, 30, 1600, 900);
	cPot1->Divide(0,2);
	cPot1->cd(1);
	// TGraph *d1_16_pot1 = new TGraph();//"d1_16_pot1", "Diamond 1 Total Signal vs Time '16;Time;Diamond1 / POT");
	// TGraph *pot_18 = new TGraph();//"pot_18", "POT vs Time '18;Time;POT");
	
	TString D1signalCut15 = TString::Format("(total[1]/pot) > %f && (total[1]/pot) < %f",-130e3,-90e3);
	ch->Draw("total[1]/pot:time", hornCut +"&&"+ potCut +"&&"+ DTpotCut);
	TGraph *d1_18_pot1 = (TGraph*)gPad->GetPrimitive("Graph")->Clone("d1_18_pot1");
	gROOT->cd(0);
	d1_18_pot1->SetTitle("Diamond 1 Total Signal vs Time '16;Time;Diamond1 / POT");
	d1_18_pot1->GetXaxis()->SetTimeDisplay(1);
	d1_18_pot1->GetXaxis()->SetNdivisions(-508);
	d1_18_pot1->GetXaxis()->SetTimeFormat("%m/%d"); 
	d1_18_pot1->GetXaxis()->SetTimeOffset(0,"utc");
	d1_18_pot1->Draw("ap");



	cPot1->cd(2);
	ch->Draw("pot:timeD", hornCut +"&&"+ potCut +"&&"+ DTpotCut);
	TGraph *pot_18 = (TGraph*)gPad->GetPrimitive("Graph")->Clone("pot_18");
	gROOT->cd(0);
	pot_18->SetTitle("Diamond 1 Total Signal vs Time '16;Time;Diamond1 / POT");
	pot_18->GetXaxis()->SetTimeDisplay(1);
	pot_18->GetXaxis()->SetNdivisions(-508);
	pot_18->GetXaxis()->SetTimeFormat("%m/%d"); 
	pot_18->GetXaxis()->SetTimeOffset(0,"utc");
	pot_18->Draw("ap");
	// TGraph *pot_18 = (TGraph*)gPad->GetPrimitive("pot_18");
	// pot_18->SetTitle("POT vs Time '18;Time;POT");
	// // pot_18->GetXaxis()->SetTimeDisplay(1);
	// gPad->Modified();
	// gPad->Update();
	// pot_18->Draw();











	c1->cd(1);
	D1_15->GetYaxis()->SetTitleOffset(1.4);
	//D1_15->GetYaxis()->SetRangeUser(-81e3,-73e3);
	//ch->Draw("total[1]/pot:pot>>D1_15", hornCut +"&&"+ potCut +"&&"+ DTpotCut +"&&"+ badHornTimeCut +"&&"+ D1sigCut15 +"&& timeD > 1428.8e6 && timeD < 1433.5e6"); 
	ch->Draw("total[1]/pot:pot>>D1_15");//,  hornCut +"&&"+ potCut); 
	//ch->Draw("total[1]/pot:pot", hornCut +"&&"+ potCut +"&&"+ DTpotCut +"&&"+ badHornTimeCut +"&&"+ D1sigCut15); //scatter
	
	c1->cd(2);
	//ch->Draw("total[1]/pot:pot>>D1_15_2",  potCut); 
	//D1_15_2->GetYaxis()->SetRangeUser(-81e3,-73e3);
	ch->Draw("pot:timeD");//, hornCut +"&&"+ potCut);
	

	// normalized signal vs time plot - profile
	TProfile* d1_16_PotVsTime = new TProfile("d1_16_PotVsTime","Diamond 1 Total Signal vs Time '16;Time;Diamond1 / POT",50,15,35); 
	TProfile* d1_18_PotVsTime = new TProfile("d1_18_PotVsTime","Diamond 1 Total Signal vs Time '18;Time;Diamond1 / POT",50,15,35);

	c3->Divide(0,2);
	c3->cd(1);
	TString D1signalCut15 = TString::Format("(total[1]/pot) > %f && (total[1]/pot) < %f",-130e3,-90e3);
	ch->Draw("total[1]/pot:timeD", hornCut +"&&"+ potCut +"&&"+ DTpotCut);
	c3->cd(2);
	ch->Draw("pot:timeD", hornCut +"&&"+ potCut +"&&"+ DTpotCut);
	
	c2->cd(1);
	D1_16->GetYaxis()->SetTitleOffset(1.4);
	D1_16->GetYaxis()->SetRangeUser(-130e3,-105e3);
	ch2->Draw("total[1]/pot:pot>>D1_16", hornCut +"&&"+ potCut +"&&"+ DTpotCut +"&&"+ antiTimeCut +"&&"+ D1sigCut16 +"&&"+ turnOntimeCut16 +"&& timeD > 1457.48e6");
	//ch2->Draw("total[1]/pot:pot", hornCut +"&&"+ potCut +"&&"+ DTpotCut +"&&"+ antiTimeCut +"&&"+ D1sigCut16 +"&&"+ turnOntimeCut16); //scatter
	
	c2->cd(2);
	ch2->Draw("pot:timeD>>c2_15", hornCut +"&&"+ potCut);
	
	//DIAM2//
	//c3->cd();
	//D2_15->GetYaxis()->SetTitleOffset(1.4);
	//D2_15->GetYaxis()->SetRangeUser(-120e3,-95e3);
	//ch->Draw("total[2]/pot:pot>>D2_15", hornCut +"&&"+ potCut +"&&"+ DTpotCut +"&&"+ badHornTimeCut +"&&"+ D2sigCut15);
	//ch->Draw("total[2]/pot:pot", hornCut +"&&"+ potCut +"&&"+ DTpotCut +"&&"+ badHornTimeCut); //scatter
	//ch->Draw("pot:timeD", hornCut +"&&"+ potCut +"&&"+ DTpotCut +"&&"+ badHornTimeCut); //scatter
	
	//c4->cd();
	//D2_16->GetYaxis()->SetTitleOffset(1.4);
	//D2_16->GetYaxis()->SetRangeUser(-125e3,-80e3);
	//ch2->Draw("total[2]/pot:pot>>D2_16", hornCut +"&&"+ potCut +"&&"+ DTpotCut +"&&"+ antiTimeCut +"&&"+ D2sigCut16 +"&&"+ turnOntimeCut16);
	//ch2->Draw("total[1]/pot:pot", hornCut +"&&"+ potCut +"&&"+ DTpotCut +"&&"+ antiTimeCut +"&&"+ D2sigCut16 +"&&"+ turnOntimeCut16); //scatter
	//ch2->Draw("pot:timeD", hornCut +"&&"+ potCut +"&&"+ DTpotCut +"&&"+ antiTimeCut +"&&"+ D2sigCut16 +"&&"+ turnOntimeCut16 +"&& timeD > 1457.48e6"); //scatter

  
  

  


}

