/*
Grabs results files from '15 and '16
Compares diamonds to pot to check linearity

pot > 15 && dt_pot < 2 && hornI<-190
 */
 //1467043565 June 27, 2016
void
linearity()
{
  gStyle->SetOptStat(00); //turns legend off
  //gStyle->SetOptStat(11); //turns legend on
  //gStyle->SetOptFit(1);

  TChain* ch = new TChain("MuMonData");
  //ch->Add("~/work/data41/dune/kdochen/RawCNGSDataAna/resultData/Results_2015_03.root");
  //ch->Add("~/work/data41/dune/kdochen/RawCNGSDataAna/resultData/Results_2015_04.root");
  //ch->Add("~/work/data41/dune/kdochen/RawCNGSDataAna/resultData/Results_2015_05.root");
  //ch->Add("~/work/data41/dune/kdochen/RawCNGSDataAna/resultData/Results_2015_06.root");
  //ch->Add("~/work/data41/dune/kdochen/RawCNGSDataAna/resultData/Results_2015_07.root");
  ch->Add("~/work/data41/dune/njohnston/numi_diamond_data_conversion/data/results/Results_2017_11.root");
  ch->Add("~/work/data41/dune/njohnston/numi_diamond_data_conversion/data/results/Results_2017_12.root");
  
  TChain* ch2 = new TChain("MuMonData");
  ch2->Add("~/work/data41/dune/kdochen/RawCNGSDataAna/resultData/Results_2015_03.root");
  ch2->Add("~/work/data41/dune/kdochen/RawCNGSDataAna/resultData/Results_2015_04.root");
  ch2->Add("~/work/data41/dune/kdochen/RawCNGSDataAna/resultData/Results_2015_05.root");
  ch2->Add("~/work/data41/dune/kdochen/RawCNGSDataAna/resultData/Results_2015_06.root");
  ch2->Add("~/work/data41/dune/kdochen/RawCNGSDataAna/resultData/Results_2015_07.root");
  //ch2->Add("~/work/data41/dune/kdochen/RawCNGSDataAna/resultData/Results_2016_03.root");
  //ch2->Add("~/work/data41/dune/kdochen/RawCNGSDataAna/resultData/Results_2016_04.root");
  //ch2->Add("~/work/data41/dune/kdochen/RawCNGSDataAna/resultData/Results_2016_05.root");
  //ch2->Add("~/work/data41/dune/kdochen/RawCNGSDataAna/resultData/Results_2016_06.root");
  //ch2->Add("~/work/data41/dune/kdochen/RawCNGSDataAna/resultData/Results_2016_07.root");


  TCanvas* c1 = new TCanvas("c1", "canvas1", 30, 30, 700, 500);  
  TCanvas* c2 = new TCanvas("c2", "canvas2", 50, 50, 700, 500);
  TCanvas* c3 = new TCanvas("c3", "canvas3", 70, 70, 700, 500);
  TCanvas* c4 = new TCanvas("c4", "canvas4", 90, 90, 700, 500);


  double timei15 = 1427.13e6;
  double timef15 = 1434.6e6;

  double timei16 = 1457.1e6;
  double timef16 = 1467.95e6;
  double badhorni = 1433.755e6;
  double antii = 1467.2e6;
  double turnonf16 = 1458.8e6;

  //all the data
  TProfile* D1_15 = new TProfile("D1_15","2015;POT / spill [x10^12];Diamond1 / POT [arb. units]",50,15,35); 
  TProfile* D1_16 = new TProfile("D1_16","2016;POT / spill [x10^12];Diamond1 / POT [arb. units]",50,15,44);

  TProfile* D2_15 = new TProfile("D2_15","Diam2 vs POT '15;POT;Diamond2 / POT",50,15,35); 
  TProfile* D2_16 = new TProfile("D2_16","Diam2 vs POT '16;POT;Diamond2 / POT",50,15,44);

  TString D1sigCut15 = TString::Format("(total[2]/pot) > %f && (total[2]/pot) < %f",-130e3,-80e3);
  TString D1sigCut16 = TString::Format("(total[1]/pot) > %f && (total[1]/pot) < %f",-130e3,-90e3);
  TString D2sigCut15 = TString::Format("(total[2]/pot) > %f && (total[2]/pot) < %f",-120e3,-80e3);
  TString D2sigCut16 = TString::Format("(total[2]/pot) > %f && (total[2]/pot) < %f",-125e3,-85e3);
  TString potCut = TString::Format("pot > %f",15.);
  TString DTpotCut = TString::Format("dt_pot > %f && dt_pot < %f",-1.,1.);
  TString hornCut = TString::Format("hornI < %f", -190.);
  TString antiTimeCut = TString::Format("timeD < %f", antii);
  TString badHornTimeCut = TString::Format("timeD < %f", badhorni);
  TString turnOntimeCut16 = TString::Format("timeD < %f", turnonf16);

  //Diam1//
  c1->cd();
  D1_15->GetYaxis()->SetTitleOffset(1.4);
  //D1_15->GetYaxis()->SetRangeUser(-130e3,-105e3);
  //ch->Draw("total[1]/pot:pot>>D1_15", hornCut +"&&"+ potCut +"&&"+ DTpotCut +"&&"+ badHornTimeCut +"&&"+ D1sigCut15 +"&& timeD > 1428.8e6 && timeD < 1433.5e6"); 
  ch->Draw("total[1]/pot:pot>>D1_15", hornCut +"&&"+ potCut); 
  //ch->Draw("total[1]/pot:pot", hornCut +"&&"+ potCut +"&&"+ DTpotCut +"&&"+ badHornTimeCut +"&&"+ D1sigCut15); //scatter


  c2->cd();
  D1_16->GetYaxis()->SetTitleOffset(1.4);
  D1_16->GetYaxis()->SetRangeUser(-130e3,-105e3);
  ch2->Draw("total[1]/pot:pot>>D1_16", hornCut +"&&"+ potCut +"&&"+ DTpotCut +"&&"+ antiTimeCut +"&&"+ D1sigCut16 +"&&"+ turnOntimeCut16 +"&& timeD > 1457.48e6");
  //ch2->Draw("total[1]/pot:pot", hornCut +"&&"+ potCut +"&&"+ DTpotCut +"&&"+ antiTimeCut +"&&"+ D1sigCut16 +"&&"+ turnOntimeCut16); //scatter


  //DIAM2//
  c3->cd();
  D2_15->GetYaxis()->SetTitleOffset(1.4);
  D2_15->GetYaxis()->SetRangeUser(-120e3,-95e3);
  ch->Draw("total[2]/pot:pot>>D2_15", hornCut +"&&"+ potCut +"&&"+ DTpotCut +"&&"+ badHornTimeCut +"&&"+ D2sigCut15);
  //ch->Draw("total[2]/pot:pot", hornCut +"&&"+ potCut +"&&"+ DTpotCut +"&&"+ badHornTimeCut); //scatter
ch->Draw("pot:timeD", hornCut +"&&"+ potCut +"&&"+ DTpotCut +"&&"+ badHornTimeCut); //scatter

  c4->cd();
  D2_16->GetYaxis()->SetTitleOffset(1.4);
  //D2_16->GetYaxis()->SetRangeUser(-125e3,-80e3);
  //ch2->Draw("total[2]/pot:pot>>D2_16", hornCut +"&&"+ potCut +"&&"+ DTpotCut +"&&"+ antiTimeCut +"&&"+ D2sigCut16 +"&&"+ turnOntimeCut16);
  //ch2->Draw("total[1]/pot:pot", hornCut +"&&"+ potCut +"&&"+ DTpotCut +"&&"+ antiTimeCut +"&&"+ D2sigCut16 +"&&"+ turnOntimeCut16); //scatter
  //ch2->Draw("pot:timeD", hornCut +"&&"+ potCut +"&&"+ DTpotCut +"&&"+ antiTimeCut +"&&"+ D2sigCut16 +"&&"+ turnOntimeCut16 +"&& timeD > 1457.48e6"); //scatter




  


}

