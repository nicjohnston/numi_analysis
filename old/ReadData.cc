// ReadData.cc
#include "TFile.h"
#include "TTree.h"
#include "TObjArray.h"
#include "TH1.h"
// Filename is the path to a muon detector file
void LoopOverData(const char* filename) {
	TFile f(filename); // Opened for reading
	TTree* tree = static_cast<TTree*> ( f.Get("MuonData") );
	TObjArray* arr = 0;
	unsigned long long time; // AKA ULong64_t. A Unix timestamp
	int usec; // Microseconds part of timestamp
	double timeD;
	tree->SetBranchAddress("data",&arr);
	tree->SetBranchAddress("time",&time);
	tree->SetBranchAddress("usec",&usec);
	int n_entries = tree->GetEntries();
	TCanvas * c1 = new TCanvas("c1","c1");
	tree->GetEntry(0);
	TH1* trigger = static_cast<TH1*> (arr->At(0));
	TH1* cherenkov = static_cast<TH1*> (arr->At(1));
	TH1* diamond1 = static_cast<TH1*> (arr->At(2));
	TH1* diamond2 = static_cast<TH1*> (arr->At(3));
	TH1* diamond3 = static_cast<TH1*> (arr->At(4));
	c1->cd();
	diamond1->Draw();
	// TFile f(filename); // Opened for reading
	// TTree* tree = static_cast<TTree*> ( f.Get("MuonData") );
	// TObjArray* arr = 0;
	// unsigned long long time; // AKA ULong64_t. A Unix timestamp
	// int usec; // Microseconds part of timestamp
	// tree->SetBranchAddress("data",&arr);
	// tree->SetBranchAddress("time",&time);
	// tree->SetBranchAddress("usec",&usec);
	// TH1* trigger = static_cast<TH1*> (arr->At(0));
	// TH1* cherenkov = static_cast<TH1*> (arr->At(1));
	// TH1* diamond1 = static_cast<TH1*> (arr->At(2));
	// TH1* diamond2 = static_cast<TH1*> (arr->At(3));
	// TH1* diamond3 = static_cast<TH1*> (arr->At(4));
	// int n_entries = tree->GetEntries();
	// tree->GetEntry(0);
	// diamond1->Draw();
	// // for (int idx = 0; idx<n_entries; idx++){
	// // 	tree->GetEntry(idx);
	// // 	// Take any actions here
	// // 	// ...
	// // }
}