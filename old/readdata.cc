// ReadData.cc
#include "TFile.h"
#include "TTree.h"
#include "TObjArray.h"
#include "TH1.h"
// Filename is the path to a muon detector file
using namespace std;
void LoopOverData(const char* filename, const int index, const int tkinte)
{
	TFile f(filename); // Opened for reading
		std::cout << "test1" << std::endl;
	TTree* tree = static_cast<TTree*> ( f.Get("MuonData") );
	TObjArray* arr = 0;
	
	unsigned long long time; // AKA ULong64_t. A Unix timestamp
	int usec; // Microseconds part of timestamp
	double timeD;
	tree->SetBranchAddress("data",&arr);
	tree->SetBranchAddress("time",&time);
	tree->SetBranchAddress("usec",&usec);
//	tree->SetBranchAddress("timeD",&timeD);
	int n_entries = tree->GetEntries();
		
	TGraph *dt = new TGraph();
	
	
	TCanvas * c1 = new TCanvas("c1","c1");
//	TCanvas * c2 = new TCanvas("c2","c2");
//	TCanvas * c3 = new TCanvas("c3","c3");
	
	tree->GetEntry(index);
	TH1* trigger = static_cast<TH1*> (arr->At(0));
	TH1* cherenkov = static_cast<TH1*> (arr->At(1));
	TH1* diamond1 = static_cast<TH1*> (arr->At(2));
	TH1* diamond2 = static_cast<TH1*> (arr->At(3));
	TH1* diamond3 = static_cast<TH1*> (arr->At(4));

	c1->cd();
	diamond1->Draw();
	timeD = time+0.000001*usec;
	cout.precision(16);
	cout << "time= " << time << endl;
	cout << "usec= " << usec << endl;
	cout << "timeD= " << timeD << endl;
	cout << "n_entries= " << n_entries << endl;
	cout << "integral= " << diamond1->Integral() << endl;
//	c2->cd();
//	diamond2->Draw();
//	c3->cd();
//	diamond3->Draw();
	if (tkinte == 1) {
		TCanvas * c4 = new TCanvas("c4","c4");
		for (int idx = 0; idx<n_entries; idx++){
			tree->GetEntry(idx);
	timeD = time+0.000001*usec;
	//			std::cout << "test5" << std::endl;
			TH1* trigger = static_cast<TH1*> (arr->At(0));
			TH1* cherenkov = static_cast<TH1*> (arr->At(1));
			TH1* diamond1 = static_cast<TH1*> (arr->At(2));
			TH1* diamond2 = static_cast<TH1*> (arr->At(3));
			TH1* diamond3 = static_cast<TH1*> (arr->At(4));
			// Take any actions here
			// ...
	//		c1->cd();
	//		diamond1->Draw();
	//		c2->cd();
	//		diamond2->Draw();
	//		c3->cd();
	//		diamond3->Draw();
			dt->SetPoint(idx,timeD,diamond1->Integral());
		}
		c4->cd();
		   dt->GetXaxis()->SetTimeDisplay(1);
   dt->GetXaxis()->SetLabelSize(0.03);
   dt->GetXaxis()->SetTimeFormat("%H:%M:%S");
   dt->GetXaxis()->SetTimeOffset(0,"gmt");
		dt->Draw();
		TGaxis *axis = new TGaxis(gPad->GetUxmin(),gPad->GetUymax(),gPad->GetUxmax(),gPad->GetUymax(),0,1000,510,"+L");
	axis->Draw();
	}
}
