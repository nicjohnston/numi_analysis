void
work_02_07_19(int chanNum = 1)
{
	// gStyle->SetOptStat(00); //turns legend off
	gStyle->SetOptStat(1111); //turns legend on
	gStyle->SetOptFit(1);
	
	//ch->Add("~/work/data41/dune/kdochen/RawCNGSDataAna/resultData/Results_2015_03.root");
	//ch->Add("~/work/data41/dune/kdochen/RawCNGSDataAna/resultData/Results_2015_04.root");
	//ch->Add("~/work/data41/dune/kdochen/RawCNGSDataAna/resultData/Results_2015_05.root");
	//ch->Add("~/work/data41/dune/kdochen/RawCNGSDataAna/resultData/Results_2015_06.root");
	//ch->Add("~/work/data41/dune/kdochen/RawCNGSDataAna/resultData/Results_2015_07.root");
	TChain* ch = new TChain("MuMonData");
	ch->Add("~/work/lnxdata41/dune/njohnston/numi_diamond_data_conversion/data/results/Results_2017_11.root");
	ch->Add("~/work/lnxdata41/dune/njohnston/numi_diamond_data_conversion/data/results/Results_2017_12.root");
	
	TChain* ch2 = new TChain("MuMonData");
	ch2->Add("~/work/lnxdata41/dune/njohnston/numi_diamond_data_conversion/data/results/Results_2018_11_withtemp2.root");
	ch2->Add("~/work/lnxdata41/dune/njohnston/numi_diamond_data_conversion/data/results/Results_2018_12_withtemp2.root");
	ch2->Add("~/work/lnxdata41/dune/njohnston/numi_diamond_data_conversion/data/results/Results_2019_01_withtemp2.root");
	//ch2->Add("~/work/data41/dune/kdochen/RawCNGSDataAna/resultData/Results_2016_03.root");
	//ch2->Add("~/work/data41/dune/kdochen/RawCNGSDataAna/resultData/Results_2016_04.root");
	//ch2->Add("~/work/data41/dune/kdochen/RawCNGSDataAna/resultData/Results_2016_05.root");
	//ch2->Add("~/work/data41/dune/kdochen/RawCNGSDataAna/resultData/Results_2016_06.root");
	//ch2->Add("~/work/data41/dune/kdochen/RawCNGSDataAna/resultData/Results_2016_07.root");
	//ch2->Add("~/work/data41/dune/kdochen/RawCNGSDataAna/resultData/Results_2015_03.root");
	//ch2->Add("~/work/data41/dune/kdochen/RawCNGSDataAna/resultData/Results_2015_04.root");
	//ch2->Add("~/work/data41/dune/kdochen/RawCNGSDataAna/resultData/Results_2015_05.root");
	//ch2->Add("~/work/data41/dune/kdochen/RawCNGSDataAna/resultData/Results_2015_06.root");
	//ch2->Add("~/work/data41/dune/kdochen/RawCNGSDataAna/resultData/Results_2015_07.root");
	
	
	TCanvas* cLin = new TCanvas("cLin", "Linearity Canvas", 30, 30, 1600, 900); 

	double timei15 = 1427.13e6;
	double timef15 = 1434.6e6;
	
	double timei16 = 1457.1e6;
	double timef16 = 1467.95e6;
	double badhorni = 1433.755e6;
	double antii = 1467.2e6;
	double turnonf16 = 1458.8e6;
	
	//all the data
	TProfile* D1_15 = new TProfile("D1_15","Diamond 1 Nov & Dec 2017;POT / spill [x10^12];Diamond1 / POT [arb. units]",50,15,35); 
	TProfile* D1_15_2 = new TProfile("D1_15_2","Diamond 1 Nov & Dec 2017;POT / spill [x10^12];Diamond1 / POT [arb. units]",50,15,35); 
	TProfile* c2_15 = new TProfile("c2_15","Second Plot Nov & Dec 2017;POT / spill [x10^12];Diamond1 / POT [arb. units]",50,15,35); 
	TProfile* D1_16 = new TProfile("D1_16","Diamond 1 Mar - Jul 2016;POT / spill [x10^12];Diamond1 / POT [arb. units]",50,15,44);
	
	TProfile* D2_15 = new TProfile("D2_15","Diam2 vs POT '15;POT;Diamond2 / POT",50,15,35); 
	TProfile* D2_16 = new TProfile("D2_16","Diam2 vs POT (2016);POT;Diamond2 / POT",50,15,44);
	
	TString D1sigCut15 = TString::Format("(total[2]/pot) > %f && (total[2]/pot) < %f",-130e3,-80e3);
	TString D1sigCut16 = TString::Format("(total[1]/pot) > %f && (total[1]/pot) < %f",-130e3,-90e3);
	TString D2sigCut15 = TString::Format("(total[2]/pot) > %f && (total[2]/pot) < %f",-120e3,-80e3);
	TString D2sigCut16 = TString::Format("(total[2]/pot) > %f && (total[2]/pot) < %f",-125e3,-85e3);
	TString potCut = TString::Format("pot > %f",15.);
	TString DTpotCut = TString::Format("dt_pot > %f && dt_pot < %f",-1.,1.);
	TString DThornCut = TString::Format("dt_horn > %f && dt_horn < %f",-1.,1.);
	TString hornCut = TString::Format("hornI < %f", -190.);
	TString antiTimeCut = TString::Format("timeD < %f", antii);
	TString badHornTimeCut = TString::Format("timeD < %f", badhorni);
	TString turnOntimeCut16 = TString::Format("timeD < %f", turnonf16);

	// linearity plot -- compare Kerrie's 2016 linearity plot to a linearity plot of the fall 2017 data
	TCanvas* cLin = new TCanvas("cLin", "Linearity Canvas", 30, 30, 1600, 900); 
	TProfile* d1_16_lin = new TProfile("d1_16_lin","Diamond 1 Total Signal vs POT (Fall 2018);POT;Diamond1 / POT",50,15,55); 
	TProfile* d1_17_lin = new TProfile("d1_17_lin","Diamond 1 Total Signal vs POT (Fall 2017);POT;Diamond1 / POT",50,15,55); 
	d1_16_lin->GetYaxis()->SetTitleOffset(1.4);
	//d1_16_lin->GetYaxis()->SetRangeUser(-130e3,-105e3);
	d1_16_lin->GetYaxis()->SetRangeUser(-89.5e3+5e3,-64.5e3+5e3);
	d1_17_lin->GetYaxis()->SetTitleOffset(1.4);
	d1_17_lin->GetYaxis()->SetRangeUser(-89.5e3,-64.5e3);
	// gStyle->SetMaxDigitsY(2);
	cLin->Divide(0,2);
	cLin->cd(1);
	//ch2->Draw("total[3]/pot:pot>>d1_16_lin", hornCut +"&&"+ potCut +"&&"+ DTpotCut +"&&"+ antiTimeCut +"&&"+ D1sigCut16 +"&&"+ turnOntimeCut16 +"&& timeD > 1457.48e6");
	ch2->Draw(Form("total[%d]/pot:pot>>d1_16_lin",chanNum), hornCut +"&&"+ potCut +"&&"+ DTpotCut);
	d1_16_lin->Fit("pol1");

	cLin->cd(2);
	ch->Draw("total[1]/pot:pot>>d1_17_lin", hornCut +"&&"+ potCut +"&&"+ DTpotCut);
	d1_17_lin->Fit("pol1");

	// return;
// gets(s);  

	// normalized signal vs time plot - tgraph
	TCanvas* cPot1 = new TCanvas("cPot1", "Signal vs Time Canvas", 30, 30, 1600, 900);
	cPot1->Divide(0,2);
	cPot1->cd(1);
	ch2->Draw(Form("total[%d]/pot:time",chanNum), hornCut +"&&"+ potCut +"&&"+ DTpotCut);
	TGraph *d1_17_pot1 = (TGraph*)gPad->GetPrimitive("Graph")->Clone("d1_17_pot1");
	gROOT->cd(0);
	d1_17_pot1->SetTitle("Total Signal vs Time (Fall 2018);Time;Signal / POT");
	d1_17_pot1->GetXaxis()->SetTimeDisplay(1);
	d1_17_pot1->GetXaxis()->SetNdivisions(-508);
	d1_17_pot1->GetXaxis()->SetTimeFormat("%m/%d"); 
	d1_17_pot1->GetXaxis()->SetTimeOffset(0,"utc");
	// d1_17_pot1->GetYaxis()->SetRangeUser(-85e3,-65e3);
	d1_17_pot1->Draw("ap");
	cPot1->cd(2);
	ch2->Draw("pot:timeD", hornCut +"&&"+ potCut +"&&"+ DTpotCut);
	TGraph *pot_17 = (TGraph*)gPad->GetPrimitive("Graph")->Clone("pot_17");
	gROOT->cd(0);
	pot_17->SetTitle("POT vs Time (Fall 2018);Time;POT");
	pot_17->GetXaxis()->SetTimeDisplay(1);
	pot_17->GetXaxis()->SetNdivisions(-508);
	pot_17->GetXaxis()->SetTimeFormat("%m/%d"); 
	pot_17->GetXaxis()->SetTimeOffset(0,"utc");
	pot_17->Draw("ap");
// gets(s); 

	// normalized signal vs time and hornI vs time plot - tgraph
	TCanvas* cSig2 = new TCanvas("cSig2", "Investigation 2", 30, 30, 1600, 900);
	cSig2->Divide(0,2);
	cSig2->cd(1);
	d1_17_pot1->Draw("ap");
	cSig2->cd(2);
	ch2->Draw("hornI:timeD", hornCut +"&&"+ potCut +"&&"+ DTpotCut);
	TGraph *hornI_17 = (TGraph*)gPad->GetPrimitive("Graph")->Clone("hornI_17");
	gROOT->cd(0);
	hornI_17->SetTitle("Horn Current vs Time (Fall 2018);Time;Current (kA)");
	hornI_17->GetXaxis()->SetTimeDisplay(1);
	hornI_17->GetXaxis()->SetNdivisions(-508);
	hornI_17->GetXaxis()->SetTimeFormat("%m/%d"); 
	hornI_17->GetXaxis()->SetTimeOffset(0,"utc");
	hornI_17->Draw("ap");

	//return;

	// normalized signal vs time and beam r position vs time plot - tgraph
	TCanvas* cSig3 = new TCanvas("cSig3", "Investigation 3", 30, 30, 1600, 900);
	cSig3->Divide(0,2);
	cSig3->cd(1);
	d1_17_pot1->Draw("ap");
	cSig3->cd(2);
	ch2->Draw("((mm1x)^2+(mm1y)^2):timeD", hornCut +"&&"+ potCut +"&&"+ DTpotCut);
	TGraph *mm1rc_17 = (TGraph*)gPad->GetPrimitive("Graph")->Clone("mm1rc_17");
	gROOT->cd(0);
	mm1rc_17->SetTitle("Radial #nu beam centroid vs Time (Fall 2018);Time;Current (kA)");
	mm1rc_17->GetYaxis()->SetRangeUser(0.5,2.5);
	mm1rc_17->GetXaxis()->SetTimeDisplay(1);
	mm1rc_17->GetXaxis()->SetNdivisions(-508);
	mm1rc_17->GetXaxis()->SetTimeFormat("%m/%d"); 
	mm1rc_17->GetXaxis()->SetTimeOffset(0,"utc");
	mm1rc_17->Draw("ap");



	// // normalized signal vs time and dt_horn vs time plot - tgraph
	// TCanvas* cSig4 = new TCanvas("cSig4", "Investigation 4", 30, 30, 1600, 900);
	// cSig4->Divide(0,2);
	// cSig4->cd(1);
	// d1_17_pot1->Draw("ap");
	// cSig4->cd(2);
	// ch2->Draw("dt_horn:timeD", hornCut +"&&"+ potCut +"&&"+ DTpotCut);
	// TGraph *invest4 = (TGraph*)gPad->GetPrimitive("Graph")->Clone("invest4");
	// gROOT->cd(0);
	// invest4->SetTitle("ACNET horn data time offset vs Time (Fall 2018);Time;Current (kA)");
	// // mm1x_17->GetYaxis()->SetRangeUser(0.5,2.5);
	// invest4->GetXaxis()->SetTimeDisplay(1);
	// invest4->GetXaxis()->SetNdivisions(-508);
	// invest4->GetXaxis()->SetTimeFormat("%m/%d"); 
	// invest4->GetXaxis()->SetTimeOffset(0,"utc");
	// invest4->Draw("ap");



	// // normalized signal vs time plot with horn cut - tgraph
	// TCanvas* cPot2 = new TCanvas("cPot2", "Signal vs Time with horn cut Canvas", 30, 30, 1600, 900);
	// cPot2->Divide(0,2);
	// cPot2->cd(1);
	// ch2->Draw(Form("total[%d]/pot:time",chanNum), hornCut +"&&"+ potCut +"&&"+ DTpotCut +"&&"+ DThornCut);
	// TGraph *d1_17_pot2 = (TGraph*)gPad->GetPrimitive("Graph")->Clone("d1_17_pot2");
	// gROOT->cd(0);
	// d1_17_pot2->SetTitle("Total Signal vs Time (Fall 2018) with horn cut;Time;Signal / POT");
	// d1_17_pot2->GetXaxis()->SetTimeDisplay(1);
	// d1_17_pot2->GetXaxis()->SetNdivisions(-508);
	// d1_17_pot2->GetXaxis()->SetTimeFormat("%m/%d"); 
	// d1_17_pot2->GetXaxis()->SetTimeOffset(0,"utc");
	// d1_17_pot2->Draw("ap");
	// cPot2->cd(2);
	// ch2->Draw("pot:timeD", hornCut +"&&"+ potCut +"&&"+ DTpotCut +"&&"+ DThornCut);
	// TGraph *pot_17_2 = (TGraph*)gPad->GetPrimitive("Graph")->Clone("pot_17_2");
	// gROOT->cd(0);
	// pot_17_2->SetTitle("Total Signal vs Time (Fall 2018) with horn cut;Time;Signal / POT");
	// pot_17_2->GetXaxis()->SetTimeDisplay(1);
	// pot_17_2->GetXaxis()->SetNdivisions(-508);
	// pot_17_2->GetXaxis()->SetTimeFormat("%m/%d"); 
	// pot_17_2->GetXaxis()->SetTimeOffset(0,"utc");
	// pot_17_2->Draw("ap");




	// // normalized signal vs time and beam x position vs time plot - tgraph
	// TCanvas* cSig5 = new TCanvas("cSig5", "Investigation 5", 30, 30, 1600, 900);
	// cSig5->Divide(0,3);
	// cSig5->cd(1);
	// d1_17_pot1->Draw("ap");
	// cSig5->cd(2);
	// ch2->Draw("mm1x:timeD", hornCut +"&&"+ potCut +"&&"+ DTpotCut);
	// TGraph *mm1x_17 = (TGraph*)gPad->GetPrimitive("Graph")->Clone("mm1x_17");
	// gROOT->cd(0);
	// mm1x_17->SetTitle("X axis #nu beam centroid vs Time (Fall 2018);Time;Current (kA)");
	// mm1x_17->GetYaxis()->SetRangeUser(0.85,1.35);
	// mm1x_17->GetXaxis()->SetTimeDisplay(1);
	// mm1x_17->GetXaxis()->SetNdivisions(-508);
	// mm1x_17->GetXaxis()->SetTimeFormat("%m/%d"); 
	// mm1x_17->GetXaxis()->SetTimeOffset(0,"utc");
	// mm1x_17->Draw("ap");
	// cSig5->cd(3);
	// ch2->Draw("mm1y:timeD", hornCut +"&&"+ potCut +"&&"+ DTpotCut);
	// TGraph *mm1y_17 = (TGraph*)gPad->GetPrimitive("Graph")->Clone("mm1y_17");
	// gROOT->cd(0);
	// mm1y_17->SetTitle("Y axis #nu beam centroid vs Time (Fall 2018);Time;Current (kA)");
	// mm1y_17->GetYaxis()->SetRangeUser(-0.75,1.2);
	// mm1y_17->GetXaxis()->SetTimeDisplay(1);
	// mm1y_17->GetXaxis()->SetNdivisions(-508);
	// mm1y_17->GetXaxis()->SetTimeFormat("%m/%d"); 
	// mm1y_17->GetXaxis()->SetTimeOffset(0,"utc");
	// mm1y_17->Draw("ap");


	

	// // normalized signal vs time plot - tgraph
	// TCanvas* cSig6 = new TCanvas("cSig6", "Investigation 6", 30, 30, 1600, 900);
	// cSig6->Divide(0,2);
	// cSig6->cd(1);
	// d1_17_pot1->Draw("ap");
	// cSig6->cd(2);
	// ch2->Draw("mm1cor/pot:timeD", hornCut +"&&"+ potCut +"&&"+ DTpotCut);
	// TGraph *mm1_17 = (TGraph*)gPad->GetPrimitive("Graph")->Clone("mm1_17");
	// gROOT->cd(0);
	// mm1_17->SetTitle("Beam intensity from mm1 (black), mm2 (red), and mm3 (blue) (Fall 2018);Time;Diamond1 / POT");
	// mm1_17->GetXaxis()->SetTimeDisplay(1);
	// mm1_17->GetXaxis()->SetNdivisions(-508);
	// mm1_17->GetXaxis()->SetTimeFormat("%m/%d"); 
	// mm1_17->GetXaxis()->SetTimeOffset(0,"utc");
	// mm1_17->SetMarkerColor(kBlack);
	// ch2->Draw("mm2cor/pot:timeD", hornCut +"&&"+ potCut +"&&"+ DTpotCut);
	// TGraph *mm2_17 = (TGraph*)gPad->GetPrimitive("Graph")->Clone("mm2_17");
	// gROOT->cd(0);
	// mm2_17->SetTitle("Beam intensity from mm1 (black), mm2 (red), and mm3 (blue) (Fall 2018);Time;Diamond1 / POT");
	// mm2_17->GetXaxis()->SetTimeDisplay(1);
	// mm2_17->GetXaxis()->SetNdivisions(-508);
	// mm2_17->GetXaxis()->SetTimeFormat("%m/%d"); 
	// mm2_17->GetXaxis()->SetTimeOffset(0,"utc");
	// mm2_17->SetMarkerColor(kRed);
	// ch2->Draw("mm3cor/pot:timeD", hornCut +"&&"+ potCut +"&&"+ DTpotCut);
	// TGraph *mm3_17 = (TGraph*)gPad->GetPrimitive("Graph")->Clone("mm3_17");
	// gROOT->cd(0);
	// mm3_17->SetTitle("Beam intensity from mm1 (black), mm2 (red), and mm3 (blue) (Fall 2018);Time;Diamond1 / POT");
	// mm3_17->GetXaxis()->SetTimeDisplay(1);
	// mm3_17->GetXaxis()->SetNdivisions(-508);
	// mm3_17->GetXaxis()->SetTimeFormat("%m/%d"); 
	// mm3_17->GetXaxis()->SetTimeOffset(0,"utc");
	// mm3_17->SetMarkerColor(kBlue);
	// mm2_17->Draw("ap");
	// mm1_17->Draw("samep");
	// mm3_17->Draw("samep");


	

	// // normalized signal vs time plot - tgraph
	// TCanvas* cSig6_1 = new TCanvas("cSig6_1", "Investigation 6.1", 30, 30, 1600, 900);
	// cSig6_1->Divide(0,2);
	// cSig6_1->cd(1);
	// d1_17_pot1->Draw("ap");
	// cSig6_1->cd(2);
	// ch2->Draw("mm1cor/pot:timeD", hornCut +"&&"+ potCut +"&&"+ DTpotCut);
	// TGraph *mm1cor_17 = (TGraph*)gPad->GetPrimitive("Graph")->Clone("mm1cor_17");
	// gROOT->cd(0);
	// mm1cor_17->SetTitle("Beam intensity from mm1 - corrected (black), mm2 - corrected (red), and mm3 - corrected (blue) (Fall 2018);Time;Diamond1 / POT");
	// mm1cor_17->GetXaxis()->SetTimeDisplay(1);
	// mm1cor_17->GetXaxis()->SetNdivisions(-508);
	// mm1cor_17->GetXaxis()->SetTimeFormat("%m/%d"); 
	// mm1cor_17->GetXaxis()->SetTimeOffset(0,"utc");
	// mm1cor_17->SetMarkerColor(kBlack);
	// ch2->Draw("mm2cor/pot:timeD", hornCut +"&&"+ potCut +"&&"+ DTpotCut);
	// TGraph *mm2cor_17 = (TGraph*)gPad->GetPrimitive("Graph")->Clone("mm2cor_17");
	// gROOT->cd(0);
	// mm2cor_17->SetTitle("Beam intensity from mm1 - corrected (black), mm2 - corrected (red), and mm3 - corrected (blue) (Fall 2018);Time;Diamond1 / POT");
	// mm2cor_17->GetXaxis()->SetTimeDisplay(1);
	// mm2cor_17->GetXaxis()->SetNdivisions(-508);
	// mm2cor_17->GetXaxis()->SetTimeFormat("%m/%d"); 
	// mm2cor_17->GetXaxis()->SetTimeOffset(0,"utc");
	// mm2cor_17->SetMarkerColor(kRed);
	// ch2->Draw("mm3cor/pot:timeD", hornCut +"&&"+ potCut +"&&"+ DTpotCut);
	// TGraph *mm3cor_17 = (TGraph*)gPad->GetPrimitive("Graph")->Clone("mm3cor_17");
	// gROOT->cd(0);
	// mm3cor_17->SetTitle("Beam intensity from mm1 - corrected (black), mm2 - corrected (red), and mm3 - corrected (blue) (Fall 2018);Time;Diamond1 / POT");
	// mm3cor_17->GetXaxis()->SetTimeDisplay(1);
	// mm3cor_17->GetXaxis()->SetNdivisions(-508);
	// mm3cor_17->GetXaxis()->SetTimeFormat("%m/%d"); 
	// mm3cor_17->GetXaxis()->SetTimeOffset(0,"utc");
	// mm3cor_17->SetMarkerColor(kBlue);
	// mm2cor_17->Draw("ap");
	// mm1cor_17->Draw("samep");
	// mm3cor_17->Draw("samep");






	// // combine investigatino 3 and 6
	// TCanvas* cSig7 = new TCanvas("cSig7", "Investigation 7", 30, 30, 1600, 900);
	// cSig7->Divide(0,3);
	// cSig7->cd(1);
	// d1_17_pot1->Draw("ap");
	// cSig7->cd(2);
	// mm1rc_17->Draw("ap");
	// cSig7->cd(3);
	// mm2_17->Draw("ap");
	// mm1_17->Draw("samep");
	// mm3_17->Draw("samep");
  




	// normalized signal vs time and raw gas muon monitor data channels 88 and 89 (indices 72 nd 73)
	TCanvas* cSig8 = new TCanvas("cSig8", "Investigation 8 - Muon Monitor plot 1", 30, 30, 1600, 900);
	cSig8->Divide(0,3);
	cSig8->cd(1);
	d1_17_pot1->Draw("ap");
	cSig8->cd(2);
	ch2->Draw("mma1cor[72]/pot:timeD", hornCut +"&&"+ potCut +"&&"+ DTpotCut);
	TGraph *mma1cor88_fa18 = (TGraph*)gPad->GetPrimitive("Graph")->Clone("mma1cor88_fa18");
	gROOT->cd(0);
	mma1cor88_fa18->SetTitle("MMA1DS Channel 88 (subtracted pedestal, pot normalized) vs Time (Fall 2018);Time;Volts");
	mma1cor88_fa18->GetYaxis()->SetRangeUser(0.85,1.35);
	mma1cor88_fa18->GetXaxis()->SetTimeDisplay(1);
	mma1cor88_fa18->GetXaxis()->SetNdivisions(-508);
	mma1cor88_fa18->GetXaxis()->SetTimeFormat("%m/%d"); 
	mma1cor88_fa18->GetXaxis()->SetTimeOffset(0,"utc");
	mma1cor88_fa18->Draw("ap");
	cSig8->cd(3);
	ch2->Draw("mma1cor[73]/pot:timeD", hornCut +"&&"+ potCut +"&&"+ DTpotCut);
	TGraph *mma1cor89_fa18 = (TGraph*)gPad->GetPrimitive("Graph")->Clone("mma1cor89_fa18");
	gROOT->cd(0);
	mma1cor89_fa18->SetTitle("MMA1DS Channel 89 (subtracted pedestal, pot normalized) vs Time (Fall 2018);Time;Volts");
	mma1cor89_fa18->GetYaxis()->SetRangeUser(-0.75,1.2);
	mma1cor89_fa18->GetXaxis()->SetTimeDisplay(1);
	mma1cor89_fa18->GetXaxis()->SetNdivisions(-508);
	mma1cor89_fa18->GetXaxis()->SetTimeFormat("%m/%d"); 
	mma1cor89_fa18->GetXaxis()->SetTimeOffset(0,"utc");
	mma1cor89_fa18->Draw("ap");
  




	// normalized signal vs time and first temperature measurement tttgth
	TCanvas* cSig9 = new TCanvas("cSig9", "Investigation 9 - temperature plot 1", 30, 30, 1600, 900);
	cSig9->Divide(0,2);
	cSig9->cd(1);
	d1_17_pot1->Draw("ap");
	cSig9->cd(2);
	ch2->Draw("tttgth:timeD", hornCut +"&&"+ potCut +"&&"+ DTpotCut);
	TGraph *tttgth_fa18 = (TGraph*)gPad->GetPrimitive("Graph")->Clone("tttgth_fa18");
	gROOT->cd(0);
	tttgth_fa18->SetTitle("MMA1DS Channel 88 (subtracted pedestal, pot normalized) vs Time (Fall 2018);Time;Volts");
	tttgth_fa18->GetYaxis()->SetRangeUser(0.85,1.35);
	tttgth_fa18->GetXaxis()->SetTimeDisplay(1);
	tttgth_fa18->GetXaxis()->SetNdivisions(-508);
	tttgth_fa18->GetXaxis()->SetTimeFormat("%m/%d"); 
	tttgth_fa18->GetXaxis()->SetTimeOffset(0,"utc");
	tttgth_fa18->Draw("ap");

  




	// normalized signal vs time and first temperature measurement tttgth
	TCanvas* cSig10 = new TCanvas("cSig10", "Investigation 10 - temperature plot 2", 30, 30, 1600, 900);
	// cSig9->Divide(0,2);
	cSig10->cd();
	// d1_17_pot1->Draw("ap");
	// cSig9->cd(2);
	ch2->Draw(Form("total[%d]/pot:tttgth",chanNum), hornCut +"&&"+ potCut +"&&"+ DTpotCut);
	TGraph *tttgthcorel_fa18 = (TGraph*)gPad->GetPrimitive("Graph")->Clone("tttgthcorel_fa18");
	gROOT->cd(0);
	tttgthcorel_fa18->SetTitle("Normalized signal vs tttgth;Temperature;Detector Signal");
	// tttgthcorel_fa18->GetYaxis()->SetRangeUser(0.85,1.35);
	// tttgthcorel_fa18->GetXaxis()->SetTimeDisplay(1);
	// tttgthcorel_fa18->GetXaxis()->SetNdivisions(-508);
	// tttgthcorel_fa18->GetXaxis()->SetTimeFormat("%m/%d"); 
	// tttgthcorel_fa18->GetXaxis()->SetTimeOffset(0,"utc");
	tttgthcorel_fa18->Draw("ap");





























	// normalized signal vs time plot - tgraph
	TCanvas* cSig11 = new TCanvas("cPot11", "Signal vs PoT Canvas", 30, 30, 1600, 900);
	cSig11->cd();
	ch2->Draw(Form("total[%d]:pot",chanNum), hornCut +"&&"+ potCut +"&&"+ DTpotCut);
	TGraph *sigVsPot = (TGraph*)gPad->GetPrimitive("Graph")->Clone("sigVsPot");
	gROOT->cd(0);
	sigVsPot->SetTitle(Form("Fall 2018 Signal vs PoT for Detector %d;PoT;Signal", chanNum));
	// d1_17_pot1->GetXaxis()->SetTimeDisplay(1);
	// d1_17_pot1->GetXaxis()->SetNdivisions(-508);
	// d1_17_pot1->GetXaxis()->SetTimeFormat("%m/%d"); 
	// d1_17_pot1->GetXaxis()->SetTimeOffset(0,"utc");
	// d1_17_pot1->GetYaxis()->SetRangeUser(-85e3,-65e3);
	sigVsPot->Draw("ap");
	sigVsPot->Fit("pol1");
	int npoint = sigVsPot->GetN();
	sigVsPot->SetPoint(npoint,0,0);
	if (chanNum == 3) {
		TH2D *hist1 = new TH2D("hist1", Form("Fall 2018 Signal vs PoT for Detector %d;PoT;Signal", chanNum), 55*8, 0, 55, 410*2, 0, 410e3);
	} else if (chanNum == 4) {
		TH2D *hist1 = new TH2D("hist1", Form("Fall 2018 Signal vs PoT for Detector %d;PoT;Signal", chanNum), 55*8, 0, 55, 820*2, 0, -8200e3);
	} else if (chanNum == 2) {
		TH2D *hist1 = new TH2D("hist1", Form("Fall 2018 Signal vs PoT for Detector %d;PoT;Signal", chanNum), 55*8, 0, 55, 400*2, 0, -4200e3);
	}
	for(int i=0; i < npoint; ++i) {
		double x,y;
		sigVsPot->GetPoint(i, x, y);
		hist1->Fill(x,y);
	}

	TCanvas* cSig12 = new TCanvas("cPot12", "Signal vs PoT Histo Canvas", 30, 30, 1600, 900);
	cSig12->cd();
	hist1->Draw("colz");
	hist1->Fit("pol1","F2","",10,55);
	// hist1->Fit("pol1","F1");
	// gets(s);
	hist1->GetFunction("pol1")->SetRange(0,55);
	hist1->GetFunction("pol1")->Draw("same");

}

