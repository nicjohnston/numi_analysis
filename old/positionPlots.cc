/*
This file generates plots of the muon beam position using the NuMI gas flow muon monitors
*/
#include "TFile.h"
#include "TTree.h"
#include "TObjArray.h"
#include "TH1.h"
using namespace std;
void gasHadMonBeamPos(const char* filename, const int sensorID = 1, const int index = 0, const int potCutThreshold = 10, const char* drawOption = "LINE") {
	// TFile *file = new TFile( filename );
	// cout << "test1" << endl;
	// TTree* tree = static_cast<TTree*> ( file->Get("MuMonData") );
	TChain *chain = new TChain( "MuMonData" );
	chain->Add("/usr/users/jplopez/data41/DUNE/Workdir/results/Results_2016_05.root");
	chain->Add("/usr/users/jplopez/data41/DUNE/Workdir/results/Results_2016_04.root");

	unsigned long long time; // AKA ULong64_t. A Unix timestamp
	int usec; // Microseconds part of timestamp
	double timeD;
	double total[4];
	double mm1x, mm1y, mm2x, mm2y;
	double pot, hornI;
	chain->SetBranchAddress("time",&time);
	chain->SetBranchAddress("usec",&usec);
	chain->SetBranchAddress("timeD",&timeD);
	chain->SetBranchAddress("total",&total);
	chain->SetBranchAddress("mm1x",&mm1x);
	chain->SetBranchAddress("mm1y",&mm1y);
	chain->SetBranchAddress("mm2x",&mm2x);
	chain->SetBranchAddress("mm2y",&mm2y);
	chain->SetBranchAddress("pot",&pot);
	chain->SetBranchAddress("hornI",&hornI);
//	chain->SetBranchAddress("timeD",&timeD);
	int n_entries = chain->GetEntries();

	TGraph *graph_mm1x = new TGraph();
	TGraph *graph_mm1y = new TGraph();
	TGraph *graph_mm2x = new TGraph();
	TGraph *graph_mm2y = new TGraph();
	TGraph2D *graph_mm1 = new TGraph2D();
	TGraph *graph_pot = new TGraph();
	TGraph *graph_hornI = new TGraph();

	TGraph *graph_mm1x_cut = new TGraph();
	TGraph *graph_mm1y_cut = new TGraph();
	TGraph *graph_pot_cut = new TGraph();
	TGraph *graph_hornI_cut = new TGraph();
	
	TCanvas * c1 = new TCanvas("c1","c1");
	// TCanvas * c2 = new TCanvas("c2","c2");
//	TCanvas * c3 = new TCanvas("c3","c3");
	chain->GetEntry(index);
	// c1->cd();
	cout.precision(16);
	cout << "time= " << time << endl;
	cout << "usec= " << usec << endl;
	cout << "timeD= " << timeD << endl;
	cout << "n_entries= " << n_entries << endl;
	cout << "integral= " << total[1] << endl;
	int cut_counter = 0;
	// TCanvas * c4 = new TCanvas("c4","c4");
	for (int idx = 0; idx < n_entries; idx++){
		chain->GetEntry(idx);
		graph_mm1x->SetPoint(idx,timeD,mm1x);
		graph_mm1y->SetPoint(idx,timeD,mm1y);
		// graph_mm1->SetPoint(idx,timeD,mm1x,mm1y);
		graph_pot->SetPoint(idx,timeD,pot);
		graph_hornI->SetPoint(idx,timeD,hornI);
		// graph_mm2x->SetPoint(idx,timeD,mm2x);
		// graph_mm2y->SetPoint(idx,timeD,mm2y);
		if ( pot > potCutThreshold ) {
			graph_mm1x_cut->SetPoint(cut_counter,timeD,mm1x);
			graph_mm1y_cut->SetPoint(cut_counter,timeD,mm1y);
			graph_pot_cut->SetPoint(cut_counter,timeD,pot);
			graph_hornI_cut->SetPoint(cut_counter,timeD,hornI);
			cut_counter = cut_counter + 1;
		}
	}
	// c4->cd();
	c1->Divide(2,3);
	c1->cd(1);
	graph_mm1x->GetXaxis()->SetTimeDisplay(1);
	graph_mm1x->GetXaxis()->SetLabelSize(0.03);
	// graph_mm1x->GetXaxis()->SetTimeFormat("%H:%M:%S");
	graph_mm1x->GetXaxis()->SetTimeOffset(0,"gmt");
	graph_mm1x->SetTitle("Alcove 1 muon monitor x position");
	graph_mm1x->Draw(drawOption);


	cout << "first plot drawn " << graph_mm1x->GetN() << endl;
	c1->cd(3);
	graph_mm1y->GetXaxis()->SetTimeDisplay(1);
	graph_mm1y->GetXaxis()->SetLabelSize(0.03);
	// graph_mm1y->GetXaxis()->SetTimeFormat("%H:%M:%S");
	graph_mm1y->GetXaxis()->SetTimeOffset(0,"gmt");
	graph_mm1y->SetTitle("Alcove 1 muon monitor y position");
	graph_mm1y->Draw(drawOption);
	cout << "second plot drawn " << graph_mm1y->GetN() << endl;
	c1->cd(5);
	graph_hornI->GetXaxis()->SetTimeDisplay(1);
	graph_hornI->GetXaxis()->SetLabelSize(0.03);
	// graph_hornI->GetXaxis()->SetTimeFormat("%H:%M:%S");
	graph_hornI->GetXaxis()->SetTimeOffset(0,"gmt");
	graph_hornI->SetTitle("Protons on targets");
	graph_hornI->Draw(drawOption);
	cout << "third plot drawn " << graph_hornI->GetN() << endl;



	c1->cd(2);
	graph_mm1x_cut->GetXaxis()->SetTimeDisplay(1);
	graph_mm1x_cut->GetXaxis()->SetLabelSize(0.03);
	// graph_mm1x->GetXaxis()->SetTimeFormat("%H:%M:%S");
	graph_mm1x_cut->GetXaxis()->SetTimeOffset(0,"gmt");
	graph_mm1x_cut->SetTitle("Alcove 1 muon monitor x position");
	graph_mm1x_cut->Draw(drawOption);
	cout << "first plot drawn " << graph_mm1x_cut->GetN() << endl;

	c1->cd(4);
	graph_mm1y_cut->GetXaxis()->SetTimeDisplay(1);
	graph_mm1y_cut->GetXaxis()->SetLabelSize(0.03);
	// graph_mm1y->GetXaxis()->SetTimeFormat("%H:%M:%S");
	graph_mm1y_cut->GetXaxis()->SetTimeOffset(0,"gmt");
	graph_mm1y_cut->SetTitle("Alcove 1 muon monitor y position");
	graph_mm1y_cut->Draw(drawOption);
	cout << "second plot drawn " << graph_mm1y_cut->GetN() << endl;

	c1->cd(6);
	graph_hornI_cut->GetXaxis()->SetTimeDisplay(1);
	graph_hornI_cut->GetXaxis()->SetLabelSize(0.03);
	// graph_hornI->GetXaxis()->SetTimeFormat("%H:%M:%S");
	graph_hornI_cut->GetXaxis()->SetTimeOffset(0,"gmt");
	graph_hornI_cut->SetTitle("Protons on targets");
	graph_hornI_cut->Draw(drawOption);
	cout << "third plot drawn " << graph_hornI_cut->GetN() << endl;



	// c1->cd(3);
	// graph_mm2x->GetXaxis()->SetTimeDisplay(1);
	// graph_mm2x->GetXaxis()->SetLabelSize(0.03);
	// // graph_mm2x->GetXaxis()->SetTimeFormat("%H:%M:%S");
	// graph_mm2x->GetXaxis()->SetTimeOffset(0,"gmt");
	// graph_mm2x->Draw();


	// c1->cd(4);
	// graph_mm2y->GetXaxis()->SetTimeDisplay(1);
	// graph_mm2y->GetXaxis()->SetLabelSize(0.03);
	// // graph_mm2y->GetXaxis()->SetTimeFormat("%H:%M:%S");
	// graph_mm2y->GetXaxis()->SetTimeOffset(0,"gmt");
	// graph_mm2y->Draw();
	// TGaxis *axis = new TGaxis(gPad->GetUxmin(),gPad->GetUymax(),gPad->GetUxmax(),gPad->GetUymax(),0,1000,510,"+L");
	// axis->Draw();
	// c2->cd();
	// graph_mm1->GetXaxis()->SetTimeDisplay(1);
	// graph_mm1->GetXaxis()->SetLabelSize(0.03);
	// graph_mm1->GetXaxis()->SetTimeOffset(0,"gmt");
	// graph_mm1->GetYaxis()->SetTitle("X axis");
	// graph_mm1->GetZaxis()->SetTitle("Y axis");
	// graph_mm1->Draw(drawOption);
}
